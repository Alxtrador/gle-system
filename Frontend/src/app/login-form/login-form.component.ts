import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { 
    this.loginForm = this.formBuilder.group({
      uname: '',
      pass: ''
    });
  }

  onSubmit(loginData) {
    this.userService.login(loginData).subscribe();
  }

  ngOnInit() {
  }

}
