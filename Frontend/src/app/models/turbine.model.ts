export class Turbine {
    public name: string;
    public id: number;
    public production: number;
    public windspeed: number;

    constructor(name: string, id: number, production: number, windspeed: number){
        this.name = name;
        this.id = id;
        this.production = production;
        this.windspeed = windspeed;
    }
}