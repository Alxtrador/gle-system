import { CoalPlant } from "./coalplant.model";
import { Market } from './market.model';

export class Manager {
    public coalplant: CoalPlant;
    public market: Market;

    constructor(coalplant: CoalPlant, market: Market) {
        this.coalplant = coalplant;
        this.market = market;
    }
}