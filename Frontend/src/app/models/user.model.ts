import { Household } from './household.model';

export class User {
    public id: number;
    public userName: string;
    public firstName: string;
    public lastName: string;

    public loggedIn: boolean;
    public blocked: boolean;
    public households: Household[];

    public image: File;
    public imgURL: string | ArrayBuffer;

    public constructor(userName: string, firstName: string, lastName: string, id: number) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.loggedIn = false;
        this.blocked = false;
        this.households = null;
    }

    updateImgView(){
        var reader = new FileReader();
        reader.readAsDataURL(this.image); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result;
        }
    }
}