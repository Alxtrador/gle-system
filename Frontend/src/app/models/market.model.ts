export class Market{
    public demand: number;
    public price: number;
    public modeled_price: number;
    public buffer: number;

    constructor(demand: number, price: number, modeled_price: number){
        this.demand = demand;
        this.price = price;
        this.modeled_price = modeled_price;
        this.buffer = 0;
    }
}