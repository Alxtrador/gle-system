import { Turbine } from './turbine.model';

export class Household {
    public name: string;
    public id: number;
    public gpsx: number;
    public gpsy: number;

    public turbines: Turbine[];
    public production: number;
    public consumption: number;
    public buffer: number;
    public bufferThreshold: number;
    public sellThresh: number;
    public buyThresh: number;
    public blackout: boolean;
    public address: string;

    public image: File;
    public imgURL: string | ArrayBuffer;

    public percentageBuff: number;
    public buffer_limit: number;

    constructor(name: string, id: number){
        this.name = name;
        this.id = id;
        this.turbines = [];
        this.consumption = 0;
        this.buffer = 0;
        this.bufferThreshold = 0;
        this.sellThresh = 0;
        this.buyThresh = 0;
        this.image = null;
        this.percentageBuff = 0;
        this.buffer_limit = 0;
        this.blackout = false;
    }

    calcTotProd(){
      this.production = 0;
      this.turbines.forEach(t => {
        this.production = this.production + t.production;
      })
      this.production = Math.round(this.production)
    }

    updateImgView(){
        var reader = new FileReader();
        reader.readAsDataURL(this.image); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result;
        }
      }
}