export enum Status {
    OFF = 0,
    STARTING = 1,
    ON = 2,
}

export class CoalPlant {
    public production: number;
    public buffer: number;
    public status: Status; // starting, running, stopped
    public buffer_ratio: number;
    public statusText: string;

    constructor(production: number, buffer: number, status: Status) {
        this.production = production;
        this.buffer = buffer;
        this.status = status;
        this.statusText = this.setStatusText(status);
        this.buffer_ratio = 0;
    }

    private setStatusText(status): string{
        switch(status) { 
            case 0: { 
               return "OFF";
            } 
            case 1: { 
               return "STARTING";
            }
            case 2: {
                return "ON";
            } 
            default: { 
               return "invalid";
            } 
         } 
    }
}