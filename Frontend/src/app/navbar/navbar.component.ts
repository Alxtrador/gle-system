import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isManager: boolean = true;

  constructor(
    private userService: UserService
    ) { }

  ngOnInit() {
    //Here we make a check after login if a user or manager logged in. Then we send them to the correct home page.
  }

  logout(){
    this.userService.logout().subscribe();;
  }

}
