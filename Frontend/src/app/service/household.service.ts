import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HouseholdService {

  constructor(
    private http: HttpClient
  ) { }

  managerGet(householdId: number, userId: number, path: string){
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/household/${householdId}/${path}`) 
  }

  userGet(householdId: number, path: string){
    return this.http.get<any>(`${environment.apiUrl}/get/household/${householdId}/${path}`);
  }

  setHouseImage(formData){
    return this.http.post(`${environment.apiUrl}/manager/image/upload/household`, formData);
  }

  // GETTERS

  getBlackout() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/households/blackout`);
  }

  getTurbines(householdId: number, manager: boolean) {
    if(manager){
      return this.http.get<any>(`${environment.apiUrl}/manager/get/user/household/${householdId}/turbines`)
    }
    return this.userGet(householdId, "turbines")
  }

  getCons(householdId: number, manager: boolean, userId: number) {
    if(manager){
      return this.managerGet(householdId, userId, "cons")
    }
    return this.userGet(householdId, "cons") 
  }

  getBuffer(householdId: number, manager: boolean, userId: number) {
    if(manager){
      return this.managerGet(householdId, userId, "buffer")
    }
    return this.userGet(householdId, "buffer") 
  }

  getSellRatio(householdId: number, manager: boolean, userId: number){
    if(manager){
      return this.managerGet(householdId, userId, "sell_ratio")
    }
    return this.userGet(householdId, "sell_ratio") 
  }

  getBuyRatio(householdId: number, manager: boolean, userId: number){
    if(manager){
      return this.managerGet(householdId, userId, "buy_ratio")
    }
    return this.userGet(householdId, "buy_ratio") 
  }

  getImage(householdId: number, manager: boolean, userId: number): Observable<Blob>{
    if(manager){
      return this.http.get(`${environment.apiUrl}/manager/image/user/${userId}/household/${householdId}/image`, {responseType: 'blob'});   
    }
    return this.http.get(`${environment.apiUrl}/image/household/${householdId}`, {responseType: 'blob'});   
  }

  getName(householdId: number, manager: boolean, userId: number){
    if(manager){
      return this.http.get<any>(`${environment.apiUrl}/manager/get/household/${householdId}/name`) // exception to the structure due to interface archetecture
    }
    return this.userGet(householdId, "name") 
  }

  getAddress(householdId: number, manager: boolean, userId: number){
    if(manager){
      return this.http.get<any>(`${environment.apiUrl}/manager/get/household/${householdId}/address`) // exception to the structure due to interface archetecture
    }
    return this.userGet(householdId, "address") 
  }

  getBufferLimit(householdId: number, manager: boolean, userId: number){
    if(manager){
      return this.managerGet(householdId, userId, "buffer_limit")
    }
    return this.userGet(householdId, "buffer_limit")
  }

  // SETTERS

  addHousehold(coords: Array<number>, name: string, address: string) {
    return this.http.post(`${environment.apiUrl}/add/household/coords/${coords}/name/${name}/address/${address}`, null);
  }

  removeHousehold(householdId: number) {
    return this.http.delete(`${environment.apiUrl}/delete/household/${householdId}`);
  }

  setImage(formData){
    return this.http.post(`${environment.apiUrl}/image/upload/household/`, formData);
  }

  setSellRatio(householdId: number,ratio: number){
    return this.http.post(`${environment.apiUrl}/control/household/${householdId}/buffer/sell/${ratio/100}`, null);
  }

  setBuyRatio(householdId: number, ratio: number){
    return this.http.post(`${environment.apiUrl}/control/household/${householdId}/buffer/buy/${ratio/100}`, null);
  }
}
