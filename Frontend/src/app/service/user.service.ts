import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  setImage(formData){
    return this.http.post<any>(`${environment.apiUrl}/image/upload/user/`, formData);
  }

  getImage(): Observable<Blob>{
    return this.http.get(`${environment.apiUrl}/image/user`, {responseType: 'blob'});   
  }

  signup(user) {
    return this.http.post(`${environment.apiUrl}/user/signup`, user, {responseType: 'text'});
  }

  login(user) {
    return this.http.post(`${environment.apiUrl}/user/login`, user);
  }

  logout(){
    return this.http.post(`${environment.apiUrl}/user/logout`, null);  
  }

  getHouseholds(manager: boolean, userId: number) {
    if(manager){
      return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/households`);
    }
    return this.http.get<any>(`${environment.apiUrl}/get/households`);
  }

  getMarketPrice() {
    return this.http.get<any>(`${environment.apiUrl}/get/elec_price`);
  }

  getUserName() {
    return this.http.get<any>(`${environment.apiUrl}/get/user_name`);
  }

  getFirstName() {
    return this.http.get<any>(`${environment.apiUrl}/get/first_name`);
  }
  
  getLastName() {
    return this.http.get<any>(`${environment.apiUrl}/get/last_name`);
  }
}
