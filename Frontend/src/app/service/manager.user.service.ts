import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerUserService {

  constructor(
    private http: HttpClient
  ) { }

  setBlock(userId,time: number){
    return this.http.post<any>(`${environment.apiUrl}/manager/control/user/${userId}/block/time/${time}`, null)
  }

  editUsername(username, userId){
    return this.http.post<any>(`${environment.apiUrl}/manager/user/update/user_name/id/${userId}/name/${username}`, null)
  }

  editFirstName(firstName, userId){
    return this.http.post<any>(`${environment.apiUrl}/manager/user/update/first_name/id/${userId}/name/${firstName}`, null)
  }

  editLastName(lastName, userId){
    return this.http.post<any>(`${environment.apiUrl}/manager/user/update/last_name/id/${userId}/name/${lastName}`, null)
  }

  setImage(formData){
    return this.http.post<any>(`${environment.apiUrl}/manager/image/upload/user`, formData);
  }

  getImage(userId): Observable<Blob>{
    return this.http.get(`${environment.apiUrl}/manager/image/user/${userId}/image`, {responseType: 'blob'});   
  }

  deleteUser(userId){
    return this.http.post<any>(`${environment.apiUrl}/manager/user/delete/${userId}`, null)
  }

  getUsers(){
    return this.http.get<any>(`${environment.apiUrl}/manager/get/users`)
  }

  getLogged(){
    return this.http.get<any>(`${environment.apiUrl}/manager/get/users/logged`)
  }

  getUsername(userId){
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/user_name`)
  }
  getFirstName(userId) {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/first_name`);
  }
  
  getLastName(userId) {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/last_name`);
  }

  getBlocked(userId) {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/block`);
  }

}