import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TurbineService {

  constructor(
    private http: HttpClient
  ) { }

  managerGet(householdId: number, turbineId: number, userId: number, path: string){
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/household/${householdId}/turbine/${turbineId}/${path}`) 
  }

  userGet(householdId: number, turbineId: number, path: string){
    return this.http.get<any>(`${environment.apiUrl}/get/household/${householdId}/turbine/${turbineId}/${path}`);
  }


  // GETTERS

  getWindSpeed(householdId: number, turbineId: number, manager: boolean, userId: number) {
    if(manager){
      return this.managerGet(householdId, turbineId, userId, "wind_speed")
    }
    return this.userGet(householdId, turbineId, "wind_speed") 
  }

  getElecProd(householdId: number, turbineId: number, manager: boolean, userId: number) {
    if(manager){
      return this.managerGet(householdId, turbineId, userId, "elec_prod")
    }
    return this.userGet(householdId, turbineId, "elec_prod") 
  }

  // SETTERS

  addTurbine(householdId: number, coords: Array<number>, name: string) {
    return this.http.post(environment.apiUrl + `/add/household/${householdId}/turbine/coords/${coords}/name/${name}`, null);
  }

  deleteTurbine(householdId: number, turbineId) {
    return this.http.delete(environment.apiUrl + `/household/${householdId}/turbine/${turbineId}`);
  }
}
