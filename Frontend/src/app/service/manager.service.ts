import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  constructor(
    private http: HttpClient
  ) { }

  getCoalProduction() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/power_plant/prod`)
  }

  getCoalBuffer() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/power_plant/buffer`)
  }

  getCoalStatus() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/power_plant/status`)
  }

  getMarketPrice() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/elec_price`)
  }

  getMarketModelledPrice() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/modelled_elec_price`)
  }

  getMarketDemand() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/market/demand`)
  }

  getMarketBuffer(){
    return this.http.get<any>(`${environment.apiUrl}/manager/get/market/buffer`)
  }

  editUsername(username, userId) {
    return this.http.post<any>(`${environment.apiUrl}/manager/user/update/user_name/id/${userId}/name/${username}`, null)
  }

  deleteUser(userId) {
    return this.http.post<any>(`${environment.apiUrl}/manager/delete/${userId}`, null)
  }

  getUsers() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/users`)
  }

  getBlackoutUsers() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/households/blackout`)
  }

  getHouseholds(userId) {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/households`)
  }

  getLogged() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/users/logged`)
  }

  getUsername(userId) {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/user/${userId}/user_name`)
  }

  getCoalRatio() {
    return this.http.get<any>(`${environment.apiUrl}/manager/get/power_plant/ratio`)
  }

  setCoalRatio(ratio) {
    return this.http.post<any>(`${environment.apiUrl}/manager/control/buffer/ratio/${ratio}`, null)
  }

  startCoalPlant() {
    return this.http.post<any>(`${environment.apiUrl}/manager/control/power_plant/prod`, null)
  }

  stopCoalPlant(){
    return this.http.post<any>(`${environment.apiUrl}/manager/control/power_plant/prod/stop`, null)
  }

  setMarketPrice(price) {
    return this.http.post<any>(`${environment.apiUrl}/manager/control/elec_price/${price}`, null)
  }

  blockUser(userId, time) {
    return this.http.post<any>(`${environment.apiUrl}/manager/control/user/${userId}/block/time/${time}`, null)
  }

  togglePrice(){
    return this.http.post<any>(`${environment.apiUrl}/manager/control/toggle/price`, null)
  }

}