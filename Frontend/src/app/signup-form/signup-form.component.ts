import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { UserService } from '../service/user.service';
import { equal } from 'assert';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  signupForm;
  responseText :string = "";
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { 
    this.signupForm = this.formBuilder.group({
      fname: '',
      lname: '',
      uname: '',
      pass: '',
      pass2: ''
    });
  }

  onSubmit(signupData) {
    if(signupData.pass === signupData.pass2){
      this.responseText = "You signed up!"
      this.userService.signup(signupData).subscribe();
    } else {
      this.responseText = "Passwords didn't match. Try again."
    }
  }

  ngOnInit() {
  }

}
