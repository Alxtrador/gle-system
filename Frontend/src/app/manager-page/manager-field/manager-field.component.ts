import { Component, OnInit, Input } from '@angular/core';
import { CoalPlant } from 'src/app/models/coalplant.model';
import { Market } from 'src/app/models/market.model';
import { ManagerService } from 'src/app/service/manager.service';
import { interval } from 'rxjs';
@Component({
  selector: 'app-manager-field',
  templateUrl: './manager-field.component.html',
  styleUrls: ['./manager-field.component.css']
})
export class ManagerFieldComponent{

  @Input('coalPlant') coalPlant = new CoalPlant(0,0,0);
  @Input('market') market = new Market(0,0,0);

  constructor(
    private managerService: ManagerService
  ) { }

  setRatio(ratio){
    this.managerService.setCoalRatio(ratio).subscribe();
  }

  setMarketPrice(price) {
    this.market.price = price;
    this.managerService.setMarketPrice(price).subscribe();
  }

  startPowerPlant() {
    this.coalPlant.status = 1;
    this.managerService.startCoalPlant().subscribe();
  }

  stopPowerPlant() {
    this.coalPlant.status = 0;
    this.managerService.stopCoalPlant().subscribe();
  }

  togglePrice(){
    this.managerService.togglePrice().subscribe();
  }
}
