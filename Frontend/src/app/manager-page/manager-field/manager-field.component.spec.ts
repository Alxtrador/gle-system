import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerFieldComponent } from './manager-field.component';

describe('ManagerFieldComponent', () => {
  let component: ManagerFieldComponent;
  let fixture: ComponentFixture<ManagerFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
