import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ManagerUserService } from 'src/app/service/manager.user.service';

@Component({
  selector: 'app-edit-user-field',
  templateUrl: './edit-user-field.component.html',
  styleUrls: ['./edit-user-field.component.css']
})
export class EditUserFieldComponent implements OnInit {

  @Input('userId') userId: number = 0;
  editForm;
  blockForm;
  blocktimeVar: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private managerUserService: ManagerUserService
  ) { 
    this.editForm = this.formBuilder.group({
      username: '',
      firstName: '',
      lastName: '',
    });

    this.blockForm = this.formBuilder.group({
      blocktime: '',
    });
  }

  edit(editData) {
    this.managerUserService.editFirstName(editData.firstName, this.userId).subscribe();
    this.managerUserService.editLastName(editData.lastName, this.userId).subscribe();
    this.managerUserService.editUsername(editData.username, this.userId).subscribe();
  }

  block(blockData){
    this.managerUserService.setBlock(this.userId, blockData.blocktime).subscribe();
  }

  changeBlockvar(time){
    this.blocktimeVar = time;
  }

  ngOnInit() {
  }

}
