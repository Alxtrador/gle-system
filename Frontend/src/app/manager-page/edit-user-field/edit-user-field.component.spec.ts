import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserFieldComponent } from './edit-user-field.component';

describe('EditUserFieldComponent', () => {
  let component: EditUserFieldComponent;
  let fixture: ComponentFixture<EditUserFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
