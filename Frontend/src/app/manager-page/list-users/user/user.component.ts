import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { ManagerUserService } from 'src/app/service/manager.user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input('user') user: User;
  
  constructor(
    private managerUserService: ManagerUserService
  ) { }

  ngOnInit() {
  }

  deleteUser(){
    this.managerUserService.deleteUser(this.user.id).subscribe()
  }

}
