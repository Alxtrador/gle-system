import { Component, Output, EventEmitter, Input } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent{

  @Input('selectedUser') selectedUser: User = new User("", "", "", -1);
  @Input('users') users: User[] = [];
  @Output() selected = new EventEmitter<User>();

  constructor(
  ) { }

  selectUser(u: User){
    this.selected.emit(u);
  }

}