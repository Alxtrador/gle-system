import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Household } from 'src/app/models/household.model';
import { Turbine } from 'src/app/models/turbine.model';
import { User } from 'src/app/models/user.model';
import { ManagerService } from 'src/app/service/manager.service';

@Component({
  selector: 'app-user-field',
  templateUrl: './user-field.component.html',
  styleUrls: ['./user-field.component.css']
})
export class UserFieldComponent implements OnInit {

  @Input('selectedUser') user: User = new User("", "", "", -1);
  manager: boolean = true;
  @Input('selectedTurbine') selectedTurbine: Turbine = new Turbine("", -1, 0, 0);
  @Input('selectedHousehold') selectedHousehold: Household = new Household("", -1);

  @Output() selectedH = new EventEmitter<Household>();
  @Output() selectedT = new EventEmitter<Turbine>();

  @Input('turbines') turbines: Turbine[] = [];

  constructor(
    private managerService: ManagerService
  ) { }

  ngOnInit(){
  }

  onSelectHousehold(h: Household){
    this.selectedH.emit(h)
    this.onSelectTurbine(new Turbine("", -1, 0, 0)); // deselect
  }

  // Selects a turbine
  onSelectTurbine(t: Turbine){
    this.selectedT.emit(t);
  }

  deleteUser(){
    this.managerService.deleteUser(this.user.id)
  }

  setBlocked(v){
    this.user.blocked = v;
    //user manager service to communicate to the server that the user is blocked then wait 10 seconds and then unblock
  }

  editUsername(name){
    this.managerService.editUsername(name, this.user.id)
    .subscribe(data => {
      this.user.firstName = name
    })
  }
}
