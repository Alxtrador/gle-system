import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ManagerFieldComponent } from './manager-field/manager-field.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { UserFieldComponent } from './user-field/user-field.component';
import { UserComponent } from './list-users/user/user.component';
import { ManagerPageComponent} from './manager-page.component';
import { UserPageModule } from '../user-page/user-page.module';
import { EditUserFieldComponent } from './edit-user-field/edit-user-field.component';

@NgModule({
  declarations: [
    ManagerFieldComponent, 
    ListUsersComponent, 
    UserFieldComponent, 
    UserComponent,
    ManagerPageComponent,
    EditUserFieldComponent,
  ],
  imports: [
    CommonModule,
    UserPageModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ManagerPageModule { }
