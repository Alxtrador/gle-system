import { Component, OnInit, ViewChild} from '@angular/core';
import { HouseholdDownloader } from '../downloaders/household.downloader';
import { User } from '../models/user.model';
import { HouseholdService } from '../service/household.service';
import { ManagerService } from '../service/manager.service';
import { UserService } from '../service/user.service';
import { TurbineService } from '../service/turbine.service';
import { TurbineDownloader } from '../downloaders/turbine.downloader';
import { interval } from 'rxjs';
import { Household } from '../models/household.model';
import { UserFieldComponent } from './user-field/user-field.component';
import { Turbine } from '../models/turbine.model';
import { ManagerUserService } from '../service/manager.user.service';
import { CoalPlant } from '../models/coalplant.model';
import { Market } from '../models/market.model';
import { ManagerUserDownloader } from '../downloaders/manager.user.downloader';
import { ManagerDownloader } from '../downloaders/manager.downloader';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manager-page',
  templateUrl: './manager-page.component.html',
  styleUrls: ['./manager-page.component.css'],
})
export class ManagerPageComponent implements OnInit {

  @ViewChild(UserFieldComponent, {static: false})
  private userfieldComponent: UserFieldComponent;

  selectedTurbine: Turbine = new Turbine("", -1, 0, 0);
  selectedHousehold: Household = new Household("", -1);
  selectedUser: User = new User("","", "", -1);
  
  users: User[] = [];
  managerUserDownloader: ManagerUserDownloader;
  householdDownloader: HouseholdDownloader
  turbineDownloader: TurbineDownloader
  managerDownloader: ManagerDownloader

  updateFreq: number = environment.update_frequency; // in seconds
  updateTimer: number = 0;

  coalPlant: CoalPlant = new CoalPlant(0,0,0);
  market: Market = new Market(0,0,0);
  

  constructor(
    managerService: ManagerService,
    userService: UserService,
    householdService: HouseholdService,
    turbineService: TurbineService,
    managerUserService: ManagerUserService
  ) { 
    this.managerUserDownloader = new ManagerUserDownloader(managerUserService)
    this.householdDownloader = new HouseholdDownloader(userService, householdService)
    this.turbineDownloader = new TurbineDownloader(householdService, turbineService)
    this.managerDownloader = new ManagerDownloader(managerService)
  }

  ngOnInit() {
  }

  ngAfterContentInit(){
    this.update();
    let second = interval(1000)
    second.subscribe(() => {
      this.updateTimer = this.updateTimer + 1;
      if(this.updateTimer == this.updateFreq ){
        this.update();
        this.updateTimer = 0;
      }
    })
  }

  onSelectHousehold(h: Household){
    this.selectedHousehold = h;
    this.selectedTurbine = new Turbine("", -1, 0, 0);; // deselect
  }

  onSelectTurbine(t: Turbine){
    this.selectedTurbine = t;
  }

  reSelect(uid, hid, tid){
      let userFiltered = this.users.filter(x => x.id == uid);
      if(userFiltered.length == 0){
        this.selectedUser = new User("","","",-1)
        this.selectedHousehold = new Household("",-1)
        this.selectedTurbine = new Turbine("", -1, 0, 0)
        return
      }
      else{
        var user = userFiltered[0];
        this.selectedUser = user
        let house = user.households.filter(x => x.id == hid)[0];
        if(house){
          this.selectedHousehold = house
          let turbine = house.turbines.filter(x => x.id == tid)[0];
          if(turbine){
            this.selectedTurbine = turbine;
          }
        }
      }
  }

  async updateUsers(){
    let tempUsers2 = [];
    let tempUsers = await this.managerUserDownloader.download();
    for(let u of tempUsers){
      let tempHouses = await this.householdDownloader.download(true, u.id)
      for(let h of tempHouses){
        let tempTurbines = await this.turbineDownloader.download(h.id, true, u.id)
        h.turbines = tempTurbines;
        h.calcTotProd();
      }
      u.households = tempHouses
      tempUsers2.push(u);
    }

    for(let u of tempUsers2)
    {
      for(let h of u.households){
        for(let t of h.turbines){
        }
      }

    }

    let suid = this.selectedUser.id
    let shid = this.selectedHousehold.id
    let stid = this.selectedTurbine.id

    this.users = tempUsers

    if(suid != -1){
      this.reSelect(suid, shid, stid)
    }
  }

  async updateManagerField(){
    let tempManager = await this.managerDownloader.download();
    this.coalPlant = tempManager.coalplant
    this.market = tempManager.market
  }



  async update(){
    await this.updateUsers()
    await this.updateManagerField()
  }

  onSelectUser(u: User){
    this.selectedUser = u;
    this.selectedHousehold = new Household("", -1);
  }

}
