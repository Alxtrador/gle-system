import { Component, OnInit, Input } from '@angular/core';
import { Turbine } from 'src/app/models/turbine.model';

@Component({
  selector: 'app-turbine',
  templateUrl: './turbine.component.html',
  styleUrls: ['./turbine.component.css']
})
export class TurbineComponent implements OnInit {

  @Input('turbine') turbine: Turbine;


  constructor() { }

  ngOnInit() {
  }

}
