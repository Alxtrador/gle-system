import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { ManagerUserService } from 'src/app/service/manager.user.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-profile-field',
  templateUrl: './profile-field.component.html',
  styleUrls: ['./profile-field.component.css']
})
export class ProfileFieldComponent implements OnInit {

  @Input('user') user: User = new User("", "", "", -1)
  @Input('manager') manager: boolean = false;
  uploadForm: FormGroup; 

  constructor(
    private userService: UserService,
    private managerUserService: ManagerUserService,
    private formBuilder: FormBuilder, private httpClient: HttpClient
  ) { }

  onFileChanged(event) {
    var file = event.target.files[0]
    this.user.image = file;
    this.uploadForm.get('user').setValue(file);
    this.uploadImg();
    this.user.updateImgView();
  }

  uploadImg(){
    const formData = new FormData();

    formData.append('user_id', this.user.id.toString());
    formData.append('user', this.uploadForm.get('user').value);

    if(this.manager){
      this.managerUserService.setImage(formData)
      .subscribe();
    }
    else{
      this.userService.setImage(formData)
      .subscribe();
    }
  
  }

  ngOnInit() {
    try {
      this.user.updateImgView(); 
    } catch (error) {
      
    }
    this.uploadForm = this.formBuilder.group({
      user: ['']
    });
  }

}
