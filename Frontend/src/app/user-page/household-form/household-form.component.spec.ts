import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseholdFormComponent } from './household-form.component';

describe('HouseholdFormComponent', () => {
  let component: HouseholdFormComponent;
  let fixture: ComponentFixture<HouseholdFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseholdFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseholdFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
