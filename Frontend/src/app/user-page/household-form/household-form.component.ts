import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HouseholdService } from '../../service/household.service';

export interface DialogData {
  gpsx: number;
  gpsy: number;
  name: string;
  address: string;
}

@Component({
  selector: 'app-household-form',
  templateUrl: './household-form.component.html',
  styleUrls: ['./household-form.component.css']
})
export class HouseholdFormComponent {

  gpsx: number;
  gpsy: number;
  name: string;
  address: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(HouseFormDialog, {
      width: '300px',
      data: {gpsx: this.gpsx, gpsy: this.gpsy, name: this.name, address: this.address}
    });

    dialogRef.afterClosed().subscribe();
  }
}

@Component({
  selector: 'house-form-dialog',
  templateUrl: 'house-form-dialog.html',
})
export class HouseFormDialog {

  constructor(
    private householdService: HouseholdService,
    public dialogRef: MatDialogRef<HouseFormDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onSubmit(): void {
    
    this.householdService.addHousehold([this.data.gpsx,this.data.gpsy], this.data.name, this.data.address).subscribe();
    this.dialogRef.close();
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}