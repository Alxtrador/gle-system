import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Turbine } from '../../models/turbine.model';

@Component({
  selector: 'app-list-turbines',
  templateUrl: './list-turbines.component.html',
  styleUrls: ['./list-turbines.component.css']
})
export class ListTurbinesComponent{

  @Input('selectedTurbine') selectedTurbine: Turbine;
  @Input('turbines') turbines: Turbine[] = [];

  @Output() selected = new EventEmitter<Turbine>();

  constructor(
  ) { 
  }

  selectTurbine(t: Turbine) {
    this.selected.emit(t);
  }
}
