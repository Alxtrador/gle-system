import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTurbinesComponent } from './list-turbines.component';

describe('ListTurbinesComponent', () => {
  let component: ListTurbinesComponent;
  let fixture: ComponentFixture<ListTurbinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTurbinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTurbinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
