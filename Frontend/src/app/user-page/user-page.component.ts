import { Component, OnInit} from '@angular/core';
import { Turbine } from '../models/turbine.model';
import { Household } from '../models/household.model';
import { UserService } from '../service/user.service';
import { HouseholdService } from '../service/household.service';
import { TurbineService } from '../service/turbine.service';
import { __await } from 'tslib';
import { HouseholdDownloader } from '../downloaders/household.downloader';
import { TurbineDownloader } from '../downloaders/turbine.downloader';
import { interval } from 'rxjs';
import { User } from '../models/user.model';
import { UserDownloader } from '../downloaders/user.downloader';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  user: User = new User("","","",0)

  selectedTurbine: Turbine = new Turbine("", -1, 0, 0);
  selectedHousehold: Household = new Household("", -1);
  marketPrice: number = 0;

  households: Household[] = [];
  turbines: Turbine[] = [];

  householdDownloader: HouseholdDownloader;
  turbineDownloader: TurbineDownloader;
  userDownloader: UserDownloader;

  updateFreq: number = environment.update_frequency; // in seconds
  updateTimer: number = 0;

  constructor(
    private userService: UserService, householdService: HouseholdService, turbineService: TurbineService
  ) { 
      this.householdDownloader = new HouseholdDownloader(userService, householdService);
      this.turbineDownloader = new TurbineDownloader(householdService, turbineService);
      this.userDownloader = new UserDownloader(userService);
  }


  onSelectHousehold(h: Household){
    this.selectedHousehold = h;
    this.selectedTurbine = new Turbine("", -1, 0, 0);; // deselect
    this.turbines = this.selectedHousehold.turbines;

  }

  // Selects a turbine
  onSelectTurbine(t: Turbine){
    this.selectedTurbine = t;
  }


  async getPrice(userService){
    return new Promise<number>(function(resolve, reject){
      userService.getMarketPrice().subscribe(data => {
        resolve(data.elec_price)
      })
    })
  }

  reSelect(hid, tid){
    try {
      let house = this.households.filter(x => x.id == hid)[0];
      let turbine = house.turbines.filter(x => x.id == tid)[0];
      this.onSelectHousehold(house);
      if(turbine){
        this.onSelectTurbine(turbine);
      }
    } catch (error) {
      
    }
  }

  async update(){
    let tempUser = await this.userDownloader.download()
    let tempHouseholds = await this.householdDownloader.download(false, 0)
    for(let house of tempHouseholds){
      house.turbines = await this.turbineDownloader.download(house.id, false, 0)
      house.calcTotProd()
    }
    this.marketPrice = Math.round((await this.getPrice(this.userService)) * 100)/100

    let shid = this.selectedHousehold.id;
    let stid = this.selectedTurbine.id;
  
    this.user = tempUser;
    this.households = tempHouseholds;
    this.reSelect(shid, stid);
  }

  ngOnInit() {
  }


  ngAfterContentInit(){
    this.update();
    let second = interval(1000)
    second.subscribe(() => {
      this.updateTimer = this.updateTimer + 1;
      if(this.updateTimer == this.updateFreq ){
        this.update();
        this.updateTimer = 0;
      }
    })
  }

  }
