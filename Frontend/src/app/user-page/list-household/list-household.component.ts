import { Component, EventEmitter, Output, Input } from '@angular/core';
import { Household } from '../../models/household.model';

@Component({
  selector: 'app-list-household',
  templateUrl: './list-household.component.html',
  styleUrls: ['./list-household.component.css']
})
export class ListHouseholdComponent{

  householdIDs: number[] = []
  @Input('households') households: Household[] = [];
  @Input('selectedHousehold') selectedHousehold: Household;
  @Input('manager') manager: boolean = false;
  @Input('userId') userId: number;

  @Output() selected = new EventEmitter<Household>();

  updateToggle: boolean = false;

  constructor() { }

  selectHousehold(h: Household) {
    this.selected.emit(h);
  }
}
