import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HouseholdFormComponent, HouseFormDialog } from './household-form/household-form.component';

// Used for popup
import { FormsModule } from '@angular/forms';
// npm install @angular/material @angular/cdk @angular/animations --save
// ng add @angular/material
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TurbineFormComponent, TurbineFormDialog } from './turbine-form/turbine-form.component';

import {UserPageComponent} from './user-page.component';
import { HouseholdComponent } from './household/household.component';
import { ListHouseholdComponent } from './list-household/list-household.component';
import { ListTurbinesComponent } from './list-turbines/list-turbines.component';
import { TurbineComponent } from './turbine/turbine.component';
import { StatsFieldComponent } from './stats-field/stats-field.component';
import { ProfileFieldComponent } from './profile-field/profile-field.component';

@NgModule({
  declarations: [
    UserPageComponent,
    HouseholdFormComponent,
    HouseFormDialog,
    TurbineFormComponent,
    TurbineFormDialog,
    HouseholdComponent,
    ListHouseholdComponent,
    ListTurbinesComponent,
    TurbineComponent,
    StatsFieldComponent,
    ProfileFieldComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: MatDialogRef,
    }
  ],
  entryComponents: [
    HouseFormDialog,
    TurbineFormDialog
  ],
  exports: [
    HouseholdComponent,
    ListHouseholdComponent,
    ListTurbinesComponent,
    TurbineComponent,
    StatsFieldComponent,
    ProfileFieldComponent
  ]
})
export class UserPageModule { }
