import { Component, Input } from '@angular/core';
import { Turbine } from '../../models/turbine.model';
import { Household } from '../../models/household.model';
import { HouseholdService } from 'src/app/service/household.service';

@Component({
  selector: 'app-stats-field',
  templateUrl: './stats-field.component.html',
  styleUrls: ['./stats-field.component.css']
})
export class StatsFieldComponent {

  @Input('selectedTurbine') selectedTurbine: Turbine;
  @Input('selectedHousehold') selectedHousehold: Household;
  @Input('marketPrice') marketPrice: number;

  @Input('manager') manager: boolean;

  setSellThresh(value: number){
    this.selectedHousehold.sellThresh = value;
    this.confirmSell()
  }

  setBuyThresh(value: number){
    this.selectedHousehold.buyThresh = value;
    this.confirmBuy()
  }

  confirmSell(){
    this.householdService.setSellRatio(this.selectedHousehold.id, this.selectedHousehold.sellThresh)
    .subscribe()
  }

  confirmBuy(){
    this.householdService.setBuyRatio(this.selectedHousehold.id, this.selectedHousehold.buyThresh)
    .subscribe()
  }

  constructor(
    private householdService: HouseholdService,
  ) {}

}