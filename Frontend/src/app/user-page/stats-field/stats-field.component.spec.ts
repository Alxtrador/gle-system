import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsFieldComponent } from './stats-field.component';

describe('StatsFieldComponent', () => {
  let component: StatsFieldComponent;
  let fixture: ComponentFixture<StatsFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
