import { NumberSymbol } from '@angular/common';
import {Component, Inject, Input } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Household } from 'src/app/models/household.model';
import { TurbineService } from '../../service/turbine.service';

export interface DialogData {
  name: string;
  id: number;
  gpsx: number;
  gpsy: number;
}

@Component({
  selector: 'app-turbine-form',
  templateUrl: './turbine-form.component.html',
  styleUrls: ['./turbine-form.component.css']
})
export class TurbineFormComponent {
 
  name: string;
  // id: number;
  gpsx: number;
  gpsy: number;

  @Input('selectedHousehold') household: Household;
  @Input('firstSelectFlag') flag: boolean;
  @Input('testString') testString: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(TurbineFormDialog, {
      width: '300px',
      data: {id: this.household.id, gpsx: this.gpsx, gpsy: this.gpsy, name: this.name}
    });
    
    dialogRef.afterClosed().subscribe();
  }
}


@Component({
  selector: 'app-turbine-form-dialog',
  templateUrl: 'turbine-form-dialog.html',
})
export class TurbineFormDialog {

  constructor(
    private turbineService: TurbineService,
    public dialogRef: MatDialogRef<TurbineFormDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onSubmit(): void {
    var coords = [0,0];
    // var coords = [this.data.gpsx, this.data.gpsy]
    this.turbineService.addTurbine(this.data.id, coords, this.data.name).subscribe();
    this.dialogRef.close();
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}