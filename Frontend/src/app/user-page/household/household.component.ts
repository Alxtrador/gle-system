import { Component, OnInit, Input } from '@angular/core';
import { Household } from 'src/app/models/household.model';
import { HouseholdService } from 'src/app/service/household.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-household',
  templateUrl: './household.component.html',
  styleUrls: ['./household.component.css']
})
export class HouseholdComponent implements OnInit {

  @Input('household') household: Household;
  @Input('manager') manager: boolean;
  @Input('userId') userId: number;

  uploadForm: FormGroup; 

  constructor(
    private householdService: HouseholdService,
    private formBuilder: FormBuilder, private httpClient: HttpClient
  ) { }

  onFileChanged(event) {
    var file = event.target.files[0]
    this.household.image = file;
    this.uploadForm.get('house').setValue(file);
    this.uploadImg();
    this.household.updateImgView();
  }

  uploadImg(){
    const formData = new FormData();

    formData.append('household_id', this.household.id.toString());
    formData.append('user_id', this.userId.toString());
    formData.append('household', this.uploadForm.get('house').value);

    if(this.manager){
      formData.append('user_id', this.userId.toString());
      this.householdService.setHouseImage(formData)
      .subscribe();
    }else{
      this.householdService.setImage(formData)
      .subscribe();
    }
  }

  ngOnInit() {
    try {
      this.household.updateImgView(); 
    } catch (error) {
      
    }
    this.uploadForm = this.formBuilder.group({
      house: ['']
    });
  }

}
