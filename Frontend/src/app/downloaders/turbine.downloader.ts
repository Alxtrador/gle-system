import { Turbine } from "../models/turbine.model";
import { HouseholdService } from "../service/household.service";
import { TurbineService } from "../service/turbine.service";

export class TurbineDownloader{
    private turbineService: TurbineService;
    private householdService: HouseholdService;
    constructor(householdService: HouseholdService, turbineService: TurbineService){
        this.householdService = householdService
        this.turbineService = turbineService
    }

      
    private async getTurbineIDs(householdService: HouseholdService, hID: number, manager: boolean){
        return new Promise<number[]>(function(resolve, reject){
            householdService.getTurbines(hID, manager).subscribe(data => {
                resolve(data.turbines)
            })
        })
    }

    private async getTurbineWindSpeed(service: TurbineService,hID: number, tID: number, manager: boolean, uID: number){
        return new Promise<number>(function(resolve, reject){
            service.getWindSpeed(hID, tID, manager, uID).subscribe(data => {
                resolve(data.wind_speed)
            })
        })
    }

    private async getTurbineElecProd(service: TurbineService,hID: number, tID: number, manager: boolean, uID: number){
        return new Promise<number>(function(resolve, reject){
            service.getElecProd(hID, tID, manager, uID).subscribe(data => {
                resolve(data.elec_prod)
            })
        })
    }

    private buildTurbine(tID, ws, ep): Turbine
    {
      let t = new Turbine("", tID, ep, ws)
      return t;
    }

    public async download(hID: number, manager: boolean, uID: number): Promise<Turbine[]>
    {
        let turbines: Turbine[] = [];
        let turbineIDs = await this.getTurbineIDs(this.householdService, hID, manager);
        for (let tID of turbineIDs){
            let ws = await this.getTurbineWindSpeed(this.turbineService, hID, tID, manager, uID);
            let ep = await this.getTurbineElecProd(this.turbineService, hID, tID, manager, uID);
            let turbine = this.buildTurbine(tID, Math.round(ws), Math.round(ep));
            turbines.push(turbine);
        }
        turbines.sort((a, b) => (a.id > b.id) ? 1 : -1);
        return turbines;
    }
}