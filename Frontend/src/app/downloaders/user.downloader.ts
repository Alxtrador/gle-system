import { UserService } from "../service/user.service";
import { User } from "../models/user.model";

export class UserDownloader {
    private userService: UserService;

    constructor(userService: UserService) {
        this.userService = userService;
    }

    private async getUserName(userService) {
        return new Promise<string>(function(resolve, reject) {
            userService.getUserName().subscribe(data => {
                resolve(data.user_name)
            });
        });
    }

    private async getFirstName(userService) {
        return new Promise<string>(function(resolve, reject) {
            userService.getFirstName().subscribe(data => {
                resolve(data.first_name)
            });
        });
    }

    private async getLastName(userService) {
        return new Promise<string>(function(resolve, reject) {
            userService.getLastName().subscribe(data => {
                resolve(data.last_name)
            });
        });
    }

    public async getUserImage(service:UserService){
        return new Promise<Blob>(function(resolve, reject){
            service.getImage().subscribe(data => {
                resolve(data)
            })
        })
    }

    public blobToFile = (theBlob: Blob, fileName:string): File => {
        var b: any = theBlob;
        b.lastModifiedDate = new Date();
        b.name = fileName;
        return <File>theBlob;
    }

    public async download(): Promise<User> {
        let user = new User(
            await this.getUserName(this.userService),
            await this.getFirstName(this.userService),
            await this.getLastName(this.userService),
            0
        );
        let img = await this.getUserImage(this.userService)
        user.image = this.blobToFile(img,"H" + user.id + ".png");
        user.updateImgView();

        return user;
    }
}