import { User } from "../models/user.model";
import { ManagerUserService } from "../service/manager.user.service";

export class ManagerUserDownloader{
    private managerUserService: ManagerUserService;
    constructor(managerUserService: ManagerUserService){
        this.managerUserService = managerUserService
    }

    private async getUserIDs(managerUserService){
        return new Promise<number[]>(function(resolve, reject){
            managerUserService.getUsers().subscribe(data => {
                resolve(data.users)
            })
        })
    }

    private async getLogged(managerUserService){
        return new Promise<number[]>(function(resolve, reject){
            managerUserService.getLogged().subscribe(data => {
                resolve(data.user_arr)
            })
        })
    }

    private async getUsername(managerUserService, uID){
        return new Promise<number[]>(function(resolve, reject){
            managerUserService.getUsername(uID).subscribe(data => {
                resolve(data.user_name)
            })
        })
    }

    private async getFirstName(managerUserService, uID) {
        return new Promise<string>(function(resolve, reject) {
            managerUserService.getFirstName(uID).subscribe(data => {
                resolve(data.first_name)
            });
        });
    }

    private async getLastName(managerUserService, uID) {
        return new Promise<string>(function(resolve, reject) {
            managerUserService.getLastName(uID).subscribe(data => {
                resolve(data.last_name)
            });
        });
    }

    public async getUserImage(managerUserService, uID){
        return new Promise<Blob>(function(resolve, reject){
            managerUserService.getImage(uID).subscribe(data => {
                resolve(data)
            })
        })
    }

    public async getBlocked(managerUserService, uID){
        return new Promise<boolean>(function(resolve, reject){
            managerUserService.getBlocked(uID).subscribe(data => {
                resolve(data.block)
            })
        })
    }

    public blobToFile = (theBlob: Blob, fileName:string): File => {
        var b: any = theBlob;
        b.lastModifiedDate = new Date();
        b.name = fileName;
        return <File>theBlob;
    }

    private buildUser(username, userId, loggedIn, firstName, lastName): User
    {
        let u = new User(username, firstName, lastName, userId);
        u.loggedIn = loggedIn
        return u;
    }
    
    public async download(): Promise<User[]>
    {
        let users: User[] = [];
        let userIDs = await this.getUserIDs(this.managerUserService);
        let loggedIDs = await this.getLogged(this.managerUserService);
        for (let uID of userIDs){
            let loggedIn = false;
            if(loggedIDs.indexOf(uID) > -1){
                loggedIn = true;
            }
            let username = await this.getUsername(this.managerUserService, uID)
            let firstName = await this.getFirstName(this.managerUserService, uID)
            let lastName = await this.getLastName(this.managerUserService, uID)
            let u = this.buildUser(username, uID, loggedIn, firstName, lastName)
            let img = await this.getUserImage(this.managerUserService, uID)
            let blocked = await this.getBlocked(this.managerUserService, uID)
            u.blocked = blocked
            u.image = this.blobToFile(img,"H" + u.id + ".png");
            u.updateImgView();
            users.push(u)
        }

    return users;
    }
}