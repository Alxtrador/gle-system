import { Household } from "../models/household.model";
import { HouseholdService } from "../service/household.service";
import { UserService } from "../service/user.service";

export class HouseholdDownloader{
    public householdService;
    public userService;
    constructor(userService, householdService){
        this.householdService = householdService
        this.userService = userService
    }

    public async getHouseholdIDs(service: UserService, manager: boolean, uID: number){ // user service
        return new Promise<number[]>(function(resolve, reject){
            service.getHouseholds(manager, uID).subscribe(data => {
                resolve(data.households)
            })
        })
    }

    public async getBlackoutIDs(service: HouseholdService){
        return new Promise<number[]>(function(resolve, reject){
            service.getBlackout().subscribe(data => {
                resolve(data.blackout)
            })
        })
    }

    public async getHouseholdConsumption(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise<number>(function(resolve, reject){
            service.getCons(hID, manager, uID).subscribe(data => {
                resolve(data.cons)
            })
        })
    }

    public async getHouseholdBuffer(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise<number>(function(resolve, reject){
            service.getBuffer(hID, manager, uID).subscribe(data => {
                resolve(data.buffer)
            })
        })
    }

    public async getHouseholdBuyRatio(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise<number>(function(resolve, reject){
            service.getBuyRatio(hID, manager, uID).subscribe(data => {
                resolve(data.buy_ratio)
            })
        })
    }

    public async getHouseholdSellRatio(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise<number>(function(resolve, reject){
            service.getSellRatio(hID, manager, uID).subscribe(data => {
                resolve(data.sell_ratio)
            })
        })
    }

    public async getHouseholdAddress(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise<string>(function(resolve, reject){
            service.getAddress(hID, manager, uID).subscribe(data => {
                resolve(data.address)
            })
        })
    }

    public async getHouseholdName(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise<string>(function(resolve, reject){
            service.getName(hID, manager, uID).subscribe(data => {
                resolve(data.name)
            })
        })
    }

    public async getHouseholdImage(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise(function(resolve, reject){
            service.getImage(hID, manager, uID).subscribe(data => {
                resolve(data)
            })
        })
    }

    public async getBufferLimit(service: HouseholdService, hID: number, manager: boolean, uID: number){
        return new Promise(function(resolve, reject){
            service.getBufferLimit(hID, manager, uID).subscribe(data => {
                resolve(data.buffer_limit)
            })
        })
    }

    public blobToFile = (theBlob: Blob, fileName:string): File => {
        var b: any = theBlob;
        b.lastModifiedDate = new Date();
        b.name = fileName;
  
        return <File>theBlob;
    }

    public buildHouse(hID, cons, buffer, buyRatio, sellRatio, name, address, img, buffer_limit): Household
    {
      let h = new Household("", hID);
      h.consumption = cons;
      h.buffer = buffer;
      h.buyThresh = buyRatio;
      h.sellThresh = sellRatio;
      h.name = name;
      h.address = address;
      h.image = this.blobToFile(img,"H" + hID + ".png");
      h.updateImgView();
      h.buffer_limit = buffer_limit;
      h.percentageBuff =(h.buffer/h.buffer_limit)*100;
      return h;
    }

    public async download(manager: boolean, userId): Promise<Household[]>{
        let households: Household[] = [];
        let householdIDs = await this.getHouseholdIDs(this.userService, manager, userId);
        for (let hID of householdIDs){
            let cons = await this.getHouseholdConsumption(this.householdService, hID, manager, userId)
            let buffer = await this.getHouseholdBuffer(this.householdService, hID, manager, userId)
            let buyRatio = await this.getHouseholdBuyRatio(this.householdService, hID, manager, userId)
            let sellRatio = await this.getHouseholdSellRatio(this.householdService, hID, manager, userId)
            let address = await this.getHouseholdAddress(this.householdService, hID, manager, userId)
            let name = await this.getHouseholdName(this.householdService, hID, manager, userId)
            let img = await this.getHouseholdImage(this.householdService, hID, manager, userId)
            let buffer_limit = await this.getBufferLimit(this.householdService, hID, manager, userId)
            let house = this.buildHouse(hID, Math.round(cons), Math.round(buffer), buyRatio*100, sellRatio*100, name, address, img, buffer_limit);
            if(manager){
                let blackouts = await this.getBlackoutIDs(this.householdService)
                if(blackouts.indexOf(hID)>-1){
                    house.blackout = true;
                }
            }
            households.push(house);
        }
        households.sort((a, b) => (a.id > b.id) ? 1 : -1);
        return households;
    }
}