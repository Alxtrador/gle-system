import { ManagerService } from "../service/manager.service";
import { Manager } from "../models/manager.model";
import { CoalPlant, Status } from "../models/coalplant.model";
import { Market } from "../models/market.model";

export class ManagerDownloader {
    private managerService: ManagerService;

    constructor(managerService: ManagerService) {
        this.managerService = managerService;
    }

    private async getProduction(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getCoalProduction().subscribe(data => {
                resolve(data.prod)
            });
        });
    }

    private async getRatio(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getCoalRatio().subscribe(data => {
                resolve(data.ratio)
            });
        });
    }


    private async getBuffer(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getCoalBuffer().subscribe(data => {
                resolve(data.buffer)
            });
        });
    }

    private async getMarketBuffer(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getMarketBuffer().subscribe(data => {
                resolve(data.buffer)
            });
        });
    }

    private async getStatus(managerService) {
        return new Promise<Status>(function(resolve, reject) {
            managerService.getCoalStatus().subscribe(data => {
                resolve(data.status)
            });
        });
    }

    private async getDemand(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getMarketDemand().subscribe(data => {
                resolve(data.demand)
            });
        });
    }

    private async getPrice(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getMarketPrice().subscribe(data => {
                resolve(data.elec_price)
            });
        });
    }

    private async getModelledPrice(managerService) {
        return new Promise<number>(function(resolve, reject) {
            managerService.getMarketModelledPrice().subscribe(data => {
                resolve(data.modelled_elec_price)
            });
        });
    }

    public async download(): Promise<Manager> {
        let coalPlant = new CoalPlant(
            await this.getProduction(this.managerService),
            await this.getBuffer(this.managerService),
            await this.getStatus(this.managerService),
        );

        coalPlant.buffer_ratio = await this.getRatio(this.managerService) * 100;
        let market = new Market(
            await this.getDemand(this.managerService),
            await this.getPrice(this.managerService),
            await this.getModelledPrice(this.managerService),
        );

        market.buffer = await this.getMarketBuffer(this.managerService);

        let manager = new Manager(coalPlant, market);
        return manager;
    }
}