const util = require("util");
const { spawn } = require('child_process');
const exec = util.promisify(require('child_process').exec);

async function setup() {
    let genCerts = await exec('openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./Interface/server.key -out ./Interface/server.crt -subj "/C=\'\'/ST=\'\'/L=\'\'/O=\'\'/CN=\'\'"');
    genCerts = await exec('openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./Simulator/server.key -out ./Simulator/server.crt -subj "/C=\'\'/ST=\'\'/L=\'\'/O=\'\'/CN=\'\'"');
    let intPromise = await exec('npm install --prefix Interface');
    let simPromise = await exec('npm install --prefix Simulator');
    return [genCerts, intPromise, simPromise];
}

async function main() {
    const InterfacePrgm = spawn('npm', ['start', '--prefix', 'Interface']);
    InterfacePrgm.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });
    
    InterfacePrgm.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });
    
    await new Promise(resolve => setTimeout(resolve, 3000));
    
    const SimulatorPrgm = spawn('npm', ['start', '--prefix', 'Simulator']);
    SimulatorPrgm.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });
    
    SimulatorPrgm.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });
}

setup()
    .then(values => {
        console.log(values);
        main();
    });