openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./Interface/server.key -out ./Interface/server.crt
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./Simulator/server.key -out ./Simulator/server.crt
cd Frontend
npm install
./bm.sh
cd ..
cd Interface
npm install
cd ..
cd Simulator
npm install