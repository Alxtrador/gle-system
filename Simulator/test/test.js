var chai = require("chai");
var expect = chai.expect;

const Sim = require("../src/Sim/Sim").Sim;
const Turbine = require("../src/Sim/Turbine").Turbine;
const Household = require('../src/Sim/Household.js').Household;
const Market = require("../src/Sim/Market.js").Market;

// When sampleNorms are set to one.
const turbineProd = 3739.611037037037; 

it('Demand_Low_Expect_Price_Low', function(done){
    sim = new Sim(100, 1);
    sim.demand = 0.001;
    sim.updateState();
    expect(sim.market.price).to.equal(0.1);
    done();
});

it('Demand_High_Expect_Price_High', function(done){
    sim = new Sim(100, 1);
    sim.demand = 1.5;
    sim.updateState();
    expect(sim.market.price).to.equal(150);
    done();
});

it('Demand_Above_2_Expect_Price_200', function(done){
    sim = new Sim(100, 1);
    sim.demand = 4000;
    sim.updateState();
    expect(sim.market.price).to.equal(200);
    done();
});

it('Turbine_On_Sample_1_Expect_Production', function(done){
    turbine = new Turbine(0,1,1);
    turbine.setElecProd(1);
    expect(turbine.elecProd).to.equal(turbineProd);
    done();
});

it('Turbine_Down_Expect_Production_0', function(done){
    turbine = new Turbine(0,1,1);
    turbine.down = true;
    turbine.setElecProd(1);
    expect(turbine.elecProd).to.equal(0);
    done();
});

it('Market_Extract_Not_Enough_Expect_Buffer_Empty', function(done){
    market = new Market(0);
    market.energy = 100;
    market.extractEnergy(150);
    expect(market.energy).to.equal(0);
    done();
});

it('Market_Extract_Enough_Expect_Buffer_Less', function(done){
    market = new Market(0);
    market.energy = 400;
    market.extractEnergy(150);
    expect(market.energy).to.equal(250);
    done();
});

it('Household_Extract_Not_Enough_Expect_Buffer_Empty', function(done){
    household = new Household(0,200,1,1);
    household.buffer = 100;
    household.cons = 150;
    household.getConsApplyBuffer();
    expect(household.buffer).to.equal(0);
    done();
});

it('Household_Extract_Expect_Buffer_Subtracted', function(done){
    household = new Household(0,400,1,1);
    household.buffer = 200;
    household.cons = 150;
    household.getConsApplyBuffer();
    expect(household.buffer).to.equal(50);
    done();
});

it('Household_Producing_And_Ratios_Set_Expect_Buffer_To_Production', function(done){
    household = new Household(0,200000,1,1);
    household.addTurbine(1,0,1,1);
    household.setElecProdAllTurbines(1);
    household.buffer = 0;
    household.cons = 0;
    household.getProdApplyBuffer();
    expect(household.buffer).to.equal(turbineProd);
    done();
});

it('Household_Producing_And_Ratios_Set_Expect_Buffer_To_Limit', function(done){
    household = new Household(0,1000,1,1);
    household.addTurbine(1,0,1,1);
    household.setElecProdAllTurbines(1);
    household.buffer = 0;
    household.cons = 0;
    household.getProdApplyBuffer();
    expect(household.buffer).to.equal(1000);
    done();
});

it('Blackout_Test', function(done){
    var sim = new Sim(1, 1);
    var userId = 1;
    var householdId = 1;
    sim.addUser(userId);
    sim.addHousehold(userId, householdId, 0);
    sim.updateState();
    expect(sim.blackouts[0]).to.equal(householdId);
    done();
});
