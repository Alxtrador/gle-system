const fetch = require('node-fetch');

const Turbine = require('./Turbine.js').Turbine;

const AVG_CONS = 360;
const CONS_DEV = 100;

const AVG_TEMP = 20;
const TEMP_DEV = 5;

const NORM_TEMP = 22;

class Household {
    constructor(coords, bufferLimit, storeRatio, consumeRatio) {
        this.coords = coords;
        this.turbines = new Map();
        this.buffer = 0;
        this.bufferLimit = bufferLimit;
        this.storeRatio = storeRatio;
        this.consumeRatio = consumeRatio;
        this.cons = 0;
    }

    /**
     * Adds the turbine with the specified coordinates to the household using the turbine ID as a key
     * 
     * @param {number} turbineId 
     * @param {(number, number)} coords 
     * @throws {ExistingKey} Specified key is already in use
     */
    addTurbine(turbineId, coords, daySampleNorm, yearlySampleNorm) {
        if (this.turbines.has(turbineId)) {
            throw new Error("ExistingKey");
        }
        this.turbines.set(turbineId, new Turbine(coords, daySampleNorm, yearlySampleNorm));
    }

    /**
     * Removes the specified turbine
     * 
     * @param {number} turbineId Key of turbine
     * @throws {NonexistentKey} Specified key does not exist
     */
    removeTurbine(turbineId) {
        if (!this.turbines.delete(turbineId)) {
            throw new Error("NonexistentKey");
        }
    }

    setCons(sampleNormal) {
        this.cons = sampleNormal*CONS_DEV + AVG_CONS + Math.abs(NORM_TEMP-(sampleNormal*TEMP_DEV + AVG_TEMP));
        if (this.cons < 0) {
            this.cons = 0;
        }
    }

    getCons() {
        return this.cons;
    }

    getConsApplyBuffer() {
        var retrieveFromBuffer = this.consumeRatio * this.cons;
        var retrieveFromMarket = (1-this.consumeRatio) * this.cons;
        if (retrieveFromBuffer < this.buffer) {
            this.buffer -= retrieveFromBuffer;
            return retrieveFromMarket;
        } else {
            var needToRetrieve = retrieveFromBuffer - this.buffer;
            retrieveFromMarket += needToRetrieve;
            this.buffer = 0;
            return retrieveFromMarket;
        }
    }

    /**
     * Returns the temperature of the area of the household
     * 
     * @returns {Promise<number>} The temperature
     */
    getTemp() {
        return fetch('https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/22/lat/66/data.json')
            .then(res => res.json())
            .then(json => json.timeSeries[0].parameters[1].values[0]);
    }

    /**
     * Returns the energy production of the specified turbine
     * 
     * @returns {number} Energy production
     */
    getProd(id) {
        return this.turbines.get(id).getProd();
    }

    /**
     * Returns the energy production of all turbines in the household
     * 
     * @returns {number} Energy production
     */
    getProdAll() {
        return this.turbines.forEach((key, value) => value.getProd());
    }

    getProdApplyBuffer() {
        var forMarket = 0;
        var prod = 0;
        for (const [key, turbine] of this.turbines.entries()) {
            prod += turbine.getElecProd();
        }
        this.buffer += prod*this.storeRatio;
        if (this.bufferLimit < this.buffer) {
            forMarket = this.buffer - this.bufferLimit;
            this.buffer = this.bufferLimit;
        }
        return prod * (1 - this.storeRatio) + forMarket;
    }

    /**
     * @return {Array<Turbine>} All turbines in household
     */
    getTurbines() {
        return this.turbines;
    }
    
    /**
     * 
     * @param {*} turbineId Key of turbine
     * @param {*} sampleNorm Sampled random value
     */
    getWindSpeed(turbineId, sampleNorm) {
        return this.turbines.get(turbineId).getWindSpeed(sampleNorm);
    }

    /**
     * 
     * @param {number} turbineId Key of turbine
     * @param {number} sampleNorm Sampled random value
     */
    setElecProdTurbine(turbineId, sampleNorm) {
        return this.turbines.get(turbineId).setElecProd(sampleNorm);
    }

    setElecProdAllTurbines(sampleNorm) {
        for (const [key, turbine] of this.turbines.entries()) {
            turbine.setElecProd(sampleNorm);
        }
    }

    getElecProdTurbine(turbineId) {
        return this.turbines.get(turbineId).getElecProd();
    }

    getBuyRatio() {
        return 1-this.consumeRatio;
    }

    getSellRatio() {
        return 1-this.storeRatio;
    }

    /**
     * 
     * @param {number} ratio New store ratio
     */
    setStoreRatio(ratio) {
        this.storeRatio = ratio;
    }

    /**
     * 
     * @param {number} ratio New consume ratio
     */
    setConsumeRatio(ratio) {
        this.consumeRatio = ratio;
    }

    /**
     * 
     * @return {number} Current buffer level of household
     */
    getBuffer() {
        return this.buffer;
    }

    getBufferLimit() {
        return this.bufferLimit;
    }

    storeEnergy(energy) {
        this.buffer += energy;
        if (this.buffer > this.bufferLimit) {
            this.buffer = this.bufferLimit;
        }
    }
}

module.exports = {
    Household: Household
}
