class Market {
    constructor(price) {
        this.price = price;
        this.energy = 0;
    }

    getPrice() {
        return this.price;
    }

    getValue(energy) {
        return energy * this.price;
    }

    setPrice(price) {
        this.price = price;
    }

    storeEnergy(energy) {
        this.energy += energy;
        return this.getValue(energy);
    }

    extractEnergy(neededEnergy) {
        var retrieved = 0;
        if (neededEnergy > this.energy) {
            retrieved = this.energy;
            this.energy = 0;
        } else {
            retrieved = neededEnergy;
            this.energy -= neededEnergy;
        }
        return [this.getValue(retrieved), retrieved];
    }
}

module.exports = {
    Market: Market
}
