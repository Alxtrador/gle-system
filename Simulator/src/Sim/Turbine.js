const DEV_SPEED = 1.7;
const AVG_WIND_SPEED = 5;
const AIR_DENSITY = 1.225; //kg/m3, taken from https://en.wikipedia.org/wiki/Density_of_air
const TURBINE_AREA = 10; //m3

const MAX_DOWN_TIME = 240000; //ms

class Turbine {
    constructor(coords, daySample, yearSample) {
        this.coords = coords;
        this.down = false;
        this.elecProd = 0;
        this.windSpeed = 0;
        this.daySample = daySample;
        this.yearSample = yearSample;
        setTimeout(this.toggleBreakDown, Math.floor(Math.random()*MAX_DOWN_TIME));
    }

    /**
     * Returns the electricity produced by the turbine based on the velocity of air and also
     * the status of the turbine (determined by the class's this.down)
     * 
     * @param {number} vel 
     */
    getElecProd() {
        return this.elecProd;
    }

    /**
     * Set the new value of energy produced by this turbine that is determined using the supplied
     * sample norm value
     * 
     * @param {number} sampleNorm 
     */
    setElecProd(sampleNorm) {
        this.setWindSpeed(sampleNorm);
        var vel = this.getWindSpeed();
        if (this.down) {
            this.elecProd = 0;
        } else {
            this.elecProd = (8/27)*AIR_DENSITY*(vel**3)*TURBINE_AREA; //power output formula taken from https://en.wikipedia.org/wiki/Wind_turbine#Efficiency
        }
    }

    /**
     * Get the windspeed affecting this turbine
     */
    getWindSpeed() {
        return this.windSpeed;
    }

    /**
     * Sets the speed of wind affecting this turbine based on the specified sampleNorm value
     * 
     * @param {number} sampleNorm 
     */
    setWindSpeed(sampleNorm) {
        this.windSpeed = (Math.abs(sampleNorm + this.daySample + this.yearSample)*DEV_SPEED) + AVG_WIND_SPEED;
    }

    /**
     * Inverts the value of this.down and uses setTimeout to call this function again at a random time in the future
     */
    toggleBreakDown() {
        this.down = !this.down;
        var _this = this;
        setTimeout(function () {_this.toggleBreakDown}, Math.floor(Math.random()*MAX_DOWN_TIME));
    }
}

module.exports = {
    Turbine: Turbine
}
