var Market = require("./Market.js").Market;
var PowerPlant = require("./PowerPlant.js").PowerPlant;
var User = require("./User.js").User;

const Normal = require("./../normal.js").Normal;

const BASE_COST = 100;

class Sim {
    constructor(price, sampleNorm) {
        this.powerPlant = new PowerPlant(0.5);
        this.market = new Market(price);
        this.users = new Map();
        this.demand = 0;
        this.modelPrice = 100; //base price
        this.useModelPrice = true;
        this.blackouts = [];
        this.sampleNorm = sampleNorm;
        this.daySampleNorm = Normal.sampleNormDist();
        this.yearlySampleNorm = Normal.sampleNormDist();
    }

    updateState() {
        this.blackouts = [];
        var elecProdTotal = 0;
        var elecConsTotal = 0;
        this.sampleNorm = Normal.sampleNormDist();

        var [toMarket, _] = this.powerPlant.prodPower(this.sampleNorm);
        this.market.storeEnergy(toMarket);
        
        for (const [key, user] of this.users.entries()) {
            var userElecProdTotal = 0;
            var userElecConsTotal = 0;

            for (const [key, household] of user.getHouseholds().entries()) {
                household.setCons(Normal.sampleNormDist());
                household.setElecProdAllTurbines(Normal.sampleNormDist());

                var householdProd = household.getProdApplyBuffer();
                var householdCons = household.getConsApplyBuffer();
                var powerDiff = householdProd - householdCons;

                if (powerDiff < 0) { //Check if additional power from the market is needed
                    userElecConsTotal += Math.abs(powerDiff);
                    var [_, retrieved] = this.market.extractEnergy(Math.abs(powerDiff));
                    if (retrieved < Math.abs(powerDiff)) { //if not enough power from the market can be acquired, the user is having a blackout
                        this.blackouts.push(key);
                    }
                    household.setStoreRatio(1);
                } else {
                    if (user.blocked) {
                        household.storeEnergy(powerDiff);
                    } else {
                        userElecProdTotal += powerDiff;
                        this.market.storeEnergy(powerDiff);
                    }
                    household.setConsumeRatio(1);
                }
            }

            elecProdTotal += userElecProdTotal;
            elecConsTotal += userElecConsTotal;
        }
        
        if(!(elecConsTotal == 0 && elecProdTotal == 0)){
            this.demand = elecConsTotal/elecProdTotal;
        }

        if (this.demand > 2) {
            this.demand = 2;
        }

        this.modelPrice = BASE_COST * this.demand;
        if (this.useModelPrice) {
            this.market.setPrice(this.modelPrice);
        }
    }

    /**
     * Adds a new user to the map of users with userId acting as key
     * 
     * @param {number} userId Unique User ID
     * @throws {ExistingKey} Specified key is already in use
     */
    addUser(userId) {
        if (this.users.has(userId)) {
            throw new Error("ExistingKey")
        }
        this.users.set(userId, new User());
    }

    /**
     * Adds a household to the specified user using the given household ID as a key
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     */
    addHousehold(userId, householdId, coords) {
        this.users.get(userId).addHousehold(householdId, coords);
    }

    /**
     * Adds a turbine to the specified user's household using the given turbine ID as a key
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     * @param {number} turbineId Unique Turbine ID
     */
    addTurbine(userId, householdId, turbineId, coords) {
        this.users.get(userId).addTurbine(householdId, turbineId, coords, this.daySampleNorm, this.yearlySampleNorm);
    }

    /**
     * Remove the specified user
     * 
     * @param {number} userId Unique User ID
     * @throws {NonexistentKey} Specified key does not exist
     */
    removeUser(userId) {
        if (!this.users.delete(userId)) {
            throw new Error("NonexistentKey");
        }
    }

    /**
     * Remove the specified user's household
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     * @throws {NonexistentKey} Specified key does not exist
     */
    removeHousehold(userId, householdId) {
        try {
            this.users.get(userId).removeHousehold(householdId);
        } catch (err) {
            throw err;
        }
    }

    /**
     * Remove the specified user's household's turbine
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     * @param {number} turbineId Unique Turbine ID
     * @throws {NonexistentKey} Specified key does not exist
     */
    removeTurbine(userId, householdId, turbineId) {
        try {
            this.users.get(userId).getHousehold(householdId).removeTurbine(turbineId);
        } catch (err) {
            throw err;
        }
    }

    getPowerPlantProd() {
        var [_, energyProd] = this.powerPlant.prodPower(this.sampleNorm);
        return energyProd;
    }

    getPowerPlantStatus() {
        return this.powerPlant.on;
    }

    getDemand() {
        return this.demand;
    }

    getBlackouts() {
        return this.blackouts;
    }

    getModelPrice() {
        return this.modelPrice;
    }

    getBuffer(userId, householdId) {
        try {
            return this.users.get(userId).getHousehold(householdId).getBuffer();
        } catch (err) {
            throw err;
        }
    }

    getPowerPlantBuffer() {
        return this.powerPlant.getBattery();
    }
    
    /**
     * Returns the ID's of the user's households
     * 
     * @param {number} userId Unique User ID
     * @return {Array<number>}
     */
    getHouseholds(userId) {
        try {
            var householdKeys = [];
            for (const [key, value] of this.users.get(userId).getHouseholds().entries()) {
                householdKeys.push(key);
            }
            return householdKeys;
        } catch (err) {
            throw err;
        }
    }

    getTurbines(userId, householdId) {
        try {
            var turbineKeys = [];
            for (const [key, value] of this.users.get(userId).getHousehold(householdId).getTurbines()) {
                turbineKeys.push(key);
            }
            return turbineKeys;
        } catch (err) {
            throw err;
        }
    }

    /**
     * Returns the current price of electricity according to the market
     * 
     * @returns {number} The current price on electricity
     */
    getElecPrice() {
        return this.market.getPrice();
    }

    /**
     * Returns the consumption of the specified user's household
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     * @returns {Promise<number>} household's consumption
     */
    getConsHousehold(userId, householdId) {
        return this.users.get(userId).getCons(householdId);
    }

    /**
     * Returns the electricity production of the specified user's household's turbine
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     * @param {number} turbineId Unique Turbine ID
     * @returns {number} turbine's electricity production
     */
    getElecProdTurbine(userId, householdId, turbineId) {
        return this.users.get(userId).getHousehold(householdId).getElecProdTurbine(turbineId);
    }

    /**
     * Returns the wind speed of the specified user's household's turbine
     * 
     * @param {number} userId Unique User ID
     * @param {number} householdId Unique Household ID
     * @param {number} turbineId Unique Turbine ID
     */
    getWindSpeed(userId, householdId, turbineId) {
        return this.users.get(userId).getHousehold(householdId).getWindSpeed(turbineId);
    }

    /**
     * Returns the specified user's balance
     * 
     * @param {number} userId Unique User ID
     * @returns {number} user's balance
     */
    getBalance(userId) {
        return this.users.get(userId).getBalance();
    }

    getBuyRatio(userId, householdId) {
        return this.users.get(userId).getHousehold(householdId).getBuyRatio();
    }

    getSellRatio(userId, householdId) {
        return this.users.get(userId).getHousehold(householdId).getSellRatio();
    }

    getBufferLimit(userId, householdId) {
        return this.users.get(userId).getHousehold(householdId).getBufferLimit();
    }

    getMarketAccess(userId) {
        return this.users.get(userId).getMarketAccess();
    }

    getProdRatio() {
        return this.powerPlant.rate;
    }

    getMarketBuffer() {
        return this.market.energy;
    }

    startPowerPlant() {
        this.powerPlant.startUp();
    }

    stopPowerPlant() {
        this.powerPlant.powerDown();
    }

    blockMarket(userId, time) {
        this.users.get(userId).blockMarketAccess(time);
    }

    /**
     * Only in use when the power plant is on. Controls amount of energy sent
     * to buffer and to market
     * 
     * @param {number} newRatio 
     */
    setProdRatio(newRatio) {
        this.powerPlant.rate = newRatio / 100;
    }

    /**
     * Sets the specified user's balance to that of the input value
     * 
     * @param {number} userId Unique User ID
     * @param {number} balance Sets the user's balance to this value
     */
    setBalance(userId, balance) {
        this.users.get(userId).setBalance(balance);
    }

    /**
     * Sets the market price to that of the input value
     * 
     * @param {number} price Set's the market price of electricity to this value
     */
    setPrice(price) {
        this.market.setPrice(price);
    }

    setSellBufferRatio(userId, householdId, newRatio) {
        this.users.get(userId).getHousehold(householdId).setStoreRatio(1-newRatio);
    }

    setBuyBufferRatio(userId, householdId, newRatio) {
        this.users.get(userId).getHousehold(householdId).setConsumeRatio(1-newRatio);
    }

    togglePrice() {
        this.useModelPrice = !this.useModelPrice;
    }

    init(json) {
        var data = json.Data;

        for (let userData of data) {
            this.addUser(userData.userId);
            for (let householdId of userData.householdIds) {
                this.addHousehold(userData.userId, householdId, [10,10]);

                for (let turbineId of userData.turbineIds) {
                    var actualHouseholdId = turbineId[0];
                    var actualTurbineId = turbineId[1];

                    if (actualHouseholdId == householdId) {
                        this.addTurbine(userData.userId, actualHouseholdId, actualTurbineId, [10,10]);
                    }
                }
            }
        }
    }
}

module.exports = {
    Sim: Sim
}
