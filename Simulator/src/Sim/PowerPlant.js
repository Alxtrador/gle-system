POWER_DEV = 50;
AVG_POWER = 10000;

const OFF = 0;
const STARTING = 1;
const ON = 2;

class PowerPlant {
    constructor(rate) {
        this.on = OFF;
        this.rate = rate;
        this.buffer = 0;
    }

    startUp() {
        this.on = STARTING;
        var _this = this;
        setTimeout(function () { _this.on = ON }, 30000)
    }

    powerDown() {
        this.on = OFF;
    }

    getBattery() {
        return this.buffer;
    }

    prodPower(sampleNorm) {
        var toMarket = 0;
        var produced = 0;
        if (this.on == ON) {
            var energy = (sampleNorm*POWER_DEV) + AVG_POWER;
            this.buffer += (this.rate * energy);
            toMarket = energy * (1 - this.rate);
            produced = energy;
        } else {
            toMarket = this.buffer;
            produced = 0;
            this.buffer = 0;
        }
        return [toMarket, produced];
    }
}

module.exports = {
    PowerPlant: PowerPlant
}
