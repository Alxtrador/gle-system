const Household = require('./Household.js').Household;

class User {
    constructor() {
        this.households = new Map();
        this.balance = 0;
        this.blocked = false;
    }

    /**
     * Adds a household to this.households
     * 
     * @param {number} id Unique household ID
     * @throws {ExistingKey} Specified key is already in use
     */
    addHousehold(id, coords) {
        if (this.households.has(id)) {
            throw new Error("ExistingKey");
        }
        this.households.set(id, new Household(coords, 10000, 0.5, 0.5));
    }

    /**
     * Adds a turbine to the household using the provided turbine ID as a key
     * 
     * @param {number} householdId 
     * @param {number} turbineId 
     * @throws {ExistingKey} Specified key is already in use
     */
    addTurbine(householdId, turbineId, coords, daySampleNorm, yearlySampleNorm) {
        try {
            this.households.get(householdId).addTurbine(turbineId, coords, daySampleNorm, yearlySampleNorm);
        } catch (err) {
            throw err;
        }
    }

    /**
     * Removes the specified household
     * 
     * @param {number} id
     * @throws {NonexistentKey} Specified key does not exist
     */
    removeHousehold(id) {
        if (!this.households.delete(id)) {
            throw new Error("NonexistentKey");
        }
    }

    /**
     * Removes the specified household's turbine
     * 
     * @param {number} householdId 
     * @param {number} turbineId
     * @throws {NonexistentKey} Specified key does not exist
     */
    removeTurbine(householdId, turbineId) {
        try {
            this.households.get(householdId).removeTurbine(turbineId);
        } catch (err) {
            throw err;
        }  
    }

    /**
     * 
     * @param {Promise<number>} householdId 
     */
    getCons(householdId, sampleNorm) {
        return this.households.get(householdId).getCons(sampleNorm);
    }

    getHousehold(householdId) {
        return this.households.get(householdId);
    }

    getHouseholds() {
        return this.households;
    }

    getMarketAccess() {
        return this.blocked;
    }
    
    
    /**
     * Returns the User's current balance
     * 
     * @returns {number} User's balance
     */
    getBalance() {
        return this.balance;
    }

    /**
     * Adds the specified amount to the User's balance
     * 
     * @param {number} money Value to add to balance
     */
    addBalance(money) {
        this.balance += money;
    }

    blockMarketAccess(time) {
        this.blocked = true;
        var _this = this;
        setTimeout(function () { _this.blocked = false }, time*1000);
    }
    
    allowMarketAccess() {
        this.blocked = false;
    }
}

module.exports = {
    User: User
}
