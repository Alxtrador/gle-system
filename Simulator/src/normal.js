class Normal {
    //Uses Box–Muller transform, https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform 
    static sampleNormDist() {
        var epsilon = 0;
        var u1 = 0;
        var u2 = 0;
        while (u1 <= epsilon && u2 <= epsilon) {
            u1 = Math.random();
            u2 = Math.random();
        }
        var z1 = Math.sqrt(-2*Math.log(u1))*Math.cos(2*Math.PI*u2);
        var z2 = Math.sqrt(-2*Math.log(u1))*Math.sin(2*Math.PI*u2);
        return z1;
    }
}

module.exports = {
    Normal: Normal
}
