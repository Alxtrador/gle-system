process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const Sim = require("./Sim").Sim;
const Normal = require("./normal.js").Normal;

const express = require('express');
const fetch = require('node-fetch');
const https = require('https');
const fs = require('fs');
const path = require('path');
const app = express();

const key = fs.readFileSync(path.join(__dirname, '../server.key'), 'utf8');
const cert = fs.readFileSync(path.join(__dirname, '../server.crt'), 'utf8');
const credentials = {key: key, cert: cert};

sim = new Sim(100, Normal.sampleNormDist());

fetch('https://localhost:8443/init/init_all')
    .then(res => res.json())
    .then(json => sim.init(json));

var add_routes = require('./Routes').add_routes(sim);
var api_routes = require('./Routes').api_routes(sim);
var delete_routes = require('./Routes').delete_routes(sim);
var control_routes = require('./Routes').control_routes(sim);

app.use('/add', add_routes);
app.use('/api', api_routes);
app.use('/delete', delete_routes);
app.use('/control', control_routes);

setInterval(function() { sim.updateState(Normal.sampleNormDist()); }, 10000);

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(8444, 'localhost');