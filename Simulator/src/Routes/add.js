var bodyParser = require('body-parser');

module.exports = function factory(sim) {
    const router = require('express').Router();
    router.use(bodyParser.json());

    /**
     * Takes user id from the interface and adds a user to the sim.
     */
    router.post('/user/:user_id', (req, res) => {
        try {
            sim.addUser(parseInt(req.params.user_id));
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }

    });
    
    /**
     * Takes user id and household id from the interface and adds a household to the sim.
     */
    router.post('/user/:user_id/household/:household_id/coords/:coords', (req, res) => {
        try {
            sim.addHousehold(parseInt(req.params.user_id), parseInt(req.params.household_id), parseInt(req.params.coords));
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });
    

    /**
     * Takes user id, household id, turbine id and turbine coords from the interface and adds a turbine in the sim.
     */
    router.post('/user/:user_id/household/:household_id/turbine/:turbine_id', (req, res) => {
        try {
            sim.addTurbine(parseInt(req.params.user_id), parseInt(req.params.household_id), parseInt(req.params.turbine_id), [10, 10]);
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    return router;
}