module.exports = function factory(sim) {
    const router = require('express').Router();

    router.post('/power_plant/prod/', (req, res) => {
        try {
            sim.startPowerPlant();
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    router.post('/power_plant/prod/stop', (req, res) => {
        try {
            sim.stopPowerPlant();
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    router.post('/buffer/ratio/:new_ratio', (req, res) => {
        try {
            sim.setProdRatio(parseInt(req.params.new_ratio));
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    router.post('/elec_price/:new_price', (req, res) => {
        try {
            sim.setPrice(parseInt(req.params.new_price));
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    router.post('/user/:user_id/market_block/time/:time', (req, res) => {
        try {
            sim.blockMarket(parseInt(req.params.user_id), parseInt(req.params.time));
            res.status(201).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    router.post('/user/:user_id/household/:household_id/buffer/sell/:new_ratio', (req, res) => {
        try {
            sim.setSellBufferRatio(parseInt(req.params.user_id), parseInt(req.params.household_id), parseFloat(req.params.new_ratio));
            res.status(201).end();
        } catch (err) {
            console.error(err);
            res.status(400).end();
        }
    });

    router.post('/user/:user_id/household/:household_id/buffer/buy/:new_ratio', (req, res) => {
        try {
            sim.setBuyBufferRatio(parseInt(req.params.user_id), parseInt(req.params.household_id), parseFloat(req.params.new_ratio));
            res.status(201).end();
        } catch (err) {
            console.error(err);
            res.status(400).end();
        }
    });

    router.post('/toggle/price', (req, res) => {
        try {
            sim.togglePrice();
            res.status(201).end();
        } catch (err) {
            console.error(err);
            res.status(400).end();
        }
    });

    return router;
}