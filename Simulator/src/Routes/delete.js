module.exports = function factory(sim) {
    const router = require('express').Router();

    router.delete('/user/:used_id', (req, res) => {
        try {
            sim.removeUser(parseInt(req.params.used_id));
            res.status(204).end();
        } catch (err) {
            res.status(400).end();
        }
    });
    
    router.delete('/household/user/:user_id/household/:household_id', (req, res) => {
        try {
            sim.removeHousehold(parseInt(req.params.user_id), parseInt(req.params.household_id));
            res.status(204).end();
        } catch (err) {
            res.status(400).end();
        }
    });
    
    router.delete('/turbine/user/:user_id/household/:household_id/turbine/:turbine_id', (req, res) => {
        try {
            sim.removeTurbine(parseInt(req.params.user_id), parseInt(req.params.household_id), parseInt(req.params.turbine_id));
            res.status(204).end();
        } catch (err) {
            res.status(400).end();
        }
    });

    return router;
}