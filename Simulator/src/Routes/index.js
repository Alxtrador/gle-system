const add_routes = require('./add.js');
const api_routes = require('./api.js');
const delete_routes = require('./delete.js');
const control_routes = require('./control.js');

module.exports = {
    api_routes: api_routes,
    add_routes: add_routes,
    delete_routes: delete_routes,
    control_routes: control_routes
};