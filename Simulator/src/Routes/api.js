module.exports = function factory(sim) {
    const router = require('express').Router();

    router.get('/cons/user/:user_id/household/:household_id', (req, res) => {
        res.end(JSON.stringify({
            cons: sim.getConsHousehold(parseInt(req.params.user_id), parseInt(req.params.household_id))
        }));
    });
    
    router.get('/wind_speed/user/:user_id/household/:household_id/turbine/:turbine_id', (req, res) => {
        res.end(JSON.stringify({
            wind_speed: sim.getWindSpeed(parseInt(req.params.user_id), parseInt(req.params.household_id), parseInt(req.params.turbine_id))
        }));
    });
    
    router.get('/elec_prod/user/:user_id/household/:household_id/turbine/:turbine_id', (req, res) => {
        res.end(JSON.stringify({
            elec_prod: sim.getElecProdTurbine(parseInt(req.params.user_id), parseInt(req.params.household_id), parseInt(req.params.turbine_id))
        }));
    });
    
    router.get('/elec_price', (req, res) => {
        res.end(JSON.stringify({
            elec_price: sim.getElecPrice()
        }));
    });

    router.get('/households/user/:user_id', (req, res) => {
        res.end(JSON.stringify({
            households: sim.getHouseholds(parseInt(req.params.user_id))
        }));
    });

    router.get('/turbines/user/:user_id/household/:household_id', (req, res) => {
        res.end(JSON.stringify({
            turbines: sim.getTurbines(parseInt(req.params.user_id), parseInt(req.params.household_id))
        }));
    });

    router.get('/buffer/user/:user_id/household/:household_id', (req, res) => {
        res.end(JSON.stringify({
            buffer: sim.getBuffer(parseInt(req.params.user_id), parseInt(req.params.household_id))
        }));
    });

    router.get('/power_plant/prod', (req, res) => {
        res.end(JSON.stringify({
            prod: sim.getPowerPlantProd()
        }));
    });

    router.get('/power_plant/status', (req, res) => {
        res.end(JSON.stringify({
            status: sim.getPowerPlantStatus()
        }));
    });

    router.get('/power_plant/buffer', (req, res) => {
        res.end(JSON.stringify({
            power_plant_buffer: sim.getPowerPlantBuffer()
        }));
    });

    router.get('/power_plant/ratio', (req, res) => {
        res.end(JSON.stringify({
            ratio: sim.getProdRatio()
        }));
    });

    router.get('/market/demand', (req, res) => {
        res.end(JSON.stringify({
            demand: sim.getDemand()
        }));
    });

    router.get('/market/buffer', (req, res) => {
        res.end(JSON.stringify({
            buffer: sim.getMarketBuffer()
        }));
    });

    router.get('/households/blackout', (req, res) => {
        res.end(JSON.stringify({
            blackout: sim.getBlackouts()
        }));
    });

    router.get('/modelled_elec_price', (req, res) => {
        res.end(JSON.stringify({
            modelled_elec_price: sim.getModelPrice()
        }));
    });

    router.get('/user/:user_id/household/:household_id/buy_ratio', (req, res) => {
        res.end(JSON.stringify({
            buy_ratio: sim.getBuyRatio(parseInt(req.params.user_id), parseInt(req.params.household_id))
        }));
    });

    router.get('/user/:user_id/household/:household_id/sell_ratio', (req, res) => {
        res.end(JSON.stringify({
            sell_ratio: sim.getSellRatio(parseInt(req.params.user_id), parseInt(req.params.household_id))
        }));
    });

    router.get('/user/:user_id/household/:household_id/buffer_limit', (req, res) => {
        res.end(JSON.stringify({
            buffer_limit: sim.getBufferLimit(parseInt(req.params.user_id), parseInt(req.params.household_id))
        }));
    });

    router.get('/user/:user_id/block', (req, res) => {
        res.end(JSON.stringify({
            block: sim.getMarketAccess(parseInt(req.params.user_id))
        }));
    });

    return router;
}