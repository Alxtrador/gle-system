(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-navbar></app-navbar>\n\n\n<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-form/login-form.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-form/login-form.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit(loginForm.value)\">\n        <div class=\"form-group\">\n            <label for=\"uname\">User Name</label>\n            <input type=\"text\" class=\"form-control\" id=\"uname\" formControlName=\"uname\" required>\n        </div>\n\n        <div class=\"form-group\">\n            <label for=\"pass\">Password</label>\n            <input type=\"password\" class=\"form-control\" id=\"pass\" formControlName=\"pass\" required>\n        </div>\n\n        <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n    </form>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/edit-user-field/edit-user-field.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/edit-user-field/edit-user-field.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\n    <div class=\"container\">\n        <form [formGroup]=\"editForm\" (ngSubmit)=\"edit(editForm.value)\">\n\n            <div class=\"form-group\">\n                <label for=\"username\">User Name</label>\n                <input type=\"text\" class=\"form-control\" id=\"username\" formControlName=\"username\">\n            </div>\n\n            <div class=\"form-group\">\n                <label for=\"firstName\">First Name</label>\n                <input type=\"text\" class=\"form-control\" id=\"firstName\" formControlName=\"firstName\">\n            </div>\n\n            <div class=\"form-group\">\n                <label for=\"lastName\">Last Name</label>\n                <input type=\"text\" class=\"form-control\" id=\"lastName\" formControlName=\"lastName\">\n            </div>\n\n            <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n        </form>\n    </div>\n\n    <div class=\"container\">\n        <form [formGroup]=\"blockForm\" (ngSubmit)=\"block(blockForm.value)\">\n\n            <div class=\"form-group\">\n                <label for=\"blocktime\">Time(seconds)</label>\n                <p>{{blocktimeVar}}</p>\n                <input #blockRange formControlName=\"blocktime\" id=\"blocktime\" type=\"range\" class=\"form-control custom-range\" min=\"10\" max=\"100\" (change)=\"changeBlockvar(blockRange.value)\"> \n            </div>\n\n            <button type=\"submit\" class=\"btn btn-danger\">Block</button>\n        </form>\n    </div>\n\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/list-users/list-users.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/list-users/list-users.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\n    <div class=\"col-xs-12\">\n        <a class=\"list-group-item clearfix border\" *ngFor=\"let u of users\" (click)=\"selectUser(u)\" [ngClass]=\"{'bg-info': u.id === selectedUser.id}\">\n            <app-user [user]='u'></app-user>\n        </a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/list-users/user/user.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/list-users/user/user.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<span class=\"pull-left\">\n    <h4 class=\"list-group-item-heading\"> Id: {{user.id}} UserName: {{user.userName}}</h4>\n\n    <div>\n        <p *ngIf=\"user.loggedIn else elseBlock\" style=\"color: green;\">Logged In</p>\n        <ng-template #elseBlock>\n            <p style=\"color: red;\">Logged Out</p>\n        </ng-template>\n    </div>\n\n</span>\n<div class=\"pull-right\">\n    <h4 class=\"list-group-item-heading\" *ngIf=\"user.blocked == true\"> BLOCKED </h4>\n    <button class=\"btn btn-danger\" (click)=\"deleteUser()\">Delete</button>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/manager-field/manager-field.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/manager-field/manager-field.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"row\">\n\n    <div class=\"col-xs-12 border\">\n        <h2>Coal Plant Data</h2>\n        <p>Status            : {{coalPlant.statusText}}</p>\n        <p>Production        : {{coalPlant.production}}</p>\n        <p>Buffer            : {{coalPlant.buffer}}</p>\n    </div>\n\n    <div class=\"col-xs-12 border\">\n        <button class=\"btn btn-success\" (click)=\"startPowerPlant()\">Start</button>\n        <button class=\"btn btn-danger\" (click)=\"stopPowerPlant()\">Stop</button>\n    </div>\n\n    <div class=\"col-xs-12 border\">\n        <h3>Market/Buffer Control</h3>\n        <h4>How much of produced power do you want to store in the buffer? (the rest will be sold to the market)</h4>\n        <p>Store: {{coalPlant.buffer_ratio}} %</p>\n        <input #bufferRange1 value=\"{{coalPlant.buffer_ratio}}\" type=\"range\" class=\"custom-range\" id=\"customRange1\" min=\"0\" max=\"100\" (change)=\"setRatio(bufferRange1.value)\"> \n    </div>\n\n    <div class=\"col-xs-12 border\">\n        <h2>Market Data: </h2>\n        <p>Demand         : {{market.demand}}</p>\n        <p>Modeled Price  : {{market.modeled_price}}</p>\n        <p>Price          : {{market.price}}</p>\n        <p>Market Buffer  : {{market.buffer}}</p>\n    </div>\n\n    <div class=\"col-xs-12 border\">\n        <input #priceinput type=\"text\" class=\"form-control\">\n        <button class=\"btn btn-primary\" (click)=\"setMarketPrice(priceinput.value)\">Set Price</button>\n    \n        <button class=\"btn btn-primary\" (click)=\"togglePrice()\">Toggle Price</button>\n    </div>\n\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/manager-page.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/manager-page.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"container-fluid\">\n\n    <div class=\"progress\" style=\"height: 10px;\" *ngIf=\"!manager\">\n        <div class=\"progress-bar progress-bar bg-info\" role=\"progressbar\" [ngStyle]=\"{width: ((updateTimer/updateFreq)*100) + '%'}\" attr.aria-valuenow=\"{{updateTimer}}\" aria-valuemin=\"0\" attr.aria-valuemax=\"updateFreq\"></div>\n    </div>\n\n    <div class=\"row\" style=\"height: 800px\">\n        \n        <div class=\"col border\">\n            <app-manager-field [coalPlant]=\"coalPlant\" [market]=\"market\"></app-manager-field>\n        </div>\n        \n        <div class=\"col border\">      \n            <h2>Users</h2>\n            <app-list-users [users]=\"users\" [selectedUser]=\"selectedUser\" (selected)=\"onSelectUser($event)\"></app-list-users>\n        </div>\n\n        <div class=\"col-4 border\" *ngIf=\"selectedUser.id >= 0\">      \n            <h2>Edit User {{selectedUser.id}}</h2>\n            <app-edit-user-field [userId]=\"selectedUser.id\" ></app-edit-user-field>\n        </div>\n\n\n    </div>\n\n    <app-user-field *ngIf=\"selectedUser.id >= 0\" [turbines]=\"selectedHousehold.turbines\" [selectedTurbine]=\"selectedTurbine\" [selectedHousehold]=\"selectedHousehold\" (selectedH)=\"onSelectHousehold($event)\" (selectedT)=\"onSelectTurbine($event)\" [selectedUser]=\"selectedUser\"></app-user-field>\n\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/user-field/user-field.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/user-field/user-field.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- \n<div class=\"row\">\n    <h2>User {{user.name}} Id {{user.id}}</h2>\n    <div class=\"col-xs-12 border\">\n        <div>\n            <h4>Change username</h4>\n            <input #namefield type=\"text\">\n            <div>\n                <button type=\"button\" class=\"btn btn-primary\" (click)=\"editUsername(namefield.value)\">Submit</button>\n            </div>\n            <div class=\"pull-right\">\n                <button type=\"button\" *ngIf=\"user.blocked else elseBlock\" class=\"btn btn-success\" (click)=\"setBlocked(false)\">Un-block</button>\n                <ng-template #elseBlock>\n                    <button type=\"button\" class=\"btn btn-warning\" (click)=\"setBlocked(true)\">Block</button>    \n                </ng-template>\n                <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteUser()\">Delete</button>    \n            </div>\n        </div>\n        <div class=\"col border\">\n            <div class=\"pull-left\">\n                <app-list-household [selectedUser]='user.id'></app-list-household>\n            </div>\n            <div class=\"pull-right\">\n                <app-list-turbines></app-list-turbines>\n            </div>\n        </div>\n    </div>\n</div> -->\n\n<!-- This container makes columns of lists -->\n<div class=\"row\" style=\"height: 800px\">\n    <div class=\"col border\">\n        <h2>Profile</h2>\n        <app-profile-field [user]=\"user\" [manager]=\"true\"></app-profile-field>\n    </div>\n    <div class=\"col border\">\n        <h2>Households</h2>\n        <div class=\"h-75 overflow-auto\">\n            <!-- Here are the households listed-->\n            <app-list-household [manager]=\"true\" [userId]=\"user.id\"[selectedHousehold]=\"selectedHousehold\" [households]=\"user.households\" (selected)=\"onSelectHousehold($event)\"></app-list-household>\n        </div>\n    </div>\n    <div class=\"col border\">\n        <h2>Turbines</h2>\n        <div class=\"h-75 overflow-auto\">\n            <!-- Here are the turbines listed-->\n            <app-list-turbines  [selectedTurbine]=\"selectedTurbine\" [turbines]=\"turbines\" (selected)=\"onSelectTurbine($event)\"></app-list-turbines>\n        </div>\n    </div>\n    <div class=\"col border\">\n        <app-stats-field [manager]=\"manager\" [marketPrice]=\"marketPrice\" [selectedTurbine]=\"selectedTurbine\" [selectedHousehold]=\"selectedHousehold\"></app-stats-field>\n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/navbar/navbar.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/navbar/navbar.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n    <a class=\"navbar-brand\" href=\"#\">GLE-Systems</a>\n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n        <ul class=\"navbar-nav\">\n            <li class=\"nav-item\">\n                \n                <a *ngIf=\"isManager\" class=\"nav-link\" routerLink=\"/manager_page\">Manager Page</a>\n                <a *ngIf=\"!isManager\" class=\"nav-link\" routerLink=\"/user_page\">User Page</a>\n                \n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/login\">Login</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/signup\">Signup</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/user_page\">User Page</a>\n            </li>\n            <li>\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"logout()\">Logout</button>\n            </li>\n        </ul>\n    </div>\n</nav>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup-form/signup-form.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup-form/signup-form.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <h2>{{responseText}}</h2>\n\n    <form [formGroup]=\"signupForm\" (ngSubmit)=\"onSubmit(signupForm.value)\">\n        <div class=\"form-group\">\n            <label for=\"fname\">First Name</label>\n            <input type=\"text\" class=\"form-control\" id=\"fname\" formControlName=\"fname\" required>\n        </div>\n\n        <div class=\"form-group\">\n            <label for=\"lname\">Last Name</label>\n            <input type=\"text\" class=\"form-control\" id=\"lname\" formControlName=\"lname\" required>\n        </div>\n\n        <div class=\"form-group\">\n            <label for=\"uname\">User Name</label>\n            <input type=\"text\" class=\"form-control\" id=\"uname\" formControlName=\"uname\" required>\n        </div>\n\n        <div class=\"form-group\">\n            <label for=\"pass\">Password</label>\n            <input type=\"password\" class=\"form-control\" id=\"pass\" formControlName=\"pass\" required>\n        </div>\n\n        <div class=\"form-group\">\n            <label for=\"pass2\">Repeat Password</label>\n            <input type=\"password\" class=\"form-control\" id=\"pass2\" formControlName=\"pass2\" required>\n        </div>\n\n        <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n    </form>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household-form/house-form-dialog.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household-form/house-form-dialog.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 mat-dialog-title>Input Household Information</h1>\n<div mat-dialog-content>\n  <mat-form-field>\n      <p>Name: </p>\n    <input matInput [(ngModel)]=\"data.name\">\n  </mat-form-field>\n  <mat-form-field>\n    <p>Address: </p>\n  <input matInput [(ngModel)]=\"data.address\">\n</mat-form-field>\n  <mat-form-field>\n      Coord X:\n    <input matInput [(ngModel)]=\"data.gpsx\">\n  </mat-form-field>\n  <mat-form-field>\n      Coord Y:\n    <input matInput [(ngModel)]=\"data.gpsy\">\n  </mat-form-field>\n</div>\n<div mat-dialog-actions>\n  <button class=\"btn btn-default\" mat-button (click)=\"onCancel()\">Cancel</button>\n  <button class=\"btn btn-success\" mat-button (click)=\"onSubmit()\" cdkFocusInitial>Submit</button>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household-form/household-form.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household-form/household-form.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button class=\"btn btn-success btn-lg center-block\" mat-raised-button (click)=\"openDialog()\">+ Add Household</button>\n            ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household/household.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household/household.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<span class=\"pull-left\">\n    <h4 class=\"list-group-item-heading\"> Id: {{household.id}} Name: {{household.name}} Address: {{household.address}}</h4>\n</span>\n<div class=\"pull-right\">\n    <div class=\"pull-left\">\n        <input\n            style=\"display: none\"\n            type=\"file\" (change)=\"onFileChanged($event)\"\n            #fileInput>\n        <button *ngIf=\"!manager\" type =\"button\" class=\"btn btn-primary\" (click)=\"fileInput.click()\">Upload Image</button>\n    </div>\n    \n    <div class=\"pull-right\">\n        <div class =\"align-top-right\">\n            <h4 class=\"list-group-item-heading\" *ngIf=\"household.blackout == true\"> BLACKOUT </h4>\n        </div>\n        <div class=\"align-bottom-right\">\n            <img [src]=\"household.imgURL\" height=\"200\">\n        </div>\n    </div>    \n</div>       ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/list-household/list-household.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/list-household/list-household.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\n    <div class=\"col-xs-12\">\n        <a class=\"list-group-item clearfix\" *ngFor=\"let h of households\" (click)=\"selectHousehold(h)\" [ngClass]=\"{'bg-info': h.id === selectedHousehold.id}\">\n            <app-household [userId]=\"userId\" [manager]=\"manager\" [household]='h'></app-household>\n        </a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/list-turbines/list-turbines.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/list-turbines/list-turbines.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\n    <div class=\"col-xs-12\">\n        <a class=\"list-group-item clearfix\" *ngFor=\"let t of turbines\" (click)=\"selectTurbine(t)\" [ngClass]=\"{'bg-info': t.id === selectedTurbine.id}\">\n            <app-turbine [turbine]='t'></app-turbine>\n        </a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/profile-field/profile-field.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/profile-field/profile-field.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p> Id: {{user.id}}</p>\n<p> Username: {{user.userName}}</p>\n<p> First Name: {{user.firstName}}</p>\n<p> Last Name: {{user.lastName}}</p>\n\n<div class=\"pull-right\">\n    <div class=\"pull-left\">\n        <input\n            style=\"display: none\"\n            type=\"file\" (change)=\"onFileChanged($event)\"\n            #fileInput>\n        <button type =\"button\" class=\"btn btn-primary\" (click)=\"fileInput.click()\">Upload Image</button>\n    </div>\n    \n    <div class=\"pull-right\">\n        <!--<h4 class=\"list-group-item-heading\"> IMAGE </h4>\n        -->\n        <img [src]=\"user.imgURL\" height=\"200\">\n    </div>    \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/stats-field/stats-field.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/stats-field/stats-field.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\n    <div class=\"col-xs-12 border\">\n        <h4>Market Price is $ {{marketPrice}}</h4>\n    </div>\n    <div class=\"col-xs-12 border\" *ngIf=\"selectedHousehold.id >= 0\">\n        <h3>Household Data: {{selectedHousehold.name}}</h3>\n        <p>Household Production Is      : {{selectedHousehold.production}}</p>\n        <p>Household Consumption Is     : {{selectedHousehold.consumption}}</p>\n        <p>Household Net-Production Is  : {{selectedHousehold.production - selectedHousehold.consumption}}</p>\n        <p>Household Buffer Is          : {{selectedHousehold.buffer}} out of {{selectedHousehold.buffer_limit}}</p>\n        <div class=\"progress\" style=\"height: 40px;\">\n            <div class=\"progress-bar progress-bar\" [ngClass]=\"{'bg-success':selectedHousehold.percentageBuff>50, 'bg-warning':selectedHousehold.percentageBuff<50 && selectedHousehold.percentageBuff>25, 'bg-danger':selectedHousehold.percentageBuff<25}\" role=\"progressbar\" [ngStyle]=\"{width: selectedHousehold.percentageBuff + '%'}\"   attr.aria-valuenow=\"{{selectedHousehold.buffer}}\" aria-valuemin=\"0\" attr.aria-valuemax=\"{{selectedHousehold.buffer_limit}}\"></div>\n        </div>\n    </div>\n    \n    <div class=\"col-xs-12 border\" *ngIf=\"(selectedHousehold.id >= 0) && !manager\" >\n        <h3>Sell Control Enabled</h3>\n        <h4>How much of produced power do you want to sell to the market? (the rest will be stored in your buffer)</h4>\n        <p>Store: {{selectedHousehold.sellThresh}} %</p>\n        <input #bufferRange1 value=\"{{selectedHousehold.sellThresh}}\" [disabled]=\"((selectedHousehold.production-selectedHousehold.consumption) < 0)\" type=\"range\" class=\"custom-range\" id=\"customRange1\" min=\"0\" max=\"100\" (change)=\"setSellThresh(bufferRange1.value)\"> \n    </div>\n\n    <div class=\"col-xs-12 border\" *ngIf=\"(selectedHousehold.id >= 0) && !manager\">\n        <h3>Buy Control Enabled</h3>\n        <h4>How much of required power do you want to buy from the market? (the rest will be used from your buffer)</h4>\n        <p>Use: {{selectedHousehold.buyThresh}} %</p>\n        <input #bufferRange2 value=\"{{selectedHousehold.buyThresh}}\" [disabled]=\"((selectedHousehold.production-selectedHousehold.consumption) > 0)\" type=\"range\" class=\"custom-range\" id=\"customRange1\" min=\"0\" max=\"100\" (change)=\"setBuyThresh(bufferRange2.value)\"> \n    </div>\n\n    <div class=\"col-xs-12 border\" *ngIf=\"selectedTurbine.id >= 0\">\n        <h2>Turbine Data ID: {{selectedTurbine.id}}</h2>\n        <p>Turbine Windpeed     : {{selectedTurbine.windspeed}}</p>\n        <p>Turbine Production   : {{selectedTurbine.production}}</p>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine-form/turbine-form-dialog.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine-form/turbine-form-dialog.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 mat-dialog-title>Adding Turbine To Household</h1>\n<div mat-dialog-content>\n  <mat-form-field>\n      <p>Name: </p>\n    <input matInput [(ngModel)]=\"data.name\">\n  </mat-form-field>\n  <!-- <mat-form-field>\n      Coord X:\n    <input matInput [(ngModel)]=\"data.gpsx\">\n  </mat-form-field>\n  <mat-form-field>\n      Coord Y:\n    <input matInput [(ngModel)]=\"data.gpsy\">\n  </mat-form-field> -->\n</div>\n<div mat-dialog-actions>\n  <button class=\"btn btn-default\" mat-button (click)=\"onCancel()\">Cancel</button>\n  <button class=\"btn btn-success\" mat-button (click)=\"onSubmit()\" cdkFocusInitial>Add</button>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine-form/turbine-form.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine-form/turbine-form.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button class=\"btn btn-success btn-lg center-block\" mat-raised-button (click)=\"openDialog()\" [disabled]=\"household.id < 0\">+ Add Turbine</button>\n            ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine/turbine.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine/turbine.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<span class=\"pull-left\">\n    <h4 class=\"list-group-item-heading\"> Id: {{turbine.id}} </h4>\n    <p>Windpeed: {{turbine.windspeed}}</p>\n    <p>Production: {{turbine.production}}</p>\n</span>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/user-page.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/user-page.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- This container makes columns of lists -->\n<div class=\"container-fluid\">\n\n    <div class=\"progress\" style=\"height: 10px;\" *ngIf=\"!manager\">\n        <div class=\"progress-bar progress-bar bg-info\" role=\"progressbar\" [ngStyle]=\"{width: ((updateTimer/updateFreq)*100) + '%'}\" attr.aria-valuenow=\"{{updateTimer}}\" aria-valuemin=\"0\" attr.aria-valuemax=\"updateFreq\"></div>\n    </div>\n\n    <div class=\"row\" style=\"height: 800px\">\n        <div class=\"col border\">\n            <h2>Profile</h2>\n            <app-profile-field [user]=\"user\"></app-profile-field>\n        </div>\n        <div class=\"col border\">\n            <h2>Households</h2>\n            <div class=\"h-75 overflow-auto\">\n                <!-- Here are the households listed-->\n                <app-list-household [selectedHousehold]=\"selectedHousehold\" [households]=\"households\" (selected)=\"onSelectHousehold($event)\"></app-list-household>\n            </div>\n            <div class=\"col-xs-12\">\n                <app-household-form></app-household-form>\n            </div>\n        </div>\n        <div class=\"col border\">\n            <h2>Turbines</h2>\n            <div class=\"h-75 overflow-auto\">\n                <!-- Here are the turbines listed-->\n                <app-list-turbines  [selectedTurbine]=\"selectedTurbine\" [turbines]=\"turbines\" (selected)=\"onSelectTurbine($event)\"></app-list-turbines>\n                \n            </div>\n            <div class=\"col-xs-12\">\n                <app-turbine-form [selectedHousehold]=\"selectedHousehold\"></app-turbine-form>\n            </div>\n        </div>\n        <!-- This Div is for stats and controller -->\n        <div class=\"col border\">\n            <app-stats-field [marketPrice]=\"marketPrice\" [selectedTurbine]=\"selectedTurbine\" [selectedHousehold]=\"selectedHousehold\"></app-stats-field>\n        </div>\n   \n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'Frontend';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login-form/login-form.component */ "./src/app/login-form/login-form.component.ts");
/* harmony import */ var _signup_form_signup_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./signup-form/signup-form.component */ "./src/app/signup-form/signup-form.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./user-page/user-page.component */ "./src/app/user-page/user-page.component.ts");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _user_page_user_page_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./user-page/user-page.module */ "./src/app/user-page/user-page.module.ts");
/* harmony import */ var _manager_page_manager_page_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./manager-page/manager-page.module */ "./src/app/manager-page/manager-page.module.ts");
/* harmony import */ var _manager_page_manager_page_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./manager-page/manager-page.component */ "./src/app/manager-page/manager-page.component.ts");












// Used for popup
//import { FormsModule } from '@angular/forms';
// npm install @angular/material @angular/cdk @angular/animations --save
// ng add @angular/material







let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
            _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_8__["LoginFormComponent"],
            _signup_form_signup_form_component__WEBPACK_IMPORTED_MODULE_9__["SignupFormComponent"],
            _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_10__["NavbarComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forRoot([
                { path: 'user_page', component: _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_11__["UserPageComponent"] },
                { path: 'login', component: _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_8__["LoginFormComponent"] },
                { path: 'signup', component: _signup_form_signup_form_component__WEBPACK_IMPORTED_MODULE_9__["SignupFormComponent"] },
                { path: 'manager_page', component: _manager_page_manager_page_component__WEBPACK_IMPORTED_MODULE_18__["ManagerPageComponent"] },
            ]),
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatInputModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["BrowserAnimationsModule"],
            _user_page_user_page_module__WEBPACK_IMPORTED_MODULE_16__["UserPageModule"],
            _manager_page_manager_page_module__WEBPACK_IMPORTED_MODULE_17__["ManagerPageModule"]
        ],
        providers: [
            {
                provide: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__["MatDialogRef"],
            }
        ],
        entryComponents: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/downloaders/household.downloader.ts":
/*!*****************************************************!*\
  !*** ./src/app/downloaders/household.downloader.ts ***!
  \*****************************************************/
/*! exports provided: HouseholdDownloader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseholdDownloader", function() { return HouseholdDownloader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_household_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/household.model */ "./src/app/models/household.model.ts");


class HouseholdDownloader {
    constructor(userService, householdService) {
        this.blobToFile = (theBlob, fileName) => {
            var b = theBlob;
            b.lastModifiedDate = new Date();
            b.name = fileName;
            return theBlob;
        };
        this.householdService = householdService;
        this.userService = userService;
    }
    getHouseholdIDs(service, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getHouseholds(manager, uID).subscribe(data => {
                    resolve(data.households);
                });
            });
        });
    }
    getBlackoutIDs(service) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getBlackout().subscribe(data => {
                    resolve(data.blackout);
                });
            });
        });
    }
    getHouseholdConsumption(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getCons(hID, manager, uID).subscribe(data => {
                    resolve(data.cons);
                });
            });
        });
    }
    getHouseholdBuffer(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getBuffer(hID, manager, uID).subscribe(data => {
                    resolve(data.buffer);
                });
            });
        });
    }
    getHouseholdBuyRatio(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getBuyRatio(hID, manager, uID).subscribe(data => {
                    resolve(data.buy_ratio);
                });
            });
        });
    }
    getHouseholdSellRatio(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getSellRatio(hID, manager, uID).subscribe(data => {
                    resolve(data.sell_ratio);
                });
            });
        });
    }
    getHouseholdAddress(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getAddress(hID, manager, uID).subscribe(data => {
                    resolve(data.address);
                });
            });
        });
    }
    getHouseholdName(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getName(hID, manager, uID).subscribe(data => {
                    resolve(data.name);
                });
            });
        });
    }
    getHouseholdImage(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getImage(hID, manager, uID).subscribe(data => {
                    resolve(data);
                });
            });
        });
    }
    getBufferLimit(service, hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getBufferLimit(hID, manager, uID).subscribe(data => {
                    resolve(data.buffer_limit);
                });
            });
        });
    }
    buildHouse(hID, cons, buffer, buyRatio, sellRatio, name, address, img, buffer_limit) {
        let h = new _models_household_model__WEBPACK_IMPORTED_MODULE_1__["Household"]("", hID);
        h.consumption = cons;
        h.buffer = buffer;
        h.buyThresh = buyRatio;
        h.sellThresh = sellRatio;
        h.name = name;
        h.address = address;
        h.image = this.blobToFile(img, "H" + hID + ".png");
        h.updateImgView();
        h.buffer_limit = buffer_limit;
        h.percentageBuff = (h.buffer / h.buffer_limit) * 100;
        return h;
    }
    download(manager, userId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let households = [];
            let householdIDs = yield this.getHouseholdIDs(this.userService, manager, userId);
            for (let hID of householdIDs) {
                let cons = yield this.getHouseholdConsumption(this.householdService, hID, manager, userId);
                let buffer = yield this.getHouseholdBuffer(this.householdService, hID, manager, userId);
                let buyRatio = yield this.getHouseholdBuyRatio(this.householdService, hID, manager, userId);
                let sellRatio = yield this.getHouseholdSellRatio(this.householdService, hID, manager, userId);
                let address = yield this.getHouseholdAddress(this.householdService, hID, manager, userId);
                let name = yield this.getHouseholdName(this.householdService, hID, manager, userId);
                let img = yield this.getHouseholdImage(this.householdService, hID, manager, userId);
                let buffer_limit = yield this.getBufferLimit(this.householdService, hID, manager, userId);
                let house = this.buildHouse(hID, Math.round(cons), Math.round(buffer), buyRatio * 100, sellRatio * 100, name, address, img, buffer_limit);
                if (manager) {
                    let blackouts = yield this.getBlackoutIDs(this.householdService);
                    if (blackouts.indexOf(hID) > -1) {
                        house.blackout = true;
                    }
                }
                households.push(house);
            }
            households.sort((a, b) => (a.id > b.id) ? 1 : -1);
            return households;
        });
    }
}


/***/ }),

/***/ "./src/app/downloaders/manager.downloader.ts":
/*!***************************************************!*\
  !*** ./src/app/downloaders/manager.downloader.ts ***!
  \***************************************************/
/*! exports provided: ManagerDownloader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerDownloader", function() { return ManagerDownloader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_manager_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/manager.model */ "./src/app/models/manager.model.ts");
/* harmony import */ var _models_coalplant_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/coalplant.model */ "./src/app/models/coalplant.model.ts");
/* harmony import */ var _models_market_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/market.model */ "./src/app/models/market.model.ts");




class ManagerDownloader {
    constructor(managerService) {
        this.managerService = managerService;
    }
    getProduction(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getCoalProduction().subscribe(data => {
                    resolve(data.prod);
                });
            });
        });
    }
    getRatio(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getCoalRatio().subscribe(data => {
                    resolve(data.ratio);
                });
            });
        });
    }
    getBuffer(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getCoalBuffer().subscribe(data => {
                    resolve(data.buffer);
                });
            });
        });
    }
    getMarketBuffer(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getMarketBuffer().subscribe(data => {
                    resolve(data.buffer);
                });
            });
        });
    }
    getStatus(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getCoalStatus().subscribe(data => {
                    resolve(data.status);
                });
            });
        });
    }
    getDemand(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getMarketDemand().subscribe(data => {
                    resolve(data.demand);
                });
            });
        });
    }
    getPrice(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getMarketPrice().subscribe(data => {
                    resolve(data.elec_price);
                });
            });
        });
    }
    getModelledPrice(managerService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerService.getMarketModelledPrice().subscribe(data => {
                    resolve(data.modelled_elec_price);
                });
            });
        });
    }
    download() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let coalPlant = new _models_coalplant_model__WEBPACK_IMPORTED_MODULE_2__["CoalPlant"](yield this.getProduction(this.managerService), yield this.getBuffer(this.managerService), yield this.getStatus(this.managerService));
            coalPlant.buffer_ratio = (yield this.getRatio(this.managerService)) * 100;
            let market = new _models_market_model__WEBPACK_IMPORTED_MODULE_3__["Market"](yield this.getDemand(this.managerService), yield this.getPrice(this.managerService), yield this.getModelledPrice(this.managerService));
            market.buffer = yield this.getMarketBuffer(this.managerService);
            let manager = new _models_manager_model__WEBPACK_IMPORTED_MODULE_1__["Manager"](coalPlant, market);
            return manager;
        });
    }
}


/***/ }),

/***/ "./src/app/downloaders/manager.user.downloader.ts":
/*!********************************************************!*\
  !*** ./src/app/downloaders/manager.user.downloader.ts ***!
  \********************************************************/
/*! exports provided: ManagerUserDownloader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerUserDownloader", function() { return ManagerUserDownloader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/user.model */ "./src/app/models/user.model.ts");


class ManagerUserDownloader {
    constructor(managerUserService) {
        this.blobToFile = (theBlob, fileName) => {
            var b = theBlob;
            b.lastModifiedDate = new Date();
            b.name = fileName;
            return theBlob;
        };
        this.managerUserService = managerUserService;
    }
    getUserIDs(managerUserService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getUsers().subscribe(data => {
                    resolve(data.users);
                });
            });
        });
    }
    getLogged(managerUserService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getLogged().subscribe(data => {
                    resolve(data.user_arr);
                });
            });
        });
    }
    getUsername(managerUserService, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getUsername(uID).subscribe(data => {
                    resolve(data.user_name);
                });
            });
        });
    }
    getFirstName(managerUserService, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getFirstName(uID).subscribe(data => {
                    resolve(data.first_name);
                });
            });
        });
    }
    getLastName(managerUserService, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getLastName(uID).subscribe(data => {
                    resolve(data.last_name);
                });
            });
        });
    }
    getUserImage(managerUserService, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getImage(uID).subscribe(data => {
                    resolve(data);
                });
            });
        });
    }
    getBlocked(managerUserService, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                managerUserService.getBlocked(uID).subscribe(data => {
                    resolve(data.block);
                });
            });
        });
    }
    buildUser(username, userId, loggedIn, firstName, lastName) {
        let u = new _models_user_model__WEBPACK_IMPORTED_MODULE_1__["User"](username, firstName, lastName, userId);
        u.loggedIn = loggedIn;
        return u;
    }
    download() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let users = [];
            let userIDs = yield this.getUserIDs(this.managerUserService);
            let loggedIDs = yield this.getLogged(this.managerUserService);
            for (let uID of userIDs) {
                let loggedIn = false;
                if (loggedIDs.indexOf(uID) > -1) {
                    loggedIn = true;
                }
                let username = yield this.getUsername(this.managerUserService, uID);
                let firstName = yield this.getFirstName(this.managerUserService, uID);
                let lastName = yield this.getLastName(this.managerUserService, uID);
                let u = this.buildUser(username, uID, loggedIn, firstName, lastName);
                let img = yield this.getUserImage(this.managerUserService, uID);
                let blocked = yield this.getBlocked(this.managerUserService, uID);
                u.blocked = blocked;
                u.image = this.blobToFile(img, "H" + u.id + ".png");
                u.updateImgView();
                users.push(u);
            }
            return users;
        });
    }
}


/***/ }),

/***/ "./src/app/downloaders/turbine.downloader.ts":
/*!***************************************************!*\
  !*** ./src/app/downloaders/turbine.downloader.ts ***!
  \***************************************************/
/*! exports provided: TurbineDownloader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurbineDownloader", function() { return TurbineDownloader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_turbine_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/turbine.model */ "./src/app/models/turbine.model.ts");


class TurbineDownloader {
    constructor(householdService, turbineService) {
        this.householdService = householdService;
        this.turbineService = turbineService;
    }
    getTurbineIDs(householdService, hID, manager) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                householdService.getTurbines(hID, manager).subscribe(data => {
                    resolve(data.turbines);
                });
            });
        });
    }
    getTurbineWindSpeed(service, hID, tID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getWindSpeed(hID, tID, manager, uID).subscribe(data => {
                    resolve(data.wind_speed);
                });
            });
        });
    }
    getTurbineElecProd(service, hID, tID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getElecProd(hID, tID, manager, uID).subscribe(data => {
                    resolve(data.elec_prod);
                });
            });
        });
    }
    buildTurbine(tID, ws, ep) {
        let t = new _models_turbine_model__WEBPACK_IMPORTED_MODULE_1__["Turbine"]("", tID, ep, ws);
        return t;
    }
    download(hID, manager, uID) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let turbines = [];
            let turbineIDs = yield this.getTurbineIDs(this.householdService, hID, manager);
            for (let tID of turbineIDs) {
                let ws = yield this.getTurbineWindSpeed(this.turbineService, hID, tID, manager, uID);
                let ep = yield this.getTurbineElecProd(this.turbineService, hID, tID, manager, uID);
                let turbine = this.buildTurbine(tID, Math.round(ws), Math.round(ep));
                turbines.push(turbine);
            }
            turbines.sort((a, b) => (a.id > b.id) ? 1 : -1);
            return turbines;
        });
    }
}


/***/ }),

/***/ "./src/app/downloaders/user.downloader.ts":
/*!************************************************!*\
  !*** ./src/app/downloaders/user.downloader.ts ***!
  \************************************************/
/*! exports provided: UserDownloader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDownloader", function() { return UserDownloader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/user.model */ "./src/app/models/user.model.ts");


class UserDownloader {
    constructor(userService) {
        this.blobToFile = (theBlob, fileName) => {
            var b = theBlob;
            b.lastModifiedDate = new Date();
            b.name = fileName;
            return theBlob;
        };
        this.userService = userService;
    }
    getUserName(userService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                userService.getUserName().subscribe(data => {
                    resolve(data.user_name);
                });
            });
        });
    }
    getFirstName(userService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                userService.getFirstName().subscribe(data => {
                    resolve(data.first_name);
                });
            });
        });
    }
    getLastName(userService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                userService.getLastName().subscribe(data => {
                    resolve(data.last_name);
                });
            });
        });
    }
    getUserImage(service) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                service.getImage().subscribe(data => {
                    resolve(data);
                });
            });
        });
    }
    download() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let user = new _models_user_model__WEBPACK_IMPORTED_MODULE_1__["User"](yield this.getUserName(this.userService), yield this.getFirstName(this.userService), yield this.getLastName(this.userService), 0);
            let img = yield this.getUserImage(this.userService);
            user.image = this.blobToFile(img, "H" + user.id + ".png");
            user.updateImgView();
            return user;
        });
    }
}


/***/ }),

/***/ "./src/app/login-form/login-form.component.css":
/*!*****************************************************!*\
  !*** ./src/app/login-form/login-form.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luLWZvcm0vbG9naW4tZm9ybS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/login-form/login-form.component.ts":
/*!****************************************************!*\
  !*** ./src/app/login-form/login-form.component.ts ***!
  \****************************************************/
/*! exports provided: LoginFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginFormComponent", function() { return LoginFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");




let LoginFormComponent = class LoginFormComponent {
    constructor(formBuilder, userService) {
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.loginForm = this.formBuilder.group({
            uname: '',
            pass: ''
        });
    }
    onSubmit(loginData) {
        this.userService.login(loginData).subscribe();
    }
    ngOnInit() {
    }
};
LoginFormComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] }
];
LoginFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-form/login-form.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login-form.component.css */ "./src/app/login-form/login-form.component.css")).default]
    })
], LoginFormComponent);



/***/ }),

/***/ "./src/app/manager-page/edit-user-field/edit-user-field.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/manager-page/edit-user-field/edit-user-field.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbmFnZXItcGFnZS9lZGl0LXVzZXItZmllbGQvZWRpdC11c2VyLWZpZWxkLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/manager-page/edit-user-field/edit-user-field.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/manager-page/edit-user-field/edit-user-field.component.ts ***!
  \***************************************************************************/
/*! exports provided: EditUserFieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserFieldComponent", function() { return EditUserFieldComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_service_manager_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/manager.user.service */ "./src/app/service/manager.user.service.ts");




let EditUserFieldComponent = class EditUserFieldComponent {
    constructor(formBuilder, managerUserService) {
        this.formBuilder = formBuilder;
        this.managerUserService = managerUserService;
        this.userId = 0;
        this.blocktimeVar = 0;
        this.editForm = this.formBuilder.group({
            username: '',
            firstName: '',
            lastName: '',
        });
        this.blockForm = this.formBuilder.group({
            blocktime: '',
        });
    }
    edit(editData) {
        this.managerUserService.editFirstName(editData.firstName, this.userId).subscribe();
        this.managerUserService.editLastName(editData.lastName, this.userId).subscribe();
        this.managerUserService.editUsername(editData.username, this.userId).subscribe();
    }
    block(blockData) {
        this.managerUserService.setBlock(this.userId, blockData.blocktime).subscribe();
    }
    changeBlockvar(time) {
        this.blocktimeVar = time;
    }
    ngOnInit() {
    }
};
EditUserFieldComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_app_service_manager_user_service__WEBPACK_IMPORTED_MODULE_3__["ManagerUserService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('userId')
], EditUserFieldComponent.prototype, "userId", void 0);
EditUserFieldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-user-field',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit-user-field.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/edit-user-field/edit-user-field.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit-user-field.component.css */ "./src/app/manager-page/edit-user-field/edit-user-field.component.css")).default]
    })
], EditUserFieldComponent);



/***/ }),

/***/ "./src/app/manager-page/list-users/list-users.component.css":
/*!******************************************************************!*\
  !*** ./src/app/manager-page/list-users/list-users.component.css ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbmFnZXItcGFnZS9saXN0LXVzZXJzL2xpc3QtdXNlcnMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/manager-page/list-users/list-users.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/manager-page/list-users/list-users.component.ts ***!
  \*****************************************************************/
/*! exports provided: ListUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListUsersComponent", function() { return ListUsersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_models_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/user.model */ "./src/app/models/user.model.ts");



let ListUsersComponent = class ListUsersComponent {
    constructor() {
        this.selectedUser = new src_app_models_user_model__WEBPACK_IMPORTED_MODULE_2__["User"]("", "", "", -1);
        this.users = [];
        this.selected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    selectUser(u) {
        this.selected.emit(u);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedUser')
], ListUsersComponent.prototype, "selectedUser", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('users')
], ListUsersComponent.prototype, "users", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], ListUsersComponent.prototype, "selected", void 0);
ListUsersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-users',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./list-users.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/list-users/list-users.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./list-users.component.css */ "./src/app/manager-page/list-users/list-users.component.css")).default]
    })
], ListUsersComponent);



/***/ }),

/***/ "./src/app/manager-page/list-users/user/user.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/manager-page/list-users/user/user.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbmFnZXItcGFnZS9saXN0LXVzZXJzL3VzZXIvdXNlci5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/manager-page/list-users/user/user.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/manager-page/list-users/user/user.component.ts ***!
  \****************************************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_service_manager_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/manager.user.service */ "./src/app/service/manager.user.service.ts");



let UserComponent = class UserComponent {
    constructor(managerUserService) {
        this.managerUserService = managerUserService;
    }
    ngOnInit() {
    }
    deleteUser() {
        this.managerUserService.deleteUser(this.user.id).subscribe();
    }
};
UserComponent.ctorParameters = () => [
    { type: src_app_service_manager_user_service__WEBPACK_IMPORTED_MODULE_2__["ManagerUserService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('user')
], UserComponent.prototype, "user", void 0);
UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./user.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/list-users/user/user.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./user.component.css */ "./src/app/manager-page/list-users/user/user.component.css")).default]
    })
], UserComponent);



/***/ }),

/***/ "./src/app/manager-page/manager-field/manager-field.component.css":
/*!************************************************************************!*\
  !*** ./src/app/manager-page/manager-field/manager-field.component.css ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbmFnZXItcGFnZS9tYW5hZ2VyLWZpZWxkL21hbmFnZXItZmllbGQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/manager-page/manager-field/manager-field.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/manager-page/manager-field/manager-field.component.ts ***!
  \***********************************************************************/
/*! exports provided: ManagerFieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerFieldComponent", function() { return ManagerFieldComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_models_coalplant_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/coalplant.model */ "./src/app/models/coalplant.model.ts");
/* harmony import */ var src_app_models_market_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/market.model */ "./src/app/models/market.model.ts");
/* harmony import */ var src_app_service_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/manager.service */ "./src/app/service/manager.service.ts");





let ManagerFieldComponent = class ManagerFieldComponent {
    constructor(managerService) {
        this.managerService = managerService;
        this.coalPlant = new src_app_models_coalplant_model__WEBPACK_IMPORTED_MODULE_2__["CoalPlant"](0, 0, 0);
        this.market = new src_app_models_market_model__WEBPACK_IMPORTED_MODULE_3__["Market"](0, 0, 0);
    }
    setRatio(ratio) {
        this.managerService.setCoalRatio(ratio).subscribe();
    }
    setMarketPrice(price) {
        this.market.price = price;
        this.managerService.setMarketPrice(price).subscribe();
    }
    startPowerPlant() {
        this.coalPlant.status = 1;
        this.managerService.startCoalPlant().subscribe();
    }
    stopPowerPlant() {
        this.coalPlant.status = 0;
        this.managerService.stopCoalPlant().subscribe();
    }
    togglePrice() {
        this.managerService.togglePrice().subscribe();
    }
};
ManagerFieldComponent.ctorParameters = () => [
    { type: src_app_service_manager_service__WEBPACK_IMPORTED_MODULE_4__["ManagerService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('coalPlant')
], ManagerFieldComponent.prototype, "coalPlant", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('market')
], ManagerFieldComponent.prototype, "market", void 0);
ManagerFieldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-manager-field',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./manager-field.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/manager-field/manager-field.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./manager-field.component.css */ "./src/app/manager-page/manager-field/manager-field.component.css")).default]
    })
], ManagerFieldComponent);



/***/ }),

/***/ "./src/app/manager-page/manager-page.component.css":
/*!*********************************************************!*\
  !*** ./src/app/manager-page/manager-page.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbmFnZXItcGFnZS9tYW5hZ2VyLXBhZ2UuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/manager-page/manager-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/manager-page/manager-page.component.ts ***!
  \********************************************************/
/*! exports provided: ManagerPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerPageComponent", function() { return ManagerPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _downloaders_household_downloader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../downloaders/household.downloader */ "./src/app/downloaders/household.downloader.ts");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/user.model */ "./src/app/models/user.model.ts");
/* harmony import */ var _service_household_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/household.service */ "./src/app/service/household.service.ts");
/* harmony import */ var _service_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/manager.service */ "./src/app/service/manager.service.ts");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
/* harmony import */ var _service_turbine_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/turbine.service */ "./src/app/service/turbine.service.ts");
/* harmony import */ var _downloaders_turbine_downloader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../downloaders/turbine.downloader */ "./src/app/downloaders/turbine.downloader.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _models_household_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../models/household.model */ "./src/app/models/household.model.ts");
/* harmony import */ var _user_field_user_field_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./user-field/user-field.component */ "./src/app/manager-page/user-field/user-field.component.ts");
/* harmony import */ var _models_turbine_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../models/turbine.model */ "./src/app/models/turbine.model.ts");
/* harmony import */ var _service_manager_user_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../service/manager.user.service */ "./src/app/service/manager.user.service.ts");
/* harmony import */ var _models_coalplant_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../models/coalplant.model */ "./src/app/models/coalplant.model.ts");
/* harmony import */ var _models_market_model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../models/market.model */ "./src/app/models/market.model.ts");
/* harmony import */ var _downloaders_manager_user_downloader__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../downloaders/manager.user.downloader */ "./src/app/downloaders/manager.user.downloader.ts");
/* harmony import */ var _downloaders_manager_downloader__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../downloaders/manager.downloader */ "./src/app/downloaders/manager.downloader.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");



















let ManagerPageComponent = class ManagerPageComponent {
    constructor(managerService, userService, householdService, turbineService, managerUserService) {
        this.selectedTurbine = new _models_turbine_model__WEBPACK_IMPORTED_MODULE_12__["Turbine"]("", -1, 0, 0);
        this.selectedHousehold = new _models_household_model__WEBPACK_IMPORTED_MODULE_10__["Household"]("", -1);
        this.selectedUser = new _models_user_model__WEBPACK_IMPORTED_MODULE_3__["User"]("", "", "", -1);
        this.users = [];
        this.updateFreq = src_environments_environment__WEBPACK_IMPORTED_MODULE_18__["environment"].update_frequency; // in seconds
        this.updateTimer = 0;
        this.coalPlant = new _models_coalplant_model__WEBPACK_IMPORTED_MODULE_14__["CoalPlant"](0, 0, 0);
        this.market = new _models_market_model__WEBPACK_IMPORTED_MODULE_15__["Market"](0, 0, 0);
        this.managerUserDownloader = new _downloaders_manager_user_downloader__WEBPACK_IMPORTED_MODULE_16__["ManagerUserDownloader"](managerUserService);
        this.householdDownloader = new _downloaders_household_downloader__WEBPACK_IMPORTED_MODULE_2__["HouseholdDownloader"](userService, householdService);
        this.turbineDownloader = new _downloaders_turbine_downloader__WEBPACK_IMPORTED_MODULE_8__["TurbineDownloader"](householdService, turbineService);
        this.managerDownloader = new _downloaders_manager_downloader__WEBPACK_IMPORTED_MODULE_17__["ManagerDownloader"](managerService);
    }
    ngOnInit() {
    }
    ngAfterContentInit() {
        this.update();
        let second = Object(rxjs__WEBPACK_IMPORTED_MODULE_9__["interval"])(1000);
        second.subscribe(() => {
            this.updateTimer = this.updateTimer + 1;
            if (this.updateTimer == this.updateFreq) {
                this.update();
                this.updateTimer = 0;
            }
        });
    }
    onSelectHousehold(h) {
        this.selectedHousehold = h;
        this.selectedTurbine = new _models_turbine_model__WEBPACK_IMPORTED_MODULE_12__["Turbine"]("", -1, 0, 0);
        ; // deselect
    }
    onSelectTurbine(t) {
        this.selectedTurbine = t;
    }
    reSelect(uid, hid, tid) {
        let userFiltered = this.users.filter(x => x.id == uid);
        if (userFiltered.length == 0) {
            this.selectedUser = new _models_user_model__WEBPACK_IMPORTED_MODULE_3__["User"]("", "", "", -1);
            this.selectedHousehold = new _models_household_model__WEBPACK_IMPORTED_MODULE_10__["Household"]("", -1);
            this.selectedTurbine = new _models_turbine_model__WEBPACK_IMPORTED_MODULE_12__["Turbine"]("", -1, 0, 0);
            return;
        }
        else {
            var user = userFiltered[0];
            this.selectedUser = user;
            let house = user.households.filter(x => x.id == hid)[0];
            if (house) {
                this.selectedHousehold = house;
                let turbine = house.turbines.filter(x => x.id == tid)[0];
                if (turbine) {
                    this.selectedTurbine = turbine;
                }
            }
        }
    }
    updateUsers() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let tempUsers2 = [];
            let tempUsers = yield this.managerUserDownloader.download();
            for (let u of tempUsers) {
                let tempHouses = yield this.householdDownloader.download(true, u.id);
                for (let h of tempHouses) {
                    let tempTurbines = yield this.turbineDownloader.download(h.id, true, u.id);
                    h.turbines = tempTurbines;
                    h.calcTotProd();
                }
                u.households = tempHouses;
                tempUsers2.push(u);
            }
            for (let u of tempUsers2) {
                for (let h of u.households) {
                    for (let t of h.turbines) {
                    }
                }
            }
            let suid = this.selectedUser.id;
            let shid = this.selectedHousehold.id;
            let stid = this.selectedTurbine.id;
            this.users = tempUsers;
            if (suid != -1) {
                this.reSelect(suid, shid, stid);
            }
        });
    }
    updateManagerField() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let tempManager = yield this.managerDownloader.download();
            this.coalPlant = tempManager.coalplant;
            this.market = tempManager.market;
        });
    }
    update() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.updateUsers();
            yield this.updateManagerField();
        });
    }
    onSelectUser(u) {
        this.selectedUser = u;
        this.selectedHousehold = new _models_household_model__WEBPACK_IMPORTED_MODULE_10__["Household"]("", -1);
    }
};
ManagerPageComponent.ctorParameters = () => [
    { type: _service_manager_service__WEBPACK_IMPORTED_MODULE_5__["ManagerService"] },
    { type: _service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _service_household_service__WEBPACK_IMPORTED_MODULE_4__["HouseholdService"] },
    { type: _service_turbine_service__WEBPACK_IMPORTED_MODULE_7__["TurbineService"] },
    { type: _service_manager_user_service__WEBPACK_IMPORTED_MODULE_13__["ManagerUserService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_user_field_user_field_component__WEBPACK_IMPORTED_MODULE_11__["UserFieldComponent"], { static: false })
], ManagerPageComponent.prototype, "userfieldComponent", void 0);
ManagerPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-manager-page',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./manager-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/manager-page.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./manager-page.component.css */ "./src/app/manager-page/manager-page.component.css")).default]
    })
], ManagerPageComponent);



/***/ }),

/***/ "./src/app/manager-page/manager-page.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/manager-page/manager-page.module.ts ***!
  \*****************************************************/
/*! exports provided: ManagerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerPageModule", function() { return ManagerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _manager_field_manager_field_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./manager-field/manager-field.component */ "./src/app/manager-page/manager-field/manager-field.component.ts");
/* harmony import */ var _list_users_list_users_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-users/list-users.component */ "./src/app/manager-page/list-users/list-users.component.ts");
/* harmony import */ var _user_field_user_field_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-field/user-field.component */ "./src/app/manager-page/user-field/user-field.component.ts");
/* harmony import */ var _list_users_user_user_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./list-users/user/user.component */ "./src/app/manager-page/list-users/user/user.component.ts");
/* harmony import */ var _manager_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./manager-page.component */ "./src/app/manager-page/manager-page.component.ts");
/* harmony import */ var _user_page_user_page_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../user-page/user-page.module */ "./src/app/user-page/user-page.module.ts");
/* harmony import */ var _edit_user_field_edit_user_field_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit-user-field/edit-user-field.component */ "./src/app/manager-page/edit-user-field/edit-user-field.component.ts");











let ManagerPageModule = class ManagerPageModule {
};
ManagerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _manager_field_manager_field_component__WEBPACK_IMPORTED_MODULE_4__["ManagerFieldComponent"],
            _list_users_list_users_component__WEBPACK_IMPORTED_MODULE_5__["ListUsersComponent"],
            _user_field_user_field_component__WEBPACK_IMPORTED_MODULE_6__["UserFieldComponent"],
            _list_users_user_user_component__WEBPACK_IMPORTED_MODULE_7__["UserComponent"],
            _manager_page_component__WEBPACK_IMPORTED_MODULE_8__["ManagerPageComponent"],
            _edit_user_field_edit_user_field_component__WEBPACK_IMPORTED_MODULE_10__["EditUserFieldComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _user_page_user_page_module__WEBPACK_IMPORTED_MODULE_9__["UserPageModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ]
    })
], ManagerPageModule);



/***/ }),

/***/ "./src/app/manager-page/user-field/user-field.component.css":
/*!******************************************************************!*\
  !*** ./src/app/manager-page/user-field/user-field.component.css ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbmFnZXItcGFnZS91c2VyLWZpZWxkL3VzZXItZmllbGQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/manager-page/user-field/user-field.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/manager-page/user-field/user-field.component.ts ***!
  \*****************************************************************/
/*! exports provided: UserFieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserFieldComponent", function() { return UserFieldComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_models_household_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/household.model */ "./src/app/models/household.model.ts");
/* harmony import */ var src_app_models_turbine_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/turbine.model */ "./src/app/models/turbine.model.ts");
/* harmony import */ var src_app_models_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/models/user.model */ "./src/app/models/user.model.ts");
/* harmony import */ var src_app_service_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/manager.service */ "./src/app/service/manager.service.ts");






let UserFieldComponent = class UserFieldComponent {
    constructor(managerService) {
        this.managerService = managerService;
        this.user = new src_app_models_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]("", "", "", -1);
        this.manager = true;
        this.selectedTurbine = new src_app_models_turbine_model__WEBPACK_IMPORTED_MODULE_3__["Turbine"]("", -1, 0, 0);
        this.selectedHousehold = new src_app_models_household_model__WEBPACK_IMPORTED_MODULE_2__["Household"]("", -1);
        this.selectedH = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectedT = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.turbines = [];
    }
    ngOnInit() {
    }
    onSelectHousehold(h) {
        this.selectedH.emit(h);
        this.onSelectTurbine(new src_app_models_turbine_model__WEBPACK_IMPORTED_MODULE_3__["Turbine"]("", -1, 0, 0)); // deselect
    }
    // Selects a turbine
    onSelectTurbine(t) {
        this.selectedT.emit(t);
    }
    deleteUser() {
        this.managerService.deleteUser(this.user.id);
    }
    setBlocked(v) {
        this.user.blocked = v;
        //user manager service to communicate to the server that the user is blocked then wait 10 seconds and then unblock
    }
    editUsername(name) {
        this.managerService.editUsername(name, this.user.id)
            .subscribe(data => {
            this.user.firstName = name;
        });
    }
};
UserFieldComponent.ctorParameters = () => [
    { type: src_app_service_manager_service__WEBPACK_IMPORTED_MODULE_5__["ManagerService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedUser')
], UserFieldComponent.prototype, "user", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedTurbine')
], UserFieldComponent.prototype, "selectedTurbine", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedHousehold')
], UserFieldComponent.prototype, "selectedHousehold", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], UserFieldComponent.prototype, "selectedH", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], UserFieldComponent.prototype, "selectedT", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('turbines')
], UserFieldComponent.prototype, "turbines", void 0);
UserFieldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user-field',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./user-field.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/manager-page/user-field/user-field.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./user-field.component.css */ "./src/app/manager-page/user-field/user-field.component.css")).default]
    })
], UserFieldComponent);



/***/ }),

/***/ "./src/app/models/coalplant.model.ts":
/*!*******************************************!*\
  !*** ./src/app/models/coalplant.model.ts ***!
  \*******************************************/
/*! exports provided: Status, CoalPlant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return Status; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoalPlant", function() { return CoalPlant; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var Status;
(function (Status) {
    Status[Status["OFF"] = 0] = "OFF";
    Status[Status["STARTING"] = 1] = "STARTING";
    Status[Status["ON"] = 2] = "ON";
})(Status || (Status = {}));
class CoalPlant {
    constructor(production, buffer, status) {
        this.production = production;
        this.buffer = buffer;
        this.status = status;
        this.statusText = this.setStatusText(status);
        this.buffer_ratio = 0;
    }
    setStatusText(status) {
        switch (status) {
            case 0: {
                return "OFF";
            }
            case 1: {
                return "STARTING";
            }
            case 2: {
                return "ON";
            }
            default: {
                return "invalid";
            }
        }
    }
}


/***/ }),

/***/ "./src/app/models/household.model.ts":
/*!*******************************************!*\
  !*** ./src/app/models/household.model.ts ***!
  \*******************************************/
/*! exports provided: Household */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Household", function() { return Household; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Household {
    constructor(name, id) {
        this.name = name;
        this.id = id;
        this.turbines = [];
        this.consumption = 0;
        this.buffer = 0;
        this.bufferThreshold = 0;
        this.sellThresh = 0;
        this.buyThresh = 0;
        this.image = null;
        this.percentageBuff = 0;
        this.buffer_limit = 0;
        this.blackout = false;
    }
    calcTotProd() {
        this.production = 0;
        this.turbines.forEach(t => {
            this.production = this.production + t.production;
        });
        this.production = Math.round(this.production);
    }
    updateImgView() {
        var reader = new FileReader();
        reader.readAsDataURL(this.image);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }
}


/***/ }),

/***/ "./src/app/models/manager.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/manager.model.ts ***!
  \*****************************************/
/*! exports provided: Manager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Manager", function() { return Manager; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Manager {
    constructor(coalplant, market) {
        this.coalplant = coalplant;
        this.market = market;
    }
}


/***/ }),

/***/ "./src/app/models/market.model.ts":
/*!****************************************!*\
  !*** ./src/app/models/market.model.ts ***!
  \****************************************/
/*! exports provided: Market */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Market", function() { return Market; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Market {
    constructor(demand, price, modeled_price) {
        this.demand = demand;
        this.price = price;
        this.modeled_price = modeled_price;
        this.buffer = 0;
    }
}


/***/ }),

/***/ "./src/app/models/turbine.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/turbine.model.ts ***!
  \*****************************************/
/*! exports provided: Turbine */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Turbine", function() { return Turbine; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Turbine {
    constructor(name, id, production, windspeed) {
        this.name = name;
        this.id = id;
        this.production = production;
        this.windspeed = windspeed;
    }
}


/***/ }),

/***/ "./src/app/models/user.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/user.model.ts ***!
  \**************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class User {
    constructor(userName, firstName, lastName, id) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.loggedIn = false;
        this.blocked = false;
        this.households = null;
    }
    updateImgView() {
        var reader = new FileReader();
        reader.readAsDataURL(this.image);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }
}


/***/ }),

/***/ "./src/app/navbar/navbar.component.css":
/*!*********************************************!*\
  !*** ./src/app/navbar/navbar.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdmJhci9uYXZiYXIuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_service_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/user.service */ "./src/app/service/user.service.ts");



let NavbarComponent = class NavbarComponent {
    constructor(userService) {
        this.userService = userService;
        this.isManager = true;
    }
    ngOnInit() {
        //Here we make a check after login if a user or manager logged in. Then we send them to the correct home page.
    }
    logout() {
        this.userService.logout().subscribe();
        ;
    }
};
NavbarComponent.ctorParameters = () => [
    { type: src_app_service_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] }
];
NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navbar',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/navbar/navbar.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./navbar.component.css */ "./src/app/navbar/navbar.component.css")).default]
    })
], NavbarComponent);



/***/ }),

/***/ "./src/app/service/household.service.ts":
/*!**********************************************!*\
  !*** ./src/app/service/household.service.ts ***!
  \**********************************************/
/*! exports provided: HouseholdService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseholdService", function() { return HouseholdService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let HouseholdService = class HouseholdService {
    constructor(http) {
        this.http = http;
    }
    managerGet(householdId, userId, path) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/household/${householdId}/${path}`);
    }
    userGet(householdId, path) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/household/${householdId}/${path}`);
    }
    setHouseImage(formData) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/image/upload/household`, formData);
    }
    // GETTERS
    getBlackout() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/households/blackout`);
    }
    getTurbines(householdId, manager) {
        if (manager) {
            return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/household/${householdId}/turbines`);
        }
        return this.userGet(householdId, "turbines");
    }
    getCons(householdId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, userId, "cons");
        }
        return this.userGet(householdId, "cons");
    }
    getBuffer(householdId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, userId, "buffer");
        }
        return this.userGet(householdId, "buffer");
    }
    getSellRatio(householdId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, userId, "sell_ratio");
        }
        return this.userGet(householdId, "sell_ratio");
    }
    getBuyRatio(householdId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, userId, "buy_ratio");
        }
        return this.userGet(householdId, "buy_ratio");
    }
    getImage(householdId, manager, userId) {
        if (manager) {
            return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/image/user/${userId}/household/${householdId}/image`, { responseType: 'blob' });
        }
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/image/household/${householdId}`, { responseType: 'blob' });
    }
    getName(householdId, manager, userId) {
        if (manager) {
            return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/household/${householdId}/name`); // exception to the structure due to interface archetecture
        }
        return this.userGet(householdId, "name");
    }
    getAddress(householdId, manager, userId) {
        if (manager) {
            return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/household/${householdId}/address`); // exception to the structure due to interface archetecture
        }
        return this.userGet(householdId, "address");
    }
    getBufferLimit(householdId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, userId, "buffer_limit");
        }
        return this.userGet(householdId, "buffer_limit");
    }
    // SETTERS
    addHousehold(coords, name, address) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/add/household/coords/${coords}/name/${name}/address/${address}`, null);
    }
    removeHousehold(householdId) {
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/delete/household/${householdId}`);
    }
    setImage(formData) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/image/upload/household/`, formData);
    }
    setSellRatio(householdId, ratio) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/control/household/${householdId}/buffer/sell/${ratio / 100}`, null);
    }
    setBuyRatio(householdId, ratio) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/control/household/${householdId}/buffer/buy/${ratio / 100}`, null);
    }
};
HouseholdService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
HouseholdService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], HouseholdService);



/***/ }),

/***/ "./src/app/service/manager.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/manager.service.ts ***!
  \********************************************/
/*! exports provided: ManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerService", function() { return ManagerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let ManagerService = class ManagerService {
    constructor(http) {
        this.http = http;
    }
    getCoalProduction() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/power_plant/prod`);
    }
    getCoalBuffer() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/power_plant/buffer`);
    }
    getCoalStatus() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/power_plant/status`);
    }
    getMarketPrice() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/elec_price`);
    }
    getMarketModelledPrice() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/modelled_elec_price`);
    }
    getMarketDemand() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/market/demand`);
    }
    getMarketBuffer() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/market/buffer`);
    }
    editUsername(username, userId) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/user/update/user_name/id/${userId}/name/${username}`, null);
    }
    deleteUser(userId) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/delete/${userId}`, null);
    }
    getUsers() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/users`);
    }
    getBlackoutUsers() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/households/blackout`);
    }
    getHouseholds(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/households`);
    }
    getLogged() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/users/logged`);
    }
    getUsername(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/user_name`);
    }
    getCoalRatio() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/power_plant/ratio`);
    }
    setCoalRatio(ratio) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/buffer/ratio/${ratio}`, null);
    }
    startCoalPlant() {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/power_plant/prod`, null);
    }
    stopCoalPlant() {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/power_plant/prod/stop`, null);
    }
    setMarketPrice(price) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/elec_price/${price}`, null);
    }
    blockUser(userId, time) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/user/${userId}/block/time/${time}`, null);
    }
    togglePrice() {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/toggle/price`, null);
    }
};
ManagerService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ManagerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ManagerService);



/***/ }),

/***/ "./src/app/service/manager.user.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/manager.user.service.ts ***!
  \*************************************************/
/*! exports provided: ManagerUserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerUserService", function() { return ManagerUserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let ManagerUserService = class ManagerUserService {
    constructor(http) {
        this.http = http;
    }
    setBlock(userId, time) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/control/user/${userId}/block/time/${time}`, null);
    }
    editUsername(username, userId) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/user/update/user_name/id/${userId}/name/${username}`, null);
    }
    editFirstName(firstName, userId) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/user/update/first_name/id/${userId}/name/${firstName}`, null);
    }
    editLastName(lastName, userId) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/user/update/last_name/id/${userId}/name/${lastName}`, null);
    }
    setImage(formData) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/image/upload/user`, formData);
    }
    getImage(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/image/user/${userId}/image`, { responseType: 'blob' });
    }
    deleteUser(userId) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/user/delete/${userId}`, null);
    }
    getUsers() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/users`);
    }
    getLogged() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/users/logged`);
    }
    getUsername(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/user_name`);
    }
    getFirstName(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/first_name`);
    }
    getLastName(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/last_name`);
    }
    getBlocked(userId) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/block`);
    }
};
ManagerUserService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ManagerUserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ManagerUserService);



/***/ }),

/***/ "./src/app/service/turbine.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/turbine.service.ts ***!
  \********************************************/
/*! exports provided: TurbineService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurbineService", function() { return TurbineService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let TurbineService = class TurbineService {
    constructor(http) {
        this.http = http;
    }
    managerGet(householdId, turbineId, userId, path) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/household/${householdId}/turbine/${turbineId}/${path}`);
    }
    userGet(householdId, turbineId, path) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/household/${householdId}/turbine/${turbineId}/${path}`);
    }
    // GETTERS
    getWindSpeed(householdId, turbineId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, turbineId, userId, "wind_speed");
        }
        return this.userGet(householdId, turbineId, "wind_speed");
    }
    getElecProd(householdId, turbineId, manager, userId) {
        if (manager) {
            return this.managerGet(householdId, turbineId, userId, "elec_prod");
        }
        return this.userGet(householdId, turbineId, "elec_prod");
    }
    // SETTERS
    addTurbine(householdId, coords, name) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + `/add/household/${householdId}/turbine/coords/${coords}/name/${name}`, null);
    }
    deleteTurbine(householdId, turbineId) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + `/household/${householdId}/turbine/${turbineId}`);
    }
};
TurbineService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
TurbineService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], TurbineService);



/***/ }),

/***/ "./src/app/service/user.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/user.service.ts ***!
  \*****************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let UserService = class UserService {
    constructor(http) {
        this.http = http;
    }
    setImage(formData) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/image/upload/user/`, formData);
    }
    getImage() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/image/user`, { responseType: 'blob' });
    }
    signup(user) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/signup`, user, { responseType: 'text' });
    }
    login(user) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/login`, user);
    }
    logout() {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/logout`, null);
    }
    getHouseholds(manager, userId) {
        if (manager) {
            return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/manager/get/user/${userId}/households`);
        }
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/households`);
    }
    getMarketPrice() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/elec_price`);
    }
    getUserName() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/user_name`);
    }
    getFirstName() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/first_name`);
    }
    getLastName() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/get/last_name`);
    }
};
UserService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UserService);



/***/ }),

/***/ "./src/app/signup-form/signup-form.component.css":
/*!*******************************************************!*\
  !*** ./src/app/signup-form/signup-form.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC1mb3JtL3NpZ251cC1mb3JtLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/signup-form/signup-form.component.ts":
/*!******************************************************!*\
  !*** ./src/app/signup-form/signup-form.component.ts ***!
  \******************************************************/
/*! exports provided: SignupFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupFormComponent", function() { return SignupFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");




let SignupFormComponent = class SignupFormComponent {
    constructor(formBuilder, userService) {
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.responseText = "";
        this.signupForm = this.formBuilder.group({
            fname: '',
            lname: '',
            uname: '',
            pass: '',
            pass2: ''
        });
    }
    onSubmit(signupData) {
        if (signupData.pass === signupData.pass2) {
            this.responseText = "You signed up!";
            this.userService.signup(signupData).subscribe();
        }
        else {
            this.responseText = "Passwords didn't match. Try again.";
        }
    }
    ngOnInit() {
    }
};
SignupFormComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] }
];
SignupFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-signup-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./signup-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup-form/signup-form.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./signup-form.component.css */ "./src/app/signup-form/signup-form.component.css")).default]
    })
], SignupFormComponent);



/***/ }),

/***/ "./src/app/user-page/household-form/household-form.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/user-page/household-form/household-form.component.css ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS9ob3VzZWhvbGQtZm9ybS9ob3VzZWhvbGQtZm9ybS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/user-page/household-form/household-form.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/user-page/household-form/household-form.component.ts ***!
  \**********************************************************************/
/*! exports provided: HouseholdFormComponent, HouseFormDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseholdFormComponent", function() { return HouseholdFormComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseFormDialog", function() { return HouseFormDialog; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _service_household_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../service/household.service */ "./src/app/service/household.service.ts");




let HouseholdFormComponent = class HouseholdFormComponent {
    constructor(dialog) {
        this.dialog = dialog;
    }
    openDialog() {
        const dialogRef = this.dialog.open(HouseFormDialog, {
            width: '300px',
            data: { gpsx: this.gpsx, gpsy: this.gpsy, name: this.name, address: this.address }
        });
        dialogRef.afterClosed().subscribe();
    }
};
HouseholdFormComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }
];
HouseholdFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-household-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./household-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household-form/household-form.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./household-form.component.css */ "./src/app/user-page/household-form/household-form.component.css")).default]
    })
], HouseholdFormComponent);

let HouseFormDialog = class HouseFormDialog {
    constructor(householdService, dialogRef, data) {
        this.householdService = householdService;
        this.dialogRef = dialogRef;
        this.data = data;
    }
    onSubmit() {
        this.householdService.addHousehold([this.data.gpsx, this.data.gpsy], this.data.name, this.data.address).subscribe();
        this.dialogRef.close();
    }
    onCancel() {
        this.dialogRef.close();
    }
};
HouseFormDialog.ctorParameters = () => [
    { type: _service_household_service__WEBPACK_IMPORTED_MODULE_3__["HouseholdService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
];
HouseFormDialog = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'house-form-dialog',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./house-form-dialog.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household-form/house-form-dialog.html")).default,
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
], HouseFormDialog);



/***/ }),

/***/ "./src/app/user-page/household/household.component.css":
/*!*************************************************************!*\
  !*** ./src/app/user-page/household/household.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS9ob3VzZWhvbGQvaG91c2Vob2xkLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/user-page/household/household.component.ts":
/*!************************************************************!*\
  !*** ./src/app/user-page/household/household.component.ts ***!
  \************************************************************/
/*! exports provided: HouseholdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseholdComponent", function() { return HouseholdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_service_household_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/household.service */ "./src/app/service/household.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");





let HouseholdComponent = class HouseholdComponent {
    constructor(householdService, formBuilder, httpClient) {
        this.householdService = householdService;
        this.formBuilder = formBuilder;
        this.httpClient = httpClient;
    }
    onFileChanged(event) {
        var file = event.target.files[0];
        this.household.image = file;
        this.uploadForm.get('house').setValue(file);
        this.uploadImg();
        this.household.updateImgView();
    }
    uploadImg() {
        const formData = new FormData();
        formData.append('household_id', this.household.id.toString());
        formData.append('user_id', this.userId.toString());
        formData.append('household', this.uploadForm.get('house').value);
        if (this.manager) {
            formData.append('user_id', this.userId.toString());
            this.householdService.setHouseImage(formData)
                .subscribe();
        }
        else {
            this.householdService.setImage(formData)
                .subscribe();
        }
    }
    ngOnInit() {
        try {
            this.household.updateImgView();
        }
        catch (error) {
        }
        this.uploadForm = this.formBuilder.group({
            house: ['']
        });
    }
};
HouseholdComponent.ctorParameters = () => [
    { type: src_app_service_household_service__WEBPACK_IMPORTED_MODULE_2__["HouseholdService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('household')
], HouseholdComponent.prototype, "household", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('manager')
], HouseholdComponent.prototype, "manager", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('userId')
], HouseholdComponent.prototype, "userId", void 0);
HouseholdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-household',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./household.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/household/household.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./household.component.css */ "./src/app/user-page/household/household.component.css")).default]
    })
], HouseholdComponent);



/***/ }),

/***/ "./src/app/user-page/list-household/list-household.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/user-page/list-household/list-household.component.css ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS9saXN0LWhvdXNlaG9sZC9saXN0LWhvdXNlaG9sZC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/user-page/list-household/list-household.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/user-page/list-household/list-household.component.ts ***!
  \**********************************************************************/
/*! exports provided: ListHouseholdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHouseholdComponent", function() { return ListHouseholdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ListHouseholdComponent = class ListHouseholdComponent {
    constructor() {
        this.householdIDs = [];
        this.households = [];
        this.manager = false;
        this.selected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.updateToggle = false;
    }
    selectHousehold(h) {
        this.selected.emit(h);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('households')
], ListHouseholdComponent.prototype, "households", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedHousehold')
], ListHouseholdComponent.prototype, "selectedHousehold", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('manager')
], ListHouseholdComponent.prototype, "manager", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('userId')
], ListHouseholdComponent.prototype, "userId", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], ListHouseholdComponent.prototype, "selected", void 0);
ListHouseholdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-household',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./list-household.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/list-household/list-household.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./list-household.component.css */ "./src/app/user-page/list-household/list-household.component.css")).default]
    })
], ListHouseholdComponent);



/***/ }),

/***/ "./src/app/user-page/list-turbines/list-turbines.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/user-page/list-turbines/list-turbines.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS9saXN0LXR1cmJpbmVzL2xpc3QtdHVyYmluZXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/user-page/list-turbines/list-turbines.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/user-page/list-turbines/list-turbines.component.ts ***!
  \********************************************************************/
/*! exports provided: ListTurbinesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListTurbinesComponent", function() { return ListTurbinesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ListTurbinesComponent = class ListTurbinesComponent {
    constructor() {
        this.turbines = [];
        this.selected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    selectTurbine(t) {
        this.selected.emit(t);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedTurbine')
], ListTurbinesComponent.prototype, "selectedTurbine", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('turbines')
], ListTurbinesComponent.prototype, "turbines", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], ListTurbinesComponent.prototype, "selected", void 0);
ListTurbinesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-turbines',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./list-turbines.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/list-turbines/list-turbines.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./list-turbines.component.css */ "./src/app/user-page/list-turbines/list-turbines.component.css")).default]
    })
], ListTurbinesComponent);



/***/ }),

/***/ "./src/app/user-page/profile-field/profile-field.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/user-page/profile-field/profile-field.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS9wcm9maWxlLWZpZWxkL3Byb2ZpbGUtZmllbGQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/user-page/profile-field/profile-field.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/user-page/profile-field/profile-field.component.ts ***!
  \********************************************************************/
/*! exports provided: ProfileFieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileFieldComponent", function() { return ProfileFieldComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_models_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/models/user.model */ "./src/app/models/user.model.ts");
/* harmony import */ var src_app_service_manager_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/manager.user.service */ "./src/app/service/manager.user.service.ts");
/* harmony import */ var src_app_service_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/service/user.service */ "./src/app/service/user.service.ts");







let ProfileFieldComponent = class ProfileFieldComponent {
    constructor(userService, managerUserService, formBuilder, httpClient) {
        this.userService = userService;
        this.managerUserService = managerUserService;
        this.formBuilder = formBuilder;
        this.httpClient = httpClient;
        this.user = new src_app_models_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]("", "", "", -1);
        this.manager = false;
    }
    onFileChanged(event) {
        var file = event.target.files[0];
        this.user.image = file;
        this.uploadForm.get('user').setValue(file);
        this.uploadImg();
        this.user.updateImgView();
    }
    uploadImg() {
        const formData = new FormData();
        formData.append('user_id', this.user.id.toString());
        formData.append('user', this.uploadForm.get('user').value);
        if (this.manager) {
            this.managerUserService.setImage(formData)
                .subscribe();
        }
        else {
            this.userService.setImage(formData)
                .subscribe();
        }
    }
    ngOnInit() {
        try {
            this.user.updateImgView();
        }
        catch (error) {
        }
        this.uploadForm = this.formBuilder.group({
            user: ['']
        });
    }
};
ProfileFieldComponent.ctorParameters = () => [
    { type: src_app_service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: src_app_service_manager_user_service__WEBPACK_IMPORTED_MODULE_5__["ManagerUserService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])('user')
], ProfileFieldComponent.prototype, "user", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])('manager')
], ProfileFieldComponent.prototype, "manager", void 0);
ProfileFieldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-profile-field',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profile-field.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/profile-field/profile-field.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profile-field.component.css */ "./src/app/user-page/profile-field/profile-field.component.css")).default]
    })
], ProfileFieldComponent);



/***/ }),

/***/ "./src/app/user-page/stats-field/stats-field.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/user-page/stats-field/stats-field.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS9zdGF0cy1maWVsZC9zdGF0cy1maWVsZC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/user-page/stats-field/stats-field.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/user-page/stats-field/stats-field.component.ts ***!
  \****************************************************************/
/*! exports provided: StatsFieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsFieldComponent", function() { return StatsFieldComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_service_household_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/household.service */ "./src/app/service/household.service.ts");



let StatsFieldComponent = class StatsFieldComponent {
    constructor(householdService) {
        this.householdService = householdService;
    }
    setSellThresh(value) {
        this.selectedHousehold.sellThresh = value;
        this.confirmSell();
    }
    setBuyThresh(value) {
        this.selectedHousehold.buyThresh = value;
        this.confirmBuy();
    }
    confirmSell() {
        this.householdService.setSellRatio(this.selectedHousehold.id, this.selectedHousehold.sellThresh)
            .subscribe();
    }
    confirmBuy() {
        this.householdService.setBuyRatio(this.selectedHousehold.id, this.selectedHousehold.buyThresh)
            .subscribe();
    }
};
StatsFieldComponent.ctorParameters = () => [
    { type: src_app_service_household_service__WEBPACK_IMPORTED_MODULE_2__["HouseholdService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedTurbine')
], StatsFieldComponent.prototype, "selectedTurbine", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedHousehold')
], StatsFieldComponent.prototype, "selectedHousehold", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('marketPrice')
], StatsFieldComponent.prototype, "marketPrice", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('manager')
], StatsFieldComponent.prototype, "manager", void 0);
StatsFieldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-stats-field',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./stats-field.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/stats-field/stats-field.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./stats-field.component.css */ "./src/app/user-page/stats-field/stats-field.component.css")).default]
    })
], StatsFieldComponent);



/***/ }),

/***/ "./src/app/user-page/turbine-form/turbine-form.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/user-page/turbine-form/turbine-form.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS90dXJiaW5lLWZvcm0vdHVyYmluZS1mb3JtLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/user-page/turbine-form/turbine-form.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/user-page/turbine-form/turbine-form.component.ts ***!
  \******************************************************************/
/*! exports provided: TurbineFormComponent, TurbineFormDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurbineFormComponent", function() { return TurbineFormComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurbineFormDialog", function() { return TurbineFormDialog; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _service_turbine_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../service/turbine.service */ "./src/app/service/turbine.service.ts");




let TurbineFormComponent = class TurbineFormComponent {
    constructor(dialog) {
        this.dialog = dialog;
    }
    openDialog() {
        const dialogRef = this.dialog.open(TurbineFormDialog, {
            width: '300px',
            data: { id: this.household.id, gpsx: this.gpsx, gpsy: this.gpsy, name: this.name }
        });
        dialogRef.afterClosed().subscribe();
    }
};
TurbineFormComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('selectedHousehold')
], TurbineFormComponent.prototype, "household", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('firstSelectFlag')
], TurbineFormComponent.prototype, "flag", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('testString')
], TurbineFormComponent.prototype, "testString", void 0);
TurbineFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-turbine-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./turbine-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine-form/turbine-form.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./turbine-form.component.css */ "./src/app/user-page/turbine-form/turbine-form.component.css")).default]
    })
], TurbineFormComponent);

let TurbineFormDialog = class TurbineFormDialog {
    constructor(turbineService, dialogRef, data) {
        this.turbineService = turbineService;
        this.dialogRef = dialogRef;
        this.data = data;
    }
    onSubmit() {
        var coords = [0, 0];
        // var coords = [this.data.gpsx, this.data.gpsy]
        this.turbineService.addTurbine(this.data.id, coords, this.data.name).subscribe();
        this.dialogRef.close();
    }
    onCancel() {
        this.dialogRef.close();
    }
};
TurbineFormDialog.ctorParameters = () => [
    { type: _service_turbine_service__WEBPACK_IMPORTED_MODULE_3__["TurbineService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
];
TurbineFormDialog = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-turbine-form-dialog',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./turbine-form-dialog.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine-form/turbine-form-dialog.html")).default,
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
], TurbineFormDialog);



/***/ }),

/***/ "./src/app/user-page/turbine/turbine.component.css":
/*!*********************************************************!*\
  !*** ./src/app/user-page/turbine/turbine.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS90dXJiaW5lL3R1cmJpbmUuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/user-page/turbine/turbine.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-page/turbine/turbine.component.ts ***!
  \********************************************************/
/*! exports provided: TurbineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TurbineComponent", function() { return TurbineComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TurbineComponent = class TurbineComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('turbine')
], TurbineComponent.prototype, "turbine", void 0);
TurbineComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-turbine',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./turbine.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/turbine/turbine.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./turbine.component.css */ "./src/app/user-page/turbine/turbine.component.css")).default]
    })
], TurbineComponent);



/***/ }),

/***/ "./src/app/user-page/user-page.component.css":
/*!***************************************************!*\
  !*** ./src/app/user-page/user-page.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItcGFnZS91c2VyLXBhZ2UuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/user-page/user-page.component.ts":
/*!**************************************************!*\
  !*** ./src/app/user-page/user-page.component.ts ***!
  \**************************************************/
/*! exports provided: UserPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageComponent", function() { return UserPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _models_turbine_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/turbine.model */ "./src/app/models/turbine.model.ts");
/* harmony import */ var _models_household_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/household.model */ "./src/app/models/household.model.ts");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
/* harmony import */ var _service_household_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/household.service */ "./src/app/service/household.service.ts");
/* harmony import */ var _service_turbine_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/turbine.service */ "./src/app/service/turbine.service.ts");
/* harmony import */ var _downloaders_household_downloader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../downloaders/household.downloader */ "./src/app/downloaders/household.downloader.ts");
/* harmony import */ var _downloaders_turbine_downloader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../downloaders/turbine.downloader */ "./src/app/downloaders/turbine.downloader.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../models/user.model */ "./src/app/models/user.model.ts");
/* harmony import */ var _downloaders_user_downloader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../downloaders/user.downloader */ "./src/app/downloaders/user.downloader.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");













let UserPageComponent = class UserPageComponent {
    constructor(userService, householdService, turbineService) {
        this.userService = userService;
        this.user = new _models_user_model__WEBPACK_IMPORTED_MODULE_10__["User"]("", "", "", 0);
        this.selectedTurbine = new _models_turbine_model__WEBPACK_IMPORTED_MODULE_2__["Turbine"]("", -1, 0, 0);
        this.selectedHousehold = new _models_household_model__WEBPACK_IMPORTED_MODULE_3__["Household"]("", -1);
        this.marketPrice = 0;
        this.households = [];
        this.turbines = [];
        this.updateFreq = src_environments_environment__WEBPACK_IMPORTED_MODULE_12__["environment"].update_frequency; // in seconds
        this.updateTimer = 0;
        this.householdDownloader = new _downloaders_household_downloader__WEBPACK_IMPORTED_MODULE_7__["HouseholdDownloader"](userService, householdService);
        this.turbineDownloader = new _downloaders_turbine_downloader__WEBPACK_IMPORTED_MODULE_8__["TurbineDownloader"](householdService, turbineService);
        this.userDownloader = new _downloaders_user_downloader__WEBPACK_IMPORTED_MODULE_11__["UserDownloader"](userService);
    }
    onSelectHousehold(h) {
        this.selectedHousehold = h;
        this.selectedTurbine = new _models_turbine_model__WEBPACK_IMPORTED_MODULE_2__["Turbine"]("", -1, 0, 0);
        ; // deselect
        this.turbines = this.selectedHousehold.turbines;
    }
    // Selects a turbine
    onSelectTurbine(t) {
        this.selectedTurbine = t;
    }
    getPrice(userService) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                userService.getMarketPrice().subscribe(data => {
                    resolve(data.elec_price);
                });
            });
        });
    }
    reSelect(hid, tid) {
        try {
            let house = this.households.filter(x => x.id == hid)[0];
            let turbine = house.turbines.filter(x => x.id == tid)[0];
            this.onSelectHousehold(house);
            if (turbine) {
                this.onSelectTurbine(turbine);
            }
        }
        catch (error) {
        }
    }
    update() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let tempUser = yield this.userDownloader.download();
            let tempHouseholds = yield this.householdDownloader.download(false, 0);
            for (let house of tempHouseholds) {
                house.turbines = yield this.turbineDownloader.download(house.id, false, 0);
                house.calcTotProd();
            }
            this.marketPrice = Math.round((yield this.getPrice(this.userService)) * 100) / 100;
            let shid = this.selectedHousehold.id;
            let stid = this.selectedTurbine.id;
            this.user = tempUser;
            this.households = tempHouseholds;
            this.reSelect(shid, stid);
        });
    }
    ngOnInit() {
    }
    ngAfterContentInit() {
        this.update();
        let second = Object(rxjs__WEBPACK_IMPORTED_MODULE_9__["interval"])(1000);
        second.subscribe(() => {
            this.updateTimer = this.updateTimer + 1;
            if (this.updateTimer == this.updateFreq) {
                this.update();
                this.updateTimer = 0;
            }
        });
    }
};
UserPageComponent.ctorParameters = () => [
    { type: _service_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _service_household_service__WEBPACK_IMPORTED_MODULE_5__["HouseholdService"] },
    { type: _service_turbine_service__WEBPACK_IMPORTED_MODULE_6__["TurbineService"] }
];
UserPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user-page',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./user-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-page/user-page.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./user-page.component.css */ "./src/app/user-page/user-page.component.css")).default]
    })
], UserPageComponent);



/***/ }),

/***/ "./src/app/user-page/user-page.module.ts":
/*!***********************************************!*\
  !*** ./src/app/user-page/user-page.module.ts ***!
  \***********************************************/
/*! exports provided: UserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageModule", function() { return UserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _household_form_household_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./household-form/household-form.component */ "./src/app/user-page/household-form/household-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _turbine_form_turbine_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./turbine-form/turbine-form.component */ "./src/app/user-page/turbine-form/turbine-form.component.ts");
/* harmony import */ var _user_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./user-page.component */ "./src/app/user-page/user-page.component.ts");
/* harmony import */ var _household_household_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./household/household.component */ "./src/app/user-page/household/household.component.ts");
/* harmony import */ var _list_household_list_household_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./list-household/list-household.component */ "./src/app/user-page/list-household/list-household.component.ts");
/* harmony import */ var _list_turbines_list_turbines_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./list-turbines/list-turbines.component */ "./src/app/user-page/list-turbines/list-turbines.component.ts");
/* harmony import */ var _turbine_turbine_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./turbine/turbine.component */ "./src/app/user-page/turbine/turbine.component.ts");
/* harmony import */ var _stats_field_stats_field_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./stats-field/stats-field.component */ "./src/app/user-page/stats-field/stats-field.component.ts");
/* harmony import */ var _profile_field_profile_field_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./profile-field/profile-field.component */ "./src/app/user-page/profile-field/profile-field.component.ts");




// Used for popup

// npm install @angular/material @angular/cdk @angular/animations --save
// ng add @angular/material












let UserPageModule = class UserPageModule {
};
UserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _user_page_component__WEBPACK_IMPORTED_MODULE_10__["UserPageComponent"],
            _household_form_household_form_component__WEBPACK_IMPORTED_MODULE_3__["HouseholdFormComponent"],
            _household_form_household_form_component__WEBPACK_IMPORTED_MODULE_3__["HouseFormDialog"],
            _turbine_form_turbine_form_component__WEBPACK_IMPORTED_MODULE_9__["TurbineFormComponent"],
            _turbine_form_turbine_form_component__WEBPACK_IMPORTED_MODULE_9__["TurbineFormDialog"],
            _household_household_component__WEBPACK_IMPORTED_MODULE_11__["HouseholdComponent"],
            _list_household_list_household_component__WEBPACK_IMPORTED_MODULE_12__["ListHouseholdComponent"],
            _list_turbines_list_turbines_component__WEBPACK_IMPORTED_MODULE_13__["ListTurbinesComponent"],
            _turbine_turbine_component__WEBPACK_IMPORTED_MODULE_14__["TurbineComponent"],
            _stats_field_stats_field_component__WEBPACK_IMPORTED_MODULE_15__["StatsFieldComponent"],
            _profile_field_profile_field_component__WEBPACK_IMPORTED_MODULE_16__["ProfileFieldComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__["BrowserAnimationsModule"]
        ],
        providers: [
            {
                provide: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogRef"],
            }
        ],
        entryComponents: [
            _household_form_household_form_component__WEBPACK_IMPORTED_MODULE_3__["HouseFormDialog"],
            _turbine_form_turbine_form_component__WEBPACK_IMPORTED_MODULE_9__["TurbineFormDialog"]
        ],
        exports: [
            _household_household_component__WEBPACK_IMPORTED_MODULE_11__["HouseholdComponent"],
            _list_household_list_household_component__WEBPACK_IMPORTED_MODULE_12__["ListHouseholdComponent"],
            _list_turbines_list_turbines_component__WEBPACK_IMPORTED_MODULE_13__["ListTurbinesComponent"],
            _turbine_turbine_component__WEBPACK_IMPORTED_MODULE_14__["TurbineComponent"],
            _stats_field_stats_field_component__WEBPACK_IMPORTED_MODULE_15__["StatsFieldComponent"],
            _profile_field_profile_field_component__WEBPACK_IMPORTED_MODULE_16__["ProfileFieldComponent"]
        ]
    })
], UserPageModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    apiUrl: 'https://localhost:8443',
    update_frequency: 10,
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/kims/gle-system/Frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map