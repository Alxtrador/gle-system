CREATE extension IF NOT EXISTS pgcrypto;

DROP TABLE IF EXISTS usr CASCADE;
CREATE TABLE usr (
    id SERIAL,
    f_name VARCHAR(255) NOT NULL,
    l_name VARCHAR(255) NOT NULL,
    usr_name VARCHAR(255) UNIQUE NOT NULL,
    role INT NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS household CASCADE;
CREATE TABLE household (
    id SERIAL,
    usr_id INT NOT NULL,
    gps_coords POINT NOT NULL,
    h_name VARCHAR(255) NOT NULL,
    addr VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (usr_id) REFERENCES usr (id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS turbine CASCADE;
CREATE TABLE turbine (
    id SERIAL,
    household_id INT NOT NULL,
    gps_coords POINT NOT NULL,
    t_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (household_id) REFERENCES household (id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS time_stamp CASCADE;
CREATE TABLE time_stamp (
    id SERIAL,
    t TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS turbine_vals CASCADE;
CREATE TABLE turbine_vals (
    id SERIAL,
    turbine_id INT NOT NULL,
    time_id INT NOT NULL,
    wind_speed FLOAT(24) NOT NULL,
    FOREIGN KEY (turbine_id) REFERENCES turbine (id) ON DELETE CASCADE,
    FOREIGN KEY (time_id) REFERENCES time_stamp (id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS household_vals CASCADE;
CREATE TABLE household_vals (
    id SERIAL,
    household_id INT NOT NULL,
    time_id INT NOT NULL,
    consumption FLOAT(24) NOT NULL,
    FOREIGN KEY (household_id) REFERENCES household (id) ON DELETE CASCADE,
    FOREIGN KEY (time_id) REFERENCES time_stamp (id) ON DELETE CASCADE
);

INSERT INTO usr VALUES (DEFAULT, 'TempManager', 'TempManager', 'TempManager', 1, crypt('password', gen_salt('bf')));