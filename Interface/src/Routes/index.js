const user_routes = require('./user.js');
const add_routes = require('./add.js');
const delete_routes = require('./delete.js');
const get_routes = require('./get.js');
const image_routes = require('./image.js');
const control_routes = require('./control.js');
const init_route = require('./init.js');

module.exports = {
    user_routes: user_routes,
    add_routes: add_routes,
    delete_routes: delete_routes,
    get_routes: get_routes,
    image_routes: image_routes,
    control_routes: control_routes,
    init_route: init_route
};