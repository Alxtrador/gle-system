var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());

const fs = require('fs');

module.exports = function factory(db) {
    router.post('/update/user_name/id/:user_id/name/:name', (req, res) => {
        if (req.session.isManager) {
            db.updateUsername(req.params.user_id, req.params.name)
                .then(val => res.end(JSON.stringify({ response: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/update/first_name/id/:user_id/name/:name', (req, res) => {
        if (req.session.isManager) {
            db.updateLastname(req.params.user_id, req.params.name)
                .then(val => res.end(JSON.stringify({ response: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/update/last_name/id/:user_id/name/:name', (req, res) => {
        if (req.session.isManager) {
            db.updateFirstname(req.params.user_id, req.params.name)
                .then(val => res.end(JSON.stringify({ response: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/delete/:userId', (req, res) => {
        if (req.session.isManager) {
            db.selectNonManager(req.params.userId)
                .then(val => db.deleteUser(val))
                .then(val => Com.delete.user(val))
                .then(val => res.end(JSON.stringify({ response: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/logged_in', (req, res) => {
        if (req.session.isManager) {
            req.sessionStore.all()
                .then(val => res.end(val.length))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    return router;
}
