const manager_user_routes = require('./user.js');
const manager_get_routes = require('./get.js');
const manager_control_routes = require('./control.js');
const manager_image_routes = require('./image.js');

module.exports = {
    manager_user_routes: manager_user_routes,
    manager_get_routes: manager_get_routes,
    manager_control_routes: manager_control_routes,
    manager_image_routes: manager_image_routes
};