var express = require('express');
var router = express.Router();
var Com = require('../../Com');

module.exports = function factory(db) {
    router.get('/power_plant/prod', (req, res) => {
        if (req.session.isManager) {
            Com.get.power_plant_prod()
                .then(val => res.end(JSON.stringify({ prod: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/power_plant/status', (req, res) => {
        if (req.session.isManager) {
            Com.get.status()
                .then(val => res.end(JSON.stringify({ status: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/market/demand', (req, res) => {
        if (req.session.isManager) {
            Com.get.demand()
                .then(val => res.end(JSON.stringify({ demand: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/market/buffer', (req, res) => {
        if (req.session.isManager) {
            Com.get.market_buffer()
                .then(val => res.end(JSON.stringify({ buffer: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/households/blackout', (req, res) => {
        if (req.session.isManager) {
            Com.get.blackout()
                .then(val => res.end(JSON.stringify({ blackout: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/elec_price', (req, res) => {
        if (req.session.isManager) {
            Com.get.elec_price()
                .then(val => res.end(JSON.stringify({ elec_price: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/modelled_elec_price', (req, res) => {
        if (req.session.isManager) {
            Com.get.modelled_elec_price()
                .then(val => res.end(JSON.stringify({ modelled_elec_price: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/power_plant/buffer', (req, res) => {
        if (req.session.isManager) {
            Com.get.power_plant_buffer()
                .then(val => res.end(JSON.stringify({ buffer: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/power_plant/ratio', (req, res) => {
        if (req.session.isManager) {
            Com.get.ratio()
                .then(val => res.end(JSON.stringify({ ratio: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/users', (req, res) => {
        if (req.session.isManager) {
            db.selectUserIds()
                .then(val => res.end(JSON.stringify({ users: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/households', (req, res) => {
        if (req.session.isManager) {
            db.selectHouseholdIds(req.params.user_id)
                .then(val => res.end(JSON.stringify({ households: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/household/:household_id/turbines', (req, res) => {
        if (req.session.isManager) {
            db.selectTurbineIds(req.params.household_id)
                .then(val => res.end(JSON.stringify({ turbines: val } )))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/users/logged', (req, res) => {
        if (req.session.isManager) {
            var sessions = req.sessionStore.sessions;
            var toSend = [];
            for (user in sessions) {
                var userJson = JSON.parse(sessions[user]);
                var data = userJson.userId;
                toSend.push(data);
            }
            res.send(JSON.stringify({ user_arr: toSend }));
            res.end();
        } else {
            res.status(400).end();
        }
    });


    router.get('/user/:user_id/household/:household_id/cons', (req, res) => {
        if (req.session.isManager) {
            Com.get.cons(req.params.user_id, req.params.household_id)
                .then(val => res.end(JSON.stringify({ cons: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/turbine/:turbine_id/wind_speed', (req, res) => {
        if (req.session.isManager) {
            Com.get.wind_speed(req.params.user_id, req.params.household_id, req.params.turbine_id)
                .then(val =>res.end(JSON.stringify({ wind_speed: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/turbine/:turbine_id/elec_prod', (req, res) => {
        if (req.session.isManager) {
            Com.get.elec_prod(req.params.user_id, req.params.household_id, req.params.turbine_id)
                .then(val => res.end(JSON.stringify({ elec_prod: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/buffer', (req, res) => {
        if (req.session.isManager) {
            Com.get.buffer(req.params.user_id, req.params.household_id)
                .then(val => res.end(JSON.stringify({ buffer: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/user_name', (req, res) => {
        if (req.session.isManager) {
            db.selectUserName(req.params.user_id)
                .then(val => res.end(JSON.stringify({ user_name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/first_name', (req, res) => {
        if (req.session.isManager) {
            db.selectFirstName(req.params.user_id)
                .then(val => res.end(JSON.stringify({ first_name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/last_name', (req, res) => {
        if (req.session.isManager) {
            db.selectLastName(req.params.user_id)
                .then(val => res.end(JSON.stringify({ last_name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/name', (req, res) => {
        if (req.session.isManager) {
            db.selectHouseholdName(req.params.household_id)
                .then(val => res.end(JSON.stringify({ name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/address', (req, res) => {
        if (req.session.isManager) {
            db.selectHouseholdAddress(req.params.household_id)
                .then(val => res.end(JSON.stringify({ address: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/block', (req, res) => {
        if (req.session.isManager) {
            Com.get.userBlocked(req.params.user_id)
                .then(val => res.end(JSON.stringify({ block: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/buy_ratio', (req, res) => {
        if (req.session.isManager) {
            Com.get.buy_ratio(req.params.user_id, req.params.household_id)
                .then(val => res.end(JSON.stringify({ buy_ratio: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/sell_ratio', (req, res) => {
        if (req.session.isManager) {
            Com.get.sell_ratio(req.params.user_id, req.params.household_id)
                .then(val => res.end(JSON.stringify({ sell_ratio: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/buffer_limit', (req, res) => {
        if (req.session.isManager) {
            Com.get.buffer_limit(req.params.user_id, req.params.household_id)
                .then(val => res.end(JSON.stringify({ buffer_limit: val })))
                .catch(err => console.err(err));
        } else {
            res.status(400).end();
        }
    });

    return router;
}