var express = require('express');
var router = express.Router();
var Com = require('../../Com');

module.exports = function factory(db) {
    router.post('/power_plant/prod', (req, res) => {
        if (req.session.isManager) {
            Com.control.prod()
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/power_plant/prod/stop', (req, res) => {
        if (req.session.isManager) {
            Com.control.stop_prod()
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/buffer/ratio/:new_ratio', (req, res) => {
        if (req.session.isManager) {
            Com.control.ratio(req.params.new_ratio)
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/elec_price/:new_price', (req, res) => {
        if (req.session.isManager) {
            Com.control.elec_price(req.params.new_price)
                .then(res.end())
                .catch(err => console.error(err));

        } else {
            res.status(400).end();
        }
    });

    router.post('/toggle/price', (req, res) => {
        if (req.session.isManager) {
            Com.control.togglePrice()
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/user/:user_id/block/time/:time', (req, res) => {
        if (req.session.isManager) {
            Com.control.market_block(req.params.user_id, req.params.time)
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    return router;
}