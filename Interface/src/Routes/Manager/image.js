var express = require('express');
var router = express.Router();
var multer = require('multer');

const fs = require('fs');

function fileFilter (req, file, cb) {
    if (req.session.userId) {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

var storage_usr_img = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './images/user/');
    },
    filename: function (req, file, cb) {
        cb(null,  req.body.user_id + '.png');
    }
});

var usr_image = multer({ storage: storage_usr_img,
                         fileFilter: fileFilter });

var storage_household_img = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './images/household/');
    },
    filename: function (req, file, cb) {
        cb(null, req.body.user_id + '-' + req.body.household_id + '.png');
    }
});

var household_image = multer({ storage: storage_household_img,
                               fileFilter: fileFilter });

var bodyParser = require('body-parser');
router.use(bodyParser.json());

module.exports = function factory(db) {
    router.post('/upload/household/', household_image.single('household'), (req, res) => {
        if (req.session.isManager) {
            res.send("Image uploaded");
        } else {
            res.status(400).end();
        }
    });

    router.post('/upload/user', usr_image.single('user'), (req, res) => {
        if (req.session.isManager) {
            res.send("Image uploaded");
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/household/:household_id/image', (req, res) => {
        if (req.session.isManager) {
            fs.stat('./images/household/' + req.params.user_id + '-' + req.params.household_id + '.png', function(err, stat) {
                if (err == null) {
                    res.sendFile('images/household/' + req.params.user_id + '-' + req.params.household_id + '.png', { root: '.' });
                } else {
                    res.sendFile('images/default_household.jpg', { root: '.' });
                }
            });
        } else {
            res.status(400).end();
        }
    });

    router.get('/user/:user_id/image', (req, res) => {
        if (req.session.isManager) {
            fs.stat('./images/user/' + req.params.user_id + '.png', function(err, stat) {
                if (err == null) {
                    res.sendFile('images/user/' + req.params.user_id + '.png', { root: '.' });
                } else {
                    res.sendFile('images/default_user.jpg', { root: '.' });
                }
            });
        } else {
            res.status(400).end();
        }
    });

    return router;
}