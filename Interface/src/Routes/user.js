var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
var Com = require('../Com');

module.exports = function factory(db) {
    router.post('/signup', function(req, res) {
        if ((req.body.uname == null) || (req.body.pass == null) || (req.body.fname == null) || (req.body.lname == null)) {
            res.end("Missing details");
        } else {
            db.insertUser(req.body.fname, req.body.lname, req.body.uname, 0, req.body.pass)
                .then(val => Com.add.user(val))
                .catch(err => console.error(err));
        }
    });
    
    router.post('/login', function(req, res) {
        if ((req.body.uname == null) || (req.body.pass == null)) {
            res.end("Missing details");
        } else {
            db.selectUser(req.body.uname, req.body.pass)
                .then(val => {
                    req.session.uname = req.body.uname;
                    req.session.userId = val[0];
                    req.session.isManager = val[1];
                    req.session.householdIds = [];
                    req.session.turbineIds = [];
                    return db.selectHouseholdIds(val[0]);
                })
                .then(val => {
                    req.session.householdIds = val;
                    return Promise.all(val.map((element) => db.selectTurbineIds(element)));
                })
                .then(val => {
                    //req.session.turbineIds = val.reduce((acc, cur) => acc.concat(cur, []));
                    //req.session.turbineIds = val.flat(); //might need to keep the pre-existing structure so turbines can be tracked with households
                    req.session.turbineIds = val;
                    res.end();
                })
                .catch(err => {
                    console.error(err);
                    res.end();
                });
        }
    });
    
    router.post('/logout', function(req, res) {
        res.clearCookie('user');
        req.session.destroy();
        res.redirect('/');
    });

    return router;
}