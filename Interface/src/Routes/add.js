var express = require('express');
var router = express.Router();
var Com = require('../Com');

module.exports = function factory(db) {
    router.post('/household/coords/:coords/name/:name/address/:address', (req, res) => {
        //get actual household_id from db
        if (req.session.userId) {
            db.insertHousehold(req.session.userId, req.params.coords.split(','), req.params.name, req.params.address)
                .then(val => Com.add.household(req.session.userId, val, req.params.coords))
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.post('/household/:household_id/turbine/coords/:coords/name/:name', (req, res) => {
        if (req.session.userId) {
            db.insertTurbine(req.params.household_id, req.params.coords.split(','), req.params.name)
                .then(val => Com.add.turbine(req.session.userId, req.params.household_id, val))
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    return router;
}