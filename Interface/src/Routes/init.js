var express = require('express');
var ipfilter = require('express-ipfilter').IpFilter;
var router = express.Router();

module.exports = function factory(db) {
    router.get('/init_all', ipfilter(['127.0.0.1'], {mode: 'allow'}), function(req, res) {
        var json = {};

        json.Data = [];
        db.selectUserIds()
            .then(userIds => {
                var promises = [];
                userIds.forEach(userId => {
                    json.Data.push({userId: userId, householdIds: [], turbineIds: []});
                    var combineUserIDHouseholdIDPromise = new Promise((resolve, reject) => {
                        resolve(db.selectHouseholdIds(userId))
                    }).then(householdIds => [userId, householdIds]);

                    promises.push(combineUserIDHouseholdIDPromise)
                })
                return Promise.all(promises);
            })
            .then(userIdsAndHouseholds => {
                var promises = [];
                userIdsAndHouseholds.forEach(res => {
                    var userId = res[0];
                    var householdIds = res[1];
                    householdIds.forEach(householdId => {
                        var index = json.Data.indexOf(json.Data.find(element => element.userId == userId));
                        json.Data[index].householdIds.push(householdId);
    
                        var combineUserIDHouseholdIDsTurbineIdPromise = new Promise((resolve, reject) => {
                            resolve(db.selectTurbineIds(householdId))
                        }).then(turbineIds => [userId, householdId, turbineIds]);
    
                        promises.push(combineUserIDHouseholdIDsTurbineIdPromise)
                    })
                })
                return Promise.all(promises);
            })
            .then(userIdsHouseholdTurbines => {
                userIdsHouseholdTurbines.forEach(res => {
                    var userId = res[0];
                    var householdId = res[1];
                    var turbineIds = res[2];
                    turbineIds.forEach(turbineId => {
                        var index = json.Data.indexOf(json.Data.find(element => element.userId == userId));
                        json.Data[index].turbineIds.push([householdId, turbineId]);
                    })
                })
            })
            .then(_ => {
                res.end(JSON.stringify(json))
            })
            .catch(err => console.error(err))
    });

    return router;
}