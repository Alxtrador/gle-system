var express = require('express');
var router = express.Router();
var Com = require('../Com');

module.exports = function factory(db) {
    router.delete('/household/:household_id', (req, res) => {
        if (req.session.userId  && req.session.householdIds.includes(req.params.household_id)) {
            db.deleteHousehold(req.session.userId, req.params.household_id) //delete household from database
                .then(val => Com.del.household(req.session.user_id, val)) //delete household from simulator
                .then(req.session.households.filter(element => element !== req.params.household_id)) //delete household from session
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.delete('/household/:household_id/turbine/:turbine_id', (req, res) => {
        if (req.session.userId && req.session.householdIds.includes(req.params.household_id) && req.session.turbineIds.includes(req.params.turbine_id)) {
            db.deleteTurbine(req.params.userId, req.params.household_id, req.params.turbine_id) //delete turbine from database
                .then(val => Com.del.household(req.session.user_id, req.params.household_id, val)) //delete turbine from simulator
                .then(req.session.turbines.filter(element => element !== req.params.turbine_id)) //delete turbine from session
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    return router;
}