var express = require('express');
var router = express.Router();
var Com = require('../Com');

module.exports = function factory(db) {
    router.get('/household/:household_id/cons', (req, res) => {
        if (req.session.userId) {
            Com.get.cons(req.session.userId, req.params.household_id)
                .then(val => res.end(JSON.stringify({ cons: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/turbine/:turbine_id/wind_speed', (req, res) => {
        if (req.session.userId) {
            Com.get.wind_speed(req.session.userId, req.params.household_id, req.params.turbine_id)
                .then(val =>res.end(JSON.stringify({ wind_speed: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/turbine/:turbine_id/elec_prod', (req, res) => {
        if (req.session.userId) {
            Com.get.elec_prod(req.session.userId, req.params.household_id, req.params.turbine_id)
                .then(val => res.end(JSON.stringify({ elec_prod: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/elec_price', (req, res) => {
        if (req.session.userId) {
            Com.get.elec_price()
                .then(val => res.end(JSON.stringify({ elec_price: val })))
                .catch(err => console.error(err));

        } else {
            res.status(400).end();
        }
    });

    router.get('/households', (req, res) => {
        if (req.session.userId) {
            Com.get.households(req.session.userId)
                .then(val => res.end(JSON.stringify({ households: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/turbines', (req, res) => {
        if (req.session.userId) {
            Com.get.turbines(req.session.userId, req.params.household_id)
                .then(val => res.end(JSON.stringify({ turbines: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    })

    router.get('/household/:household_id/buffer', (req, res) => {
        if (req.session.userId) {
            Com.get.buffer(req.session.userId, req.params.household_id)
                .then(val => res.end(JSON.stringify({ buffer: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/buy_ratio', (req, res) => {
        if (req.session.userId) {
            Com.get.buy_ratio(req.session.userId, req.params.household_id)
                .then(val => res.end(JSON.stringify({ buy_ratio: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/sell_ratio', (req, res) => {
        if (req.session.userId) {
            Com.get.sell_ratio(req.session.userId, req.params.household_id)
                .then(val => res.end(JSON.stringify({ sell_ratio: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/address', (req, res) => {
        if (req.session.userId) {
            db.selectHouseholdAddress(req.params.household_id)
                .then(val => res.end(JSON.stringify({ address: val })))
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/name', (req, res) => {
        if (req.session.userId) {
            db.selectHouseholdName(req.params.household_id)
                .then(val => res.end(JSON.stringify({ name: val })))
        } else {
            res.status(400).end();
        }
    });

    router.get('/household/:household_id/buffer_limit', (req, res) => {
        if (req.session.userId) {
            Com.get.buffer_limit(req.session.userId, req.params.household_id)
                .then(val => res.end(JSON.stringify({ buffer_limit: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/user_name', (req, res) => {
        if (req.session.userId) {
            db.selectUserName(req.session.userId)
                .then(val => res.end(JSON.stringify({ user_name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/first_name', (req, res) => {
        if (req.session.userId) {
            db.selectFirstName(req.session.userId)
                .then(val => res.end(JSON.stringify({ first_name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });

    router.get('/last_name', (req, res) => {
        if (req.session.userId) {
            db.selectLastName(req.session.userId)
                .then(val => res.end(JSON.stringify({ last_name: val })))
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });
    
    return router;
}