var express = require('express');
var router = express.Router();
var Com = require('../Com');

module.exports = function factory(db) {
    router.post('/household/:household_id/buffer/sell/:new_ratio', (req, res) => {
        if (req.session.userId) {
            Com.control.bufferSell(req.session.userId, req.params.household_id, req.params.new_ratio)
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();
        }
    });
    
    router.post('/household/:household_id/buffer/buy/:new_ratio', (req, res) => {
        if (req.session.userId) {
            Com.control.bufferBuy(req.session.userId, req.params.household_id, req.params.new_ratio)
                .then(res.end())
                .catch(err => console.error(err));
        } else {
            res.status(400).end();  
        }
    });

    return router;
}