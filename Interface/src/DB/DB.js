const pgp = require('pg-promise')();
require('dotenv').config();

class DB {
    constructor() {
        const cn = {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            database: process.env.DB_USER,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
        };
        const db = pgp(cn);
        this.db = db;
    }

    insertUser(f_name, l_name, user_name, roll, password) {
        return this.db.query("INSERT INTO usr VALUES (DEFAULT, $1, $2, $3, $4, crypt($5, gen_salt('bf'))) RETURNING id", [f_name, l_name, user_name, roll, password])
            .then(val => val[0].id);
    }

    insertHousehold(user_id, coords, h_name, address) {
        return this.db.query('INSERT INTO household VALUES (DEFAULT, $1, (point ($2, $3)), $4, $5) RETURNING id', [user_id, coords[0], coords[1], h_name, address])
            .then(val => val[0].id);
    }

    insertTurbine(house_id, coords, t_name) {
        return this.db.query('INSERT INTO turbine VALUES (DEFAULT, $1, (point ($2, $3)), $4) RETURNING id', [house_id, coords[0], coords[1], t_name])
            .then(val => val[0].id);
    }

    selectUser(user_name, password) {
        return this.db.query('SELECT * FROM usr WHERE usr_name = $1 AND password = crypt($2, password)', [user_name, password])
            .then(val =>  [val[0].id, val[0].role]);
    }

    selectUserName(user_id) {
        return this.db.query('SELECT * FROM usr WHERE id = $1', [user_id])
            .then(val => val[0].usr_name);
    }

    selectFirstName(user_id) {
        return this.db.query('SELECT * FROM usr WHERE id = $1', [user_id])
            .then(val => val[0].f_name);
    }

    selectLastName(user_id) {
        return this.db.query('SELECT * FROM usr WHERE id = $1', [user_id])
            .then(val => val[0].l_name);
    }

    selectNonManager(user_id) {
        return this.db.query('SELECT * FROM usr WHERE id = $1 AND role = 0', [user_id])
            .then(val => val[0].id);
    }

    selectUserIds() {
        return this.db.query('SELECT * FROM usr')
            .then(val => val.map((element) => element.id));
    }

    selectHouseholdIds(user_id) {
        return this.db.query('SELECT * FROM household WHERE usr_id = $1', [user_id])
            .then(val => val.map((element) => element.id));
    }

    selectTurbineIds(household_id){
        return this.db.query('SELECT * FROM turbine WHERE household_id = $1', [household_id])
            .then(val => val.map((element) => element.id));
    }

    selectHouseholdName(household_id) {
        return this.db.query('SELECT * FROM household WHERE id = $1', [household_id])
            .then(val => val[0].h_name);
    }

    selectHouseholdAddress(household_id) {
        return this.db.query('SELECT * FROM household WHERE id = $1', [household_id])
            .then(val => val[0].addr);
    }

    deleteUser(user_id) {
        return this.db.query('DELETE FROM usr WHERE id = $1', [user_id]);
    }

    deleteHousehold(household_id) {
        return this.db.query('DELETE FROM household WHERE id = $1', [household_id]);
    }

    deleteTurbine(turbine_id) {
        return this.db.query('DELETE FROM turbine WHERE id = $1', [turbine_id]);
    }

    updateUsername(user_id, new_name) {
        return this.db.query('UPDATE usr SET usr_name = $1 WHERE id = $2', [new_name, user_id]);
    }

    updateFirstname(user_id, new_name) {
        return this.db.query('UPDATE usr SET f_name = $1 WHERE id = $2', [new_name, user_id]);
    }

    updateLastname(user_id, new_name) {
        return this.db.query('UPDATE usr SET l_name = $1 WHERE id = $2', [new_name, user_id]);
    }
}

module.exports = {
    DB: DB
}
