const fetch = require('node-fetch');

function user(user_id) {
    return fetch('https://localhost:8444/delete/user/' + user_id, {method: 'DELETE'})
        .then(res => {
            if(res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("user_id does not exist");
            }
        });
}

function household(user_id, household_id) {
    return fetch('https://localhost:8444/delete/user/' + user_id + '/household/' + household_id, {method: 'DELETE'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            } else {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Household_id does not exist");
            }
        });
}

function turbine(user_id, household_id, turbine_id) {
    return fetch('https://localhost:8444/delete/user/' + user_id + '/household/' + household_id + '/turbine/' + turbine_id, {method: 'DELETE'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Turbine_id value does not exist");
            }
        });
}

module.exports = {
    user,
    household,
    turbine
}