const fetch = require('node-fetch');

function user(user_id) {
    return fetch('https://localhost:8444/add/user/' + user_id, {method: 'POST'})
        .then(res => {
            if(res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("user_id value is already used");
            }
        });
}

function household(user_id, household_id, coords) {
    return fetch('https://localhost:8444/add/user/' + user_id + '/household/' + household_id + '/coords/' + coords, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Household_id value is already used");
            }
        });
}

function turbine(user_id, household_id, turbine_id) {
    return fetch('https://localhost:8444/add/user/' + user_id + '/household/' + household_id + '/turbine/' + turbine_id, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Turbine_id value is already used");
            }
        });
}

module.exports = {
    user,
    household,
    turbine,
}