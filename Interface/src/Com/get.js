const fetch = require('node-fetch');

function cons(user_id, household_id) {
    return fetch('https://localhost:8444/api/cons/user/' + user_id + '/household/' + household_id)
        .then(res => res.json())
        .then(json => json.cons)
        .catch(err => console.error(err));
}

function wind_speed(user_id, household_id, turbine_id) {
    return fetch('https://localhost:8444/api/wind_speed/user/' + user_id + '/household/' + household_id + '/turbine/' + turbine_id)
        .then(res => res.json())
        .then(json => json.wind_speed)
        .catch(err => console.error(err));
}

function elec_prod(user_id, household_id, turbine_id) {
    return fetch('https://localhost:8444/api/elec_prod/user/' + user_id + '/household/' + household_id + '/turbine/' + turbine_id)
        .then(res => res.json())
        .then(json => json.elec_prod)
        .catch(err => console.error(err));
}

function elec_price() {
    return fetch('https://localhost:8444/api/elec_price')
        .then(res => res.json())
        .then(json => json.elec_price)
        .catch(err => console.error(err));
}

function households(user_id) {
    return fetch('https://localhost:8444/api/households/user/' + user_id)
        .then(res => res.json())
        .then(json => json.households)
        .catch(err => console.error(err));
}

function turbines(user_id, household_id) {
    return fetch('https://localhost:8444/api/turbines/user/' + user_id + '/household/' + household_id)
        .then(res => res.json())
        .then(json => json.turbines)
        .catch(err => console.error(err));
}

function buffer(user_id, household_id) {
    return fetch('https://localhost:8444/api/buffer/user/' + user_id + '/household/' + household_id)
        .then(res => res.json())
        .then(json => json.buffer)
        .catch(err => console.error(err));
}

function power_plant_prod() {
    return fetch('https://localhost:8444/api/power_plant/prod')
        .then(res => res.json())
        .then(json => json.prod)
        .catch(err => console.error(err));
}

function status() {
    return fetch('https://localhost:8444/api/power_plant/status')
        .then(res => res.json())
        .then(json => json.status)
        .catch(err => console.error(err));
}

function demand() {
    return fetch('https://localhost:8444/api/market/demand')
        .then(res => res.json())
        .then(json => json.demand)
        .catch(err => console.error(err));
}

function market_buffer() {
    return fetch('https://localhost:8444/api/market/buffer')
        .then(res => res.json())
        .then(json => json.buffer)
        .catch(err => console.error(err));
}

function blackout() {
    return fetch('https://localhost:8444/api/households/blackout')
        .then(res => res.json())
        .then(json => json.blackout)
        .catch(err => console.error(err));
}

function modelled_elec_price() {
    return fetch('https://localhost:8444/api/modelled_elec_price')
        .then(res => res.json())
        .then(json => json.modelled_elec_price)
        .catch(err => console.error(err));
}

function buy_ratio(userId, householdId) {
    return fetch('https://localhost:8444/api/user/' + userId + '/household/' + householdId + '/buy_ratio')
        .then(res => res.json())
        .then(json => json.buy_ratio)
        .catch(err => console.error(err));
}

function sell_ratio(userId, householdId) {
    return fetch('https://localhost:8444/api/user/' + userId + '/household/' + householdId + '/sell_ratio')
        .then(res => res.json())
        .then(json => json.sell_ratio)
        .catch(err => console.error(err));
}

function power_plant_buffer() {
    return fetch('https://localhost:8444/api/power_plant/buffer')
        .then(res => res.json())
        .then(json => json.power_plant_buffer)
        .catch(err => console.error(err));
}

function ratio() {
    return fetch('https://localhost:8444/api/power_plant/ratio')
        .then(res => res.json())
        .then(json => json.ratio)
        .catch(err => console.error(err));
}

function buffer_limit(userId, householdId) {
    return fetch('https://localhost:8444/api/user/' + userId + '/household/'+ householdId + '/buffer_limit')
        .then(res => res.json())
        .then(json => json.buffer_limit)
        .catch(err => console.error(err));
}

function blackout() {
    return fetch('https://localhost:8444/api/households/blackout')
        .then(res => res.json())
        .then(json => json.blackout)
        .catch(err => console.error(err));
}

function userBlocked(userId) {
    return fetch('https://localhost:8444/api/user/' + userId + '/block')
        .then(res => res.json())
        .then(json => json.block)
        .catch(err => console.error(err));
}


module.exports = {
    cons,
    wind_speed,
    elec_prod,
    elec_price,
    households,
    turbines,
    buffer,
    power_plant_prod,
    status,
    demand,
    market_buffer,
    blackout,
    modelled_elec_price,
    buy_ratio,
    sell_ratio,
    power_plant_buffer,
    ratio,
    buffer_limit,
    blackout,
    userBlocked
}