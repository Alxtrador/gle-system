const fetch = require('node-fetch');

function prod() {
    return fetch('https://localhost:8444/control/power_plant/prod', {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Power plant production could not be changed");
            }
        });
}

function stop_prod() {
    return fetch('https://localhost:8444/control/power_plant/prod/stop', {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Power plant production could not be changed");
            }
        });
}

function ratio(new_ratio) {
    return fetch('https://localhost:8444/control/buffer/ratio/' + new_ratio, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Ratio could not be changed");
            }
        }); 
}

function elec_price(new_price) {
    return fetch('https://localhost:8444/control/elec_price/' + new_price, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Price could not be changed");
            }
        });
}

function market_block(userId, time) {
    return fetch('https://localhost:8444/control/user/' + userId + '/market_block/time/' + time, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Market access could not be blocked");
            }
        });
}

function bufferSell(userId, householdId, newRatio) {
    return fetch('https://localhost:8444/control/user/'+ userId + '/household/'+ householdId + '/buffer/sell/' + newRatio, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Could not change sell buffer ratio");
            }
        });
}

function bufferBuy(userId, householdId, newRatio) {
    return fetch('https://localhost:8444/control/user/'+ userId + '/household/'+ householdId + '/buffer/buy/' + newRatio, {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Could not change buy buffer ratio");
            }
        });
}

function togglePrice() {
    return fetch('https://localhost:8444/control/toggle/price', {method: 'POST'})
        .then(res => {
            if (res.status != 201) {
                throw res.status;
            }
        })
        .catch(err => {
            if (err == 400) {
                console.error("Could not toggle market price");
            }
        });
}

module.exports = {
    prod,
    stop_prod,
    ratio,
    elec_price,
    market_block,
    bufferSell,
    bufferBuy,
    togglePrice,
}