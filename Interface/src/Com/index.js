const add = require('./add.js');
const get = require('./get.js');
const del = require('./delete.js');
const control = require('./control.js');

module.exports = {
    add,
    get,
    del,
    control
}