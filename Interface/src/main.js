process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const express = require('express');
const session = require('express-session');
const https = require('https');
const fs = require('fs');
const path = require('path');
const app = express();


app.use(express.static('public'));
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true,
              maxAge: 86400 * 1000 }
}));

const key = fs.readFileSync(path.join(__dirname, '../server.key'), 'utf8');
const cert = fs.readFileSync(path.join(__dirname, '../server.crt'), 'utf8');
const credentials = {key: key, cert: cert};

var DB = require('./DB').DB;
var db = new DB();

var user_routes = require('./Routes').user_routes(db);
var add_routes = require('./Routes').add_routes(db);
var delete_routes = require('./Routes').delete_routes(db);
var get_routes = require('./Routes').get_routes(db);
var control_routes = require('./Routes').control_routes(db);
var init_route = require('./Routes').init_route(db);
var manager_control_routes = require('./Routes/Manager').manager_control_routes(db);
var manager_get_routes = require('./Routes/Manager').manager_get_routes(db);
var manager_user_routes = require('./Routes/Manager').manager_user_routes(db);
var manager_image_routes = require('./Routes/Manager').manager_image_routes(db);
var image_routes = require('./Routes').image_routes(db);


app.use('/user', user_routes);
app.use('/add', add_routes);
app.use('/delete', delete_routes);
app.use('/get', get_routes);
app.use('/control', control_routes);
app.use('/init', init_route);
app.use('/manager/control', manager_control_routes);
app.use('/manager/get', manager_get_routes);
app.use('/manager/user', manager_user_routes);
app.use('/manager/image', manager_image_routes);
app.use('/image', image_routes);


var httpsServer = https.createServer(credentials, app);
httpsServer.listen(8443);
